<?php

namespace App\Deal\Ads;
use Auth;
use Hash;
use Exception;
use App\Models\Ad;
use Aws\S3\S3Client;
use App\Models\Favorite;
use Illuminate\Support\Str;
use App\Deal\Mailers\UserMailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Illuminate\Contracts\Filesystem\Filesystem;

//use League\Flysystem\Filesystem;

class AdRepository {

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Store new ad service.
     *
     * @param $data
     * @return bool
     */
    public function storeAd($data)
    {
        $ad = new Ad();

        $ad->title = $data['title'];
        $ad->slug = '';
        $ad->description = $data['description'];
        $ad->price = $data['price'];
        $ad->type = $data['type'];
        $ad->user_type = $this->auth->user()->type;
        $ad->category_id = $data['category_id'];
        $ad->city_id = $data['city_id'];
        $ad->status = false;
        $ad->hide_phone = $data['hide_phone'];
        $ad->negotiable = $data['negotiable'];
        $ad->user_id = $this->auth->user()->id;
        $ad->moderated_by = $this->auth->user()->id;

        if(!empty($data['shop_slug']))
            $ad->shop_slug = $data['shop_slug'];

        //Start transaction
        DB::beginTransaction();
        try{
            $ad->save();
            $medias = [];
            if(!is_null($data['medias'][0])){
                foreach ($data['medias'] as $file) {
                    $medianame = sha1(time().date('d-m-Y-H-i-s').uniqid(rand(), true).str_random('60')) . '.' . $file->getClientOriginalExtension();
                    $media = [];


                    $tmpImage = Image::make($file)->heighten(460)->save();
                    // $tmpImage = Image::make($file->getRealPath())->heighten(460);
                    // $hash = md5($tmpImage->__toString());
                    // $path = "images/item/tp/{$hash}.{$file->getClientOriginalExtension()}";
                    // $payload = (string)$tmpImage->encode("jpg");
                    //Image::make($file->getRealPath())->resize(816,460)->save(public_path('images/item/tp/'.$medianame));
                    //dd($payload);
                    // $media['url'] = asset($path);
                    $path = $file->store("images/item/tp");
                    $media['url'] = Storage::url($path);
                    
                    // Storage::putFile("images/item/tp/", $payload);
                    // $tmpImage->save(storage_path("app/".$path));
                    $medias[] = $media;
                    // dd($media);
                }
            }else{
                $media['url'] = asset("front/images/default-ad.png");
                $medias[] = $media;
            }
            $ad->medias()->createMany($medias);
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {

            // dd($e);
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de créer une annonce. (AdRepository@storeAd)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Update ad service.
     *
     * @param $data
     * @param $id
     * @return bool
     */
    public function updateAd($data,$id)
    {
        $ad = Ad::where('id',$id)->where('user_id',$this->auth->user()->id)->firstOrFail();
        $ad->title = $data['title'];
        $ad->slug = '';
        $ad->description = $data['description'];
        $ad->price = $data['price'];
        $ad->type = $data['type'];
        $ad->category_id = $data['category_id'];
        $ad->city_id = $data['city_id'];
        $ad->status = false;
        $ad->hide_phone = $data['hide_phone'];
        $ad->negotiable = $data['negotiable'];
        $ad->user_id = $this->auth->user()->id;

        if(!empty($data['shop_slug'])){
            $ad->shop_slug = $data['shop_slug'];
        }


        //Start transaction
        DB::beginTransaction();
        try{
            $ad->slug = Str::slug($data['title']).'-'.$ad->id.mt_rand(100000,999999);
            $medias = [];
            if(!is_null($data['medias'][0])){
                foreach ($data['medias'] as $file) {
                    $medianame = sha1(time().date('d-m-Y-H-i-s').uniqid(rand(), true).str_random('60')) . '.' . $file->getClientOriginalExtension();
                    $media = [];


                    $tmpImage = Image::make($file)->heighten(460)->save();
                    // $tmpImage = Image::make($file->getRealPath())->heighten(460);
                    // $hash = md5($tmpImage->__toString());
                    // $path = "images/item/tp/{$hash}.{$file->getClientOriginalExtension()}";
                    // $payload = (string)$tmpImage->encode("jpg");
                    //Image::make($file->getRealPath())->resize(816,460)->save(public_path('images/item/tp/'.$medianame));
                    //dd($payload);
                    // $media['url'] = asset($path);
                    $path = $file->store("images/item/tp");
                    $media['url'] = Storage::url($path);
                    
                    // Storage::putFile("images/item/tp/", $payload);
                    // $tmpImage->save(storage_path("app/".$path));
                    $medias[] = $media;
                    // dd($media);
                }
            }
            $ad->medias()->createMany($medias);
            $ad->save();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de mettre à jour une annonce. (AdRepository@updateAd)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Delete ad service.
     *
     * @param $id
     * @return bool
     */
    public function destroyAd($id)
    {
        //Start transaction
        DB::beginTransaction();
        try{
            $ad = Ad::where('id','=',$id)->where('user_id','=',$this->auth->user()->id)->first();
            $favorites = Favorite::where('ad_id','=',$ad->id)->where('user_id','=',$this->auth->user()->id)->get();
            // Delete all favorites
            foreach($favorites as $favorite)
            {
                $favorite->delete();
            }
            // Delete ad
            $ad->delete();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer une annonce. (AdRepository@destroyAd)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Delete ad admin service.
     *
     * @param $id
     * @return bool
     */
    public function destroyAdAdmin($id)
    {
        //Start transaction
        DB::beginTransaction();
        try{
            $ad = Ad::find($id);
            $favorites = Favorite::where('ad_id','=',$ad->id)->get();
            // Delete all favorites
            foreach($favorites as $favorite)
            {
                $favorite->delete();
            }

            // Delete ad
            $ad->delete();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer une annonce. (AdRepository@destroyAdAdmin)');
            //False if error occurs
            return false;
        }
    }

}
