<?php

/**
 * Global function to supervise and log activity message (errors, warnings,infos an success)
 */

if(!function_exists('supervisor')){
    function supervisor($message,$type = 'danger')
    {
        $log = new App\Models\Log();
        $log->message = $message;
        $log->type = $type;
        $log->user_id = !is_null(auth()->user()) ? auth()->user()->id : null;
        $log->created_at = Carbon\Carbon::now();
        $log->save();
    }
}