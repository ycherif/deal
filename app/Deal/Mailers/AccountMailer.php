<?php

namespace App\Deal\Mailers;

use Illuminate\Support\Facades\Mail;

class AccountMailer extends Mailer {

    /**
     * Send email for activation account.
     *
     * @param $email
     * @param $data
     */
    public function verify($email, $data)
    {
        $view       = 'emails.accounts.activate-link';
        $subject    = $data['subject'];
        //$fromEmail  = 'no-reply@'.config('deal.country').'pinsdeal.com';
        $fromEmail = env('MAIL_USERNAME');
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    /**
     * Send email for credentials account.
     *
     * @param $email
     * @param $data
     */
    public function credential($email, $data)
    {
        $view       = 'emails.accounts.credential';
        $subject    = $data['subject'];
        $fromEmail = env('MAIL_USERNAME');
        //$fromEmail  = 'no-replay@'.config('deal.country').'pinsdeal.com';
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    /**
     * Send email for updated credentials account.
     *
     * @param $email
     * @param $data
     */
    public function updateCredential($email, $data)
    {
        $view       = 'emails.accounts.updatecredential';
        $subject    = $data['subject'];
        $fromEmail = env('MAIL_USERNAME');
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    /**
     * Send email with user password.
     *
     * @param $email
     * @param $data
     */
    public function newAccount($email, $data)
    {
        $view       = 'emails.accounts.newaccount';
        $subject    = $data['subject'];
        $fromEmail = env('MAIL_USERNAME');
        //$fromEmail  = 'no-replay@'.config('deal.country').'pinsdeal.com';
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    /**
     * Send email for reset password
     *
     * @param $email
     * @param $data
     */
    public function passwordReset($email, $data)
    {
        $view       = 'emails.accounts.password-reset';
        $subject    = $data['subject'];
        $fromEmail = env('MAIL_USERNAME');
        //$fromEmail  = 'no-replay@'.config('deal.country').'pinsdeal.com';
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    public function shopInvoice($email, $data)
    {
        $view       = 'emails.shops.invoice';
        $subject    = $data['subject'];
        //$fromEmail  = 'no-replay@'.config('deal.country').'pinsdeal.com';
        $fromEmail = env('MAIL_USERNAME');
        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }
}
