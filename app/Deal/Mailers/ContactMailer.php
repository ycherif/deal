<?php

namespace App\Deal\Mailers;

use App\Models\User;

class ContactMailer extends Mailer {

    /**
     * Send email for contacting user.
     *
     * @param $data
     */
    public function contactMember($data)
    {
        $view       = 'emails.contacts.member';
        $subject    = 'À propos de votre annonce';
        $fromEmail  = $data['sender'];
        $email = $data['receiver'];

        $datas = [
            'email' => $fromEmail,
            'name' => $data['name'],
            'phone' => $data['phone'],
            'body' => $data['body'],
            'slug' => $data['slug']
        ];
        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
    }

    /**
     * Send email for contacting user.
     *
     * @param $data
     */
    public function contactShop($data)
    {
        $view       = 'emails.shops.shop';
        $subject    = 'PinsDeal';
        $fromEmail  = $data['sender'];
        $email = $data['receiver'];

        $datas = [
            'email' => $fromEmail,
            'name' => $data['name'],
            'phone' => $data['phone'],
            'body' => $data['body'],
            'slug' => $data['slug']
        ];
        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
    }

    /**
     * Send email for contacting moderator.
     *
     * @param $data
     * @return bool
     */
    public function contactModerator($data)
    {
        $view       = 'emails.contacts.moderator';
        $subject    = 'Signaler un abus sur une annonce';
        $fromEmail  = $data['sender'];
        $email = User::findOrFail($data['id_receiver'])->email;

        $datas = [
            'email' => $fromEmail,
            'reason' => $data['reason'],
            'body' => $data['body'],
            'slug' => $data['slug'],

        ];
        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function contactModerators($data)
    {
        $view       = 'emails.contacts.moderators';
        $subject    = $data['subject'];
        $fromEmail = env('MAIL_USERNAME');
        $email      = $data['receiver'];

        $datas = [
            'email' => $data['sender'],
            'body' => $data['message'],
            'username' => $data['username'],
            'phone'=>$data['phone']

        ];
        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
        return true;
    }

    /**
     * Send email for contacting moderator.
     *
     * @param $data
     * @return bool
     */
    public function contactAdmin($data)
    {
        $view       = 'emails.contacts.admin';
        $subject    = 'Contact Message';
        $fromEmail  = env('MAIL_USERNAME');
        $email  = config('deal.emailAdmin');

        $datas = [
            'email' => $data['sender'],
            'body' => $data['message']
        ];
        if(!empty($data['firstname']))
            $datas['firstname'] = $data['firstname'];
        if(!empty($data['lastname']))
            $datas['lastname'] = $data['lastname'];
        if(!empty($data['enterprise']))
            $datas['enterprise'] = $data['enterprise'];

        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
        return true;
    }

    /**
     * Send email for contacting moderator.
     *
     * @param $data
     * @return bool
     */
    public function contactFaq($data)
    {
        $view       = 'emails.contacts.faq';
        $subject    = 'Réponse à votre question sur PinsDeal';
        $fromEmail = env('MAIL_USERNAME');
        //$fromEmail  = 'no-replay@'.config('deal.country').'pinsdeal.com';
        $email = $data['email'];

        $datas = [
            'email' => $fromEmail,
            'question' => $data['question'],
            'response' => $data['response']
        ];
        $this->sendTo($email, $subject, $fromEmail, $view, $datas);
        return true;
    }
}