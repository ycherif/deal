<?php

namespace App\Deal\Mailers;

use Illuminate\Support\Facades\Mail;

abstract class Mailer {

    /**
     * Send email service
     *
     * @param $email
     * @param $subject
     * @param $fromEmail
     * @param $view
     * @param array $data
     */
    public function sendTo($email, $subject, $fromEmail, $view, $data = [])
    {
        Mail::send($view, $data, function($message) use($email, $subject, $fromEmail)
        {
            //dd($message);
            $message->from($fromEmail,env('COMPT_NAME'));
            $message->to($email)->subject($subject);
        });

    }
}