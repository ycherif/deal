<?php

namespace App\Deal\Observers;

use Illuminate\Support\Str;

class AdObserver{


    public function created($model)
    {
        $model->slug = Str::slug($model->title).'-'.$model->id.mt_rand(100000,999999);
        $model->save();
    }

}
