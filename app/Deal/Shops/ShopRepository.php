<?php

namespace App\Deal\Shops;
use App\Deal\Mailers\UserMailer;
use App\Models\Ad;
use App\Models\Shop;
use Illuminate\Contracts\Auth\Guard;
use Exception;
use Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ShopRepository {

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Store new shop service.
     *
     * @param $data
     * @return bool
     */
    public function storeShop($data)
    {

        $shop = new Shop();

        $shop->name = $data['name'];
        $shop->slug = Str::slug($data['name']);
        $shop->description = $data['description'];
        $shop->site = $data['site'];
        $shop->phone = $data['phone'];
        $shop->email = $data['email'];
        $shop->address1 = $data['address1'];
        $shop->address2 = $data['address2'];
        $shop->address3 = $data['address3'];
        $shop->category_name = $data['category_name'];
        $shop->slug_search = Str::slug($data['category_name']);;
        $shop->city_name = $data['city_name'];
        $shop->city_slug = Str::slug($data['city_name']);
        $shop->user_id = $this->auth->user()->id;

        //Start transaction
        DB::beginTransaction();
        try{
            if(!empty($data['logo']) ){
                $medianame = sha1(time().date('d-m-Y-H-i-s').uniqid(rand(), true).str_random('60')).'.'.$data['logo']->getClientOriginalExtension();
                $shop->logo = "back/img/shops/" . $medianame;
                //Image::make($data['logo']->getRealPath())->fit(200,200)->save(public_path('back/img/shops/'.$medianame));
                Storage::disk('s3')->put('back/img/avatars/' . $medianame);

            }else{
                $shop->logo = "back/img/shops/default-shop.png";
            }


            $shop->save();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de créer une boutique. (ShopRepository@storeShop)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Update shop service.
     *
     * @param $data
     * @param $id
     * @param bool $isAdmin
     * @return bool
     */
    public function updateShop($data, $id, $isAdmin = false)
    {

        $shop = $isAdmin ? Shop::where('id',$id)->firstOrFail() : Shop::where('id',$id)->where('user_id',$this->auth->user()->id)->firstOrFail();
        $ads = Ad::where('shop_slug',$shop->slug)->get();

        $shop->name = $data['name'];
        $shop->slug = Str::slug($data['name']);
        $shop->description = $data['description'];
        $shop->phone = $data['phone'];
        $shop->email = $data['email'];
        $shop->site = $data['site'];
        $shop->address1 = $data['address1'];
        $shop->address2 = $data['address2'];
        $shop->address3 = $data['address3'];
        $shop->category_name = $data['category_name'];
        $shop->slug_search = Str::slug($data['category_name']);
        $shop->city_name = $data['city_name'];
        $shop->city_slug = Str::slug($data['city_name']);

        //Start transaction
        DB::beginTransaction();
        try{
            if(!empty($data['logo']) ){
                $medianame = sha1(time().date('d-m-Y-H-i-s').uniqid(rand(), true).str_random('60')).'.'.$data['logo']->getClientOriginalExtension();
                $shop->logo = "back/img/shops/" . $medianame;
                $tmpImage = Image::make($data['logo']->getRealPath())->heighten(460);
                $payload = (string)$tmpImage->encode();

                //Image::make($data['logo']->getRealPath())->fit(200,200)->save(public_path('back/img/shops/'.$medianame));
                Storage::disk('s3')->put("back/img/shops/" . $medianame, $payload);

            }

            $shop->save();
            //Commit if no error
            DB::commit();
            foreach($ads as $ad){
                $ad->shop_slug = $shop->slug;
                $ad->save();
            }

            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de mettre à jour une boutique. (ShopRepository@updateShop)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Delete shop service.
     *
     * @param $id
     * @return bool
     */
    public function destroyShop($id)
    {
        //Start transaction
        DB::beginTransaction();
        try{
            $shop = Shop::where('id','=',$id)->where('user_id','=',$this->auth->user()->id)->first();

            // Delete ad
            $shop->delete();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer une boutique. (ShopRepository@destroyShop)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Delete shop admin service.
     *
     * @param $id
     * @return bool
     */
    public function destroyShopAdmin($id)
    {
        //Start transaction
        DB::beginTransaction();
        try{
            $shop = Shop::findOrFail($id);
            // Delete ad
            $shop->delete();
            //Commit if no error
            DB::commit();
            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer une boutique. (ShopRepository@destroyShopAdmin)');
            //False if error occurs
            return false;
        }
    }

}
