<?php

namespace App\Deal\Users;
use App\Deal\Mailers\AccountMailer;
use App\Models\Ad;
use App\Models\Favorite;
use App\Models\Media;
use App\Models\Role;
use App\Models\Social;
use App\Models\User;
use App\Models\Password;
use Illuminate\Contracts\Auth\Guard;
use Exception;
use Hash, Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Intervention\Image\Facades\Image;

class UserRepository {

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Mailer service instance
     *
     * @var AccountMailer
     */
    protected $accountMailer;

    public function __construct(Guard $auth, AccountMailer $accountMailer )
    {
        $this->accountMailer = $accountMailer;
        $this->auth = $auth;
    }

    /**
     * Register new user service
     *
     * @param $data
     * @param bool $isAdmin
     * @param bool $firstTime
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register($data,$isAdmin,$firstTime)
    {
        $user = $firstTime ? new User : User::onlyTrashed()->where('email','=',$data['email'])->firstOrFail();
        $token = sha1(str_random(80) . Carbon::now());
        $user->username        = $data['username'];
        $user->firstname       = $data['firstname'];
        $user->lastname        = $data['lastname'];
        $user->email           = $data['email'];
        $user->city_id         = $data['city_id'];
        $user->type            = $data['type'];
        $user->phone           = $data['phone'];
        $user->gender          = $data['gender'];
        $user->active          = $isAdmin ? true : false;
        $user->active_token    = $isAdmin ? null : $token;
        $password = $isAdmin ? str_random(8) : '';
        $user->password        = $isAdmin ? Hash::make($password) : Hash::make($data['password']);
        $userRole = $isAdmin ? $data['userRole'] : 'member';

        if(!$firstTime){
            $user->deleted_at = null;
        }

        //Start transaction
        DB::beginTransaction();
        try {
            //Save new user
            $user->save();
            //Assign Role
            $role = Role::whereSlug($userRole)->firstOrFail();
            $user->assignRole($role);
            //Commit if no error
            DB::commit();
            //Send confirmation mail
            $data = $isAdmin ? [
                'firstname'    => ucfirst($user->firstname),
                'password'         => $password,
                'subject'       => 'Identifiants de compte',
                'email'         => $user->email,
                'role' => $userRole,
                'username' => $user->username
            ] : [
                'firstname'    => ucfirst($user->firstname),
                'token'         => $token,
                'subject'       => 'Activation de compte',
                'email'         => $user->email
            ];

            if($isAdmin){
                $this->accountMailer->credential($user->email, $data);
            }else{
                //dd($data);
                $this->accountMailer->verify($user->email, $data);
            }

            //dd('edr');

            //True if no error
            return true;
        } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de créer un compte utilisateur. (UserRepository@register)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Reset user password
     *
     * @param User $user
     */
    public function resetPassword( User $user)
    {
        $token = sha1(str_random(80). Carbon::now());
        $password = new Password;
        $password->email = $user->email;
        $password->token = $token;
        $password->created_at = Carbon::now();
        $password->save();
        $data = [
            'firstname'    => $user->firstname,
            'token'         => $token,
            'subject'       => 'Réinitialisation de mot de passe',
            'email'         => $user->email
        ];

        $this->accountMailer->passwordReset($user->email, $data);
    }

    /**
     * Update user informations
     *
     * @param $data
     * @param bool $isAdmin
     * @return bool
     */
    public function updateProfile($data, $isAdmin = false)
    {
        $alreadyHasRole = false;
        // Check if admin or not and select appropriate user
        $user = $isAdmin ? User::findOrFail($data['user_id']) : $this->auth->user();
        $lastAvatar = $user->avatar;
        if (isset($data['userRole'])){
            foreach($user->roles as $rule){
                if ($rule->slug === $data['userRole']){
                    $alreadyHasRole = true;
                }
            }
        }
        $user->username = $data['username'];
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->type = $isAdmin ? $data['type'] : $user->type;
        $user->city_id = $data['city_id'];
        $user->phone = $data['phone'];
        $user->description = $isAdmin ? $user->description : $data['description'];
        $user->address = $isAdmin ? $user->address : $data['address'];

        $userPassword = '';

        // If image is selected and not admin
        if(isset($data['avatar']) && !is_null($data['avatar']) && !$isAdmin ){
            $medianame = sha1(time().date('d-m-Y-H-i-s').uniqid(rand(), true).str_random('60')) . '.' . $data['avatar']->getClientOriginalExtension();
            $tmpImage = Image::make($data['avatar']->getRealPath())->heighten(460);
            $payload = (string)$tmpImage->encode();

            $user->avatar = "/back/img/avatars/" . $medianame;

            //dd($lastAvatar);
            $tmpImage->save(public_path('back/img/avatars/'.$medianame));
            // Image::make($data['avatar']->getRealPath())->fit(200,200)->save(public_path('back/img/avatars/'.$medianame));
            //Storage::disk('s3')->delete($lastAvatar,$payload);
            // Storage::disk('s3')->put('back/img/avatars/' . $medianame,$payload);

        }
        //Start transaction
        DB::beginTransaction();
        try {
            // Assign role and create new password
            if($isAdmin){
                //Assign Role

                if(!$alreadyHasRole)
                {
                    $role = Role::whereSlug($data['userRole'])->firstOrFail();
                    $user->assignRole($role);
                }
                if($data['changePassword']){
                    $userPassword = str_random(8);
                    $user->password = Hash::make($userPassword);
                }
            }

            // Save user
            $user->save();
            //Commit if no error
            DB::commit();
            // If is admin send to user email with
            if($isAdmin){
                $data = [
                    'subject'   => 'Mis à jour de compte',
                    'username'  => $user->username,
                    'firstname' => $user->firstname,
                    'lastname'  => $user->lastname,
                    'type'      => $user->type,
                    'phone'     => $user->phone,
                    'role'      => $user->roles[0]->slug,
                    'city'      => $user->city->name,
                    'email'     => $user->email
                ];

                // If password is set
                if(!empty($userPassword) && isset($userPassword)){
                    $data['password'] = $userPassword;
                }
                $this->accountMailer->updateCredential($user->email, $data);
            }
            //True if no error
            return true;
       } catch (Exception $e) {
            //Rollback if error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de mettre à jour le profile. (UserRepository@updateProfile)');
            //False if error occurs
            return false;
        }
    }

    /**
     * Delete user account and their ads and favorites service
     *
     * @param null $id
     * @param null $isAdmin
     * @return bool
     */
    public function deleteAccount($id = null, $isAdmin = null)
    {
        $user = is_null($id) ? User::findOrFail($this->auth->user()->id) : User::findOrFail($id);
        $userSocials = Social::where('user_id','=', $user->id)->get();
        $userAds = Ad::where('user_id','=', $user->id)->get();
        $userFavotites = Favorite::where('user_id','=', $user->id)->get();

        DB::beginTransaction();
        try {
            // Delete all user ads
            foreach($userAds as $ad){
                $medias = Media::where('ad_id','=',$ad->id)->get();
                foreach($medias as $media){
                    $media->delete();
                }
                $ad->delete();
            }

            // Delete all user socials logins
            foreach($userSocials as $userSocial){
                $userSocial->delete();
            }

            // Delete all user favorites
            foreach($userFavotites as $favorite){
                $favorite->delete();
            }

            // Delete user
            if($isAdmin){
                $user->banned = true;
            }
            $user->active = false;
            $user->save();
            $user->delete();
            // Commit if all good
            DB::commit();
            if(!$isAdmin)
                $this->auth->logout();
            return true;
        } catch (Exception $e) {
            // Rollback if an error occurs
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer un compte. (UserRepository@deleteAccount)');
            return false;
        }
    }
}
