<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Exception;
use Validator;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Role;
use App\Models\User;
use App\Models\Social;
use Illuminate\Support\Str;
use App\Traits\CaptchaTrait;
use App\Deal\Users\UserRepository;
use App\Deal\Mailers\AccountMailer;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Facades\Socialite;


class AuthController extends Controller {

    use CaptchaTrait;

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Mailer service instance
     *
     * @var AccountMailer
     */
    protected $accountMailer;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct( Guard $auth, UserRepository $userRepository, AccountMailer $accountMailer )
    {
        $this->auth = $auth;
        $this->userRepository = $userRepository;
        $this->accountMailer = $accountMailer;
    }

    /**
     * Show login page.
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Authenticate user.
     *
     * @param LoginRequest $req
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(LoginRequest $req)
    {
        $password   = $req->input('password');
        $remember   = $req->input('remember');
        $field = filter_var($req->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if($this->auth->attempt([$field => $req->input('login'), 'password' => $password, 'active' => true, 'banned' => false], $remember == 1 ? true : false)){
            return redirect()->route('profile.index');
        } else {
            return redirect()->back()
                ->with('message','Votre mot de passe ou email/nom d\'utilisateur est incorrecte!')
                ->with('type', 'danger')
                ->withInput();
        }
    }

    /**
     * Logout user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
       try{
           $user = User::findOrFail($this->auth->user()->id);
           $user->last_login = Carbon::now();
           $user->save();
           $this->auth->logout();
           return redirect()->route('home');
       } catch(Exception $e){
           // Store message in log
           supervisor('Impossible de déconnecter un utilisateur. (AuthController@getLogout)','warning');
           return redirect()->route('profile.index');
       }
    }

    /**
     * Show register page
     *
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        $cities = City::all();
        return view('auth.register',compact('cities'));
    }

    /**
     * Register new user
     *
     * @param RegisterRequest $req
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postRegister(RegisterRequest $req)
    {
        $isAdmin = false;
        $firstTime = true;

        // Check if user is not a robot by google reCaptcha
        if($this->captchaCheck() == false){
            return redirect()->back()
                ->withErrors(['Confirmer que vous n\'êtes pas un robot!'])
                ->withInput();
        }

        $newUser = [
            'username'      => $req->input('username'),
            'firstname'     => $req->input('firstname'),
            'lastname'      => $req->input('lastname'),
            'email'         => $req->input('email'),
            'password'      => $req->input('password'),
            'city_id'       => $req->input('city_id'),
            'type'          => $req->input('type'),
            'phone'         => $req->input('phone'),
            'gender'        => $req->input('gender')
        ];

        // Check if user is soft deleted and not banned by admin
        $userExist = User::onlyTrashed()->where('email','=',$req->input('email'))->first();
        if(!is_null($userExist)){
            if($userExist->banned == true){
                return redirect()->route('home')
                    ->with('type', 'danger')
                    ->with('message', 'Votre compte à été bani du site. Contactez le support pour plus d\'info!');
            }
            $firstTime = false;
        } else{
            // Validate
            $validation = Validator::make(['email' => $req->input('email'), 'username' => $req->input('username')], ['email' => 'unique:users','username' => 'unique:users']);
            if($validation->fails()){
                return redirect()->route('auth.register')
                    ->withErrors($validation)
                    ->withInput();
            }
        }

        // Validate
        $validation = Validator::make(['username' => $req->input('username')], ['username' => 'unique:users']);
        if($validation->fails()){
            return redirect()->route('auth.register')
                ->withErrors($validation)
                ->withInput();
        }
        //dd('dfr');
        if($this->userRepository->register($newUser, $isAdmin, $firstTime)){
            return redirect()->route('home')
                ->with('type', 'success')
                ->with('message', 'Votre compte à bien été créé. Nous vous avons envoyé un mail de confirmation.');
        } else{
            // Store message in log
            supervisor('Impossible de créer un utilisateur. (AuthController@postRegister)','warning');
            return redirect()->route('auth.register')
                ->with('type', 'danger')
                ->with('message', 'Le compte n\'a pas été créé. Rééssayez plus tard!');
        }
    }

    /**
     * Activate user account
     *
     * @param $token
     * @return mixed
     */
    public function getConfirm($token)
    {
        try{
            // Find corresponding user and update information
            $user = User::where('active_token','=',$token)->firstOrFail();
            $user->active_token = "";
            $user->active = true;
            $user->save();
            return redirect()->route('auth.login')
                ->with('message', 'Votre compte à bien été activé. Connectez-vous maintenant.')
                ->with('type','success');
        } catch(Exception $e){
            // Store message in log
            supervisor('Impossible d\'activer un compte d\'utilisateur. (AuthController@getConfirm)','warning');
            return redirect()->route('home')
                ->with('message', 'Rassurez-vous d\'avoir clicker sur le bon lien de confirmation!')
                ->with('type','danger');
        }
    }

    /**
     * Redirect user to social
     *
     * @param $provider
     * @return $this
     */
    public function getSocialRedirect($provider)
    {
        $providerKey = Config::get('services.' . $provider);
        if(empty($providerKey)){
            return redirect()->route('home')
                ->with('message','L\'authentification avec ce réseau social n\'est pas encore disponible!')
                ->with('type', 'warning');
        }
        return Socialite::driver( $provider )->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getSocialHandle($provider, \Illuminate\Http\Request $request)
    {
        try{
            $user = Socialite::driver($provider)->user();
            $socialUser = null;
            // Check is this email present
            $userCheck = User::where('email', '=', $user->email)->first();

            if(!is_null($userCheck)) {
                $socialUser = $userCheck;
            } else {
                // Check if user had been trashed
                $userTrashed = User::onlyTrashed()->where('email','=',$user->email)->first();
                if(!is_null($userTrashed)){
                    // Check if user had been banned by admin
                    if($userTrashed->banned == true){
                        return redirect()->route('home')
                            ->with('message', 'Votre compte à été banis du site. Contactez le support pour plus d\'info!')
                            ->with('type', 'danger');
                    }
                    $userTrashed->deleted_at = null;
                    $userTrashed->active = true;
                    $userTrashed->social()->deleted_at = null;
                    $userTrashed->save();
                    $this->auth->login($userTrashed, true);
                    return redirect()->route('profile.index');
                }

                $sameSocialId = Social::where('social_id', '=', $user->id)->where('provider', '=', $provider )->first();
                if(is_null($sameSocialId)) {
                    //There is no combination of this social id and provider, so create new one
                    $newSocialUser = new User;
                    $newSocialUser->email = $user->email;
                    if(is_array(explode(' ', $user->name))){
                        $name = explode(' ', $user->name);
                        $newSocialUser->firstname = ucfirst($name[0]);
                        $newSocialUser->lastname = ucfirst($name[1]);
                    }

                    //Users password
                    $userPassword = Str::random(8);

                    $newSocialUser->password = Hash::make($userPassword);
                    $newSocialUser->avatar = $user->avatar;
                    $newSocialUser->type = 'P';
                    $newSocialUser->active = true;

                    $newSocialUser->save();
                    $socialData = new Social;
                    $socialData->social_id = $user->id;
                    $socialData->provider= $provider;
                    $newSocialUser->social()->save($socialData);
                    // Assign role
                    $role = Role::whereSlug('member')->first();
                    $newSocialUser->assignRole($role);
                    $socialUser = $newSocialUser;
                    //Send password to user

                    $data = [
                        'username'    => $user->name,
                        'password' => $userPassword,
                        'subject'       => 'Nouveau compte',
                        'email'         => $user->email
                    ];
                    if (! is_null($user->email)) {
                        $this->accountMailer->newAccount($user->email, $data);
                    }

                } else {
                    //Load this existing social user
                    $socialUser = $sameSocialId->user;
                }
            }
            // Loggin the use if the account is active only
            if(User::where('email','=',$socialUser->email)->first()->active == true){
                $this->auth->login($socialUser, true);
                return redirect()->route('profile.index');
            } else{
                return redirect()->route('home')
                    ->with('message','Votre compte a été désactivé. Contactez l\'administrateur!')
                    ->with('type', 'warning');
            }
            return $this->auth->abort(500);
        }catch (Exception $e){
            return redirect()->route('home')
                    ->with('message',"Connexion par reseau social impossible.")
                    ->with('type', 'warning');
        }
    }
}
