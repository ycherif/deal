<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Deal\Users\UserRepository;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use App\Models\Password;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller {

    /**
     * Show forgot password form page
     *
     * @return \Illuminate\View\View
     */
    public function getForgot()
    {
        return view('auth.password-forgot');
    }

    /**
     * @param UserRepository $userRepository
     * @param ForgotPasswordRequest $req
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postForgot(UserRepository $userRepository, ForgotPasswordRequest $req)
    {
        $email  = $req->input('email');
        $user   = User::where('email', '=', $email)->firstOrFail();
        if(empty($user) || is_null($user)) {
            // Store message in log
            supervisor('Impossible de réinitialier un compte via l\'email','warning');
            return redirect()->back()
                ->with('type', 'danger')
                ->with('message','Cette adresse email n\'existe pas!');
        }
        $userRepository->resetPassword($user);
        return redirect()->back()
            ->with('type', 'success')
            ->with('message', 'Nous vous avons envoyé un lien de réinitialisation de mot de passe par email!');
    }

    /**
     * Reset user password form page
     *
     * @param $token
     * @return \Illuminate\View\View
     */
    public function getReset($token)
    {
        return view('auth.password-reset', compact('token'));
    }

    /**
     * Reset user password
     *
     * @param ResetPasswordRequest $req
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postReset(ResetPasswordRequest $req)
    {
        $password = Password::where('token', '=', $req->input('token'))->firstOrFail();
        if(empty($password)) {
            // Store message in log
            supervisor('Impossible de réinitialier un mot de passe de compte','danger');
            return redirect()->route('home')
                ->with('type', 'danger')
                ->with('message', 'Vous n\'avez pas cliqué sur le bon lien !');
        }
        $user = User::where('email', '=', $password->email)->firstOrFail();
        $user->password = Hash::make($req->input('password'));
        $user->save();
        $password->delete();
        return redirect()->route('auth.login')
            ->with('type', 'success')
            ->with('message', 'Votre mot de passe a bien été modifié.');
    }
}