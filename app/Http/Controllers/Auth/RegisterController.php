<?php
namespace App\Http\Controllers\Auth;
use App\Models\City;
use App\Models\User;
use App\Deal\Users\UserRepository;
use App\Deal\Mailers\AccountMailer;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Mailer service instance
     *
     * @var AccountMailer
     */
    protected $accountMailer;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $userRepository;

    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, AccountMailer $accountMailer )
    {
        $this->auth = auth();
        $this->userRepository = $userRepository;
        $this->accountMailer = $accountMailer;
        $this->middleware('guest');
    }

    /**
     * Show register page
     *
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        $cities = City::all();
        return view('auth.register',compact('cities'));
    }

    /**
     * Register new user
     *
     * @param RegisterRequest $req
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postRegister(RegisterRequest $req)
    {
        $isAdmin = false;
        $firstTime = true;
        
        $newUser = [
            'username'      => $req->input('username'),
            'firstname'     => $req->input('firstname'),
            'lastname'      => $req->input('lastname'),
            'email'         => $req->input('email'),
            'password'      => $req->input('password'),
            'city_id'       => $req->input('city_id'),
            'type'          => $req->input('type'),
            'phone'         => $req->input('phone'),
            'gender'        => $req->input('gender')
        ];

        // Check if user is soft deleted and not banned by admin
        $userExist = User::onlyTrashed()->where('email','=',$req->input('email'))->first();
        if(!is_null($userExist)){
            if($userExist->banned == true){
                return redirect()->route('home')
                    ->with('type', 'danger')
                    ->with('message', 'Votre compte à été bani du site. Contactez le support pour plus d\'info!');
            }
            $firstTime = false;
        } else{
            // Validate
            $validation = Validator::make(['email' => $req->input('email'), 'username' => $req->input('username')], ['email' => 'unique:users','username' => 'unique:users']);
            if($validation->fails()){
                return redirect()->route('auth.register')
                    ->withErrors($validation)
                    ->withInput();
            }
        }

        // Validate
        $validation = Validator::make(['username' => $req->input('username')], ['username' => 'unique:users']);
        if($validation->fails()){
            return redirect()->route('auth.register')
                ->withErrors($validation)
                ->withInput();
        }
        //dd('dfr');
        if($this->userRepository->register($newUser, $isAdmin, $firstTime)){
            return redirect()->route('home')
                ->with('type', 'success')
                ->with('message', 'Votre compte à bien été créé. Nous vous avons envoyé un mail de confirmation.');
        } else{
            // Store message in log
            supervisor('Impossible de créer un utilisateur. (AuthController@postRegister)','warning');
            return redirect()->route('auth.register')
                ->with('type', 'danger')
                ->with('message', 'Le compte n\'a pas été créé. Rééssayez plus tard!');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}