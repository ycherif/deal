<?php

namespace App\Http\Controllers\Back;

use App\Deal\Ads\AdRepository;
use App\Http\Requests\DeleteAdRequest;
use App\Models\Ad;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

class AdminsAdsController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $adRepository;

    /**
     * @param Guard $auth
     * @param AdRepository $adRepository
     */
    public function __construct(Guard $auth,AdRepository $adRepository )
    {
        $this->adRepository = $adRepository;
        $this->auth = $auth;
    }

    /**
     * Display a listing of ads.
     *
     * @return Response
     */
    public function index()
    {
        $ads = Ad::orderBy('created_at', 'DESC')->paginate(config('deal.back-admin-ads-pagination'));
        return view('back.admins.ads.index',compact('ads'));
    }

    /**
     * Approve new ad.
     *
     * @param $id
     * @return mixed
     */
    public function approve($id)
    {
        try{
            $ad = Ad::findOrFail($id);
            $ad->status = true;
            $ad->published_at = Carbon::now();
            $ad->moderated_by = $this->auth->user()->id;
            $ad->save();
            return Redirect::back()
                ->with('message', 'L\'annonce a bien été approvée!')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible d\'approuver une annonce. (AdminsAdsController@approve)','warning');
            return Redirect::back()
                ->with('message', 'L\'annonce n\'a pas pu être approuvée. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }
    }

    /**
     * Disapprove existing ad.
     *
     * @param $id
     * @return mixed
     */
    public function disapprove($id)
    {
        try{
            $ad = Ad::findOrFail($id);
            $ad->status = false;
            $ad->moderated_by = $this->auth->user()->id;
            $ad->save();
            return Redirect::back()
                ->with('message', 'L\'annonce a bien été désapprovée!')
                ->with('type', 'success');
        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible de désapprouver une annonce. (AdminsAdsController@disapprove)','warning');
            return Redirect::back()
                ->with('message', 'L\'annonce n\'a pas pu être désapprouvée. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }
    }

    /**
     * Get published ads.
     *
     * @return \Illuminate\View\View
     */
    public function published()
    {
        $ads = Ad::where('status',true)->orderBy('created_at', 'DESC')->paginate(config('deal.back-admin-published-pagination'));
        return view('back.admins.ads.published',compact('ads'));
    }

    /**
     * Get pending ads.
     *
     * @return \Illuminate\View\View
     */
    public function pending()
    {
        $ads = Ad::where('status',false)->orderBy('created_at', 'DESC')->paginate(config('deal.back-admin-pending-pagination'));
        return view('back.admins.ads.pending',compact('ads'));
    }

    /**
     * Get pending shop's ads.
     *
     * @return \Illuminate\View\View
     */
    public function shopAd()
    {
        $ads = Ad::whereNotNull('shop_slug')->where('status',false)->orderBy('created_at', 'DESC')->paginate(config('deal.back-admin-pending-pagination'));
        return view('back.admins.ads.shopAd',compact('ads'));
    }

    /**
     * Remove the specified ad from storage.
     *
     * @param DeleteAdRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(DeleteAdRequest $req, $id)
    {
        if($req->input('ad_id') == $id && !is_null(Ad::find($id))){
            if($this->adRepository->destroyAdAdmin($id)){
                return redirect()->route('admin.ads.index')
                    ->with('message', 'Votre annonce a bien été supprimer.')
                    ->with('type', 'success');
            } else {
                return redirect()->route('admin.ads.index')
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        } else{
            // Store message in log
            supervisor('Impossible de supprimer une annonce. (AdminsAdsController@destroy)','warning');
            return redirect()->route('admin.ads.index')
                ->with('message', 'Une érreur est survenue. L\'annonce que vous essayez de supprimer n\'existe pas ou ne vous appartient pas!')
                ->with('type', 'danger');
        }
    }

    public function deleteAds()
    {
        dd();
    }
}
