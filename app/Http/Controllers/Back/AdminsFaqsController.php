<?php

namespace App\Http\Controllers\Back;

use App\Deal\Mailers\ContactMailer;
use App\Http\Requests\AdminsCreateFaqRequest;
use App\Models\Faq;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminsFaqsController extends Controller
{
    /**
     * ContactMailer instance
     *
     * @var ContactMailer
     */
    protected $contactMailer;

    /**
     * @param ContactMailer $contactMailer
     */
    public function __construct(ContactMailer $contactMailer)
    {
        $this->contactMailer = $contactMailer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(config('back-admin-faq-pagination'));
        return view('back.admins.faq.index',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('back.admins.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AdminsCreateFaqRequest $req
     * @return Response
     */
    public function store(AdminsCreateFaqRequest $req)
    {
        try{
            $faq = new Faq();
            $faq->question = $req->input('question');
            $faq->response = $req->input('response');
            $faq->save();

            return Redirect::route('admin.faq')
                ->with('message', 'La question a bien été créée !')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible de créer une question dans la faq. (AdminsFaqsController@store)','warning');
            return Redirect::back()
                ->with('message', 'La question n\'a pas pu être créée. Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        return view('back.admins.faq.edit',compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdminsCreateFaqRequest  $req
     * @param  int  $id
     * @return Response
     */
    public function update(AdminsCreateFaqRequest $req, $id)
    {
        try{
            $faq = Faq::findOrFail($id);
            $faq->question = $req->input('question');
            $faq->response = $req->input('response');
            $faq->save();

            return redirect()->route('admin.faq')
                ->with('message', 'La question a bien été mise à jour !')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible de mettre à jour une question de la faq. (AdminsFaqsController@update)','warning');
            return redirect()->back()
                ->with('message', 'La question n\'a pas pu être mise à jour . Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }

    /**
     * Delete question.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $faq = Faq::where('id','=',$id)->first();

            $faq->delete();
            DB::commit();
            return redirect()->back()
                ->with('message', 'La question a bien été supprimé !')
                ->with('type', 'success');

        } catch (Exception $e) {
            DB::rollback();
            // Store message in log
            supervisor('Impossible de supprimer une question de la faq. (AdminsFaqsController@destroy)','warning');
            return redirect()->back()
                ->with('message', 'La question n\'a pas pu être supprimée. Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }

    /**
     * Send Mail to user.
     *
     * @param  int  $id
     * @return Response
     */
    public function sendMail($id)
    {
        $faq = Faq::findOrFail($id);

        $data = [
            'question' => $faq->question,
            'response' => $faq->response,
            'email' => $faq->email
        ];

        if($this->contactMailer->contactFaq($data)){
            return redirect()->back()
                ->with('message', 'La response a été envoyé avec succée!')
                ->with('type', 'success');
        }else{
            // Store message in log
            supervisor('Impossible d\'envoyer un mail de réponse a une question (faq). (AdminsFaqsController@sendMail)','warning');
        return redirect()->back()
            ->with('message', 'La response n\'a pas été envoyé. Merci de rééssayer plus tard !!')
            ->with('type', 'warning');
        }
    }

    /**
     * add question to Faq.
     *
     * @param  int  $id
     * @return Response
     */
    public function publish($id)
    {
        try{
            $faq = Faq::findOrFail($id);
            $faq->status = true;
            $faq->save();
            return redirect()->back()
                ->with('message', 'La question a bien été publiée !')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible de publier une réponse dans la faq. (AdminsFaqsController@publish)','warning');
            return redirect()->back()
                ->with('message', 'La question n\'a pas pu être publier . Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }

    /**
     * remove question to Faq.
     *
     * @param  int  $id
     * @return Response
     */
    public function unpublish($id)
    {
        try{
            $faq = Faq::findOrFail($id);
            $faq->status = false;
            $faq->save();
            return redirect()->back()
                ->with('message', 'La question a bien été retirée !')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible de retirer une réponse dans une faq. (AdminsFaqsController@unpublish)','warning');
            return redirect()->back()
                ->with('message', 'La question n\'a pas pu être retirer . Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }
}
