<?php

namespace App\Http\Controllers\Back;

use App\Deal\Mailers\AccountMailer;
use App\Http\Requests\ShopCodeValidationRequest;
use App\Models\Invoice;
use App\Models\Shop;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminsInvoicesController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    public function __construct( Guard $auth, AccountMailer $accountMailer)
    {
        $this->accountMailer = $accountMailer;
        $this->auth = $auth;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $invoices = Invoice::orderBy('created_at','DESC')->paginate(config('deal.back-admin-invoices-pagination'));
        return view('back.admins.invoices.index', compact('invoices'));
    }

    public function overDate()
    {
        $today = new DateTime();
        //dd($today);

        $invoices = Invoice::where('expiration_date' ,'<', $today)->orderBy('created_at','DESC')->paginate(config('deal.back-admin-invoices-pagination'));
        return view('back.admins.invoices.index', compact('invoices'));
    }


    /**
     * @param ShopCodeValidationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(ShopCodeValidationRequest $request)
    {
        DB::beginTransaction();
        //try{
            $invoice = Invoice::find($request->invoiceId);

            if ($request->invoiceCode === $invoice->code){

                $shop = Shop::find($invoice->shop_id);
                $shop->status = true;
                $invoice->status = true;
                $invoice->expiration_date = Carbon::now();
                $shop->published_at = Carbon::now();
                setlocale(LC_TIME,"fr_FR.UTF-8");
                $data = [
                    'subject'=>'Confirmation Paiement boutique ',
                    'shop' => $shop->name,
                    'begin'=>date('l j F Y,H:i:s', strtotime($invoice->updated_at)),
                    'end'=>date('l j F Y,H:i:s',strtotime($invoice->expiration_date))
                ];

                $invoice->save();
                $shop->save();
                DB::commit();
                $this->accountMailer->shopInvoice($shop->email,$data);
                return redirect()->back()
                    ->with('message', 'Le paiement est validé')
                    ->with('type', 'success');
            }else
                return redirect()->back()
                    ->with('message', 'Le code de verification n\'est pas correct')
                    ->with('type', 'danger');
        /*}catch (Exception $ex){
            DB::rollback();
            // Store message in log
            supervisor('Impossible d\'approuver un paiement. (AdminsInvoicesController@approve)');
            return redirect()->back()
                ->with('message', 'Le paiement n\'a pas pu être approuvé. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }*/
    }

    /**
     * Disapprove invoice.
     *
     * @param $id
     * @return mixed
     */
    public function disapprove($id)
    {
        DB::beginTransaction();
        try{
            // disapprove and invoice and desactive the linked shop if there is no other invoice
            $invoice = Invoice::findOrFail($id);
            $invoice->status = false;
            $invoice->save();

            $shop = Shop::where('id','=',$invoice->shop_id)->first();

            if(is_null(Invoice::where('id','!=',$invoice->id)->where('shop_id',$shop->id)->where('status',true)->first())){
                $shop->status = false;
                $shop->published_at = Carbon::now();
                $shop->save();
            }

            DB::commit();
            return redirect()->back()
                ->with('message', 'Le paiement a bien été désapprové!')
                ->with('type', 'success');
        }catch (Exception $ex){
            DB::rollback();
            // Store message in log
            supervisor('Impossible de désapprouver un paiement. (AdminsInvoicesController@disapprove)');
            return redirect()->back()
                ->with('message', 'Le paiement n\'a pas pu être désapprouvé. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }
    }



}
