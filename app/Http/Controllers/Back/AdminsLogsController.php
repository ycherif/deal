<?php

namespace App\Http\Controllers\Back;

use App\Models\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminsLogsController extends Controller
{
    /**
     * Display a listing of the logs.
     *
     * @return Response
     */
    public function index()
    {
        $logs = Log::paginate(config('deal.back-admin-logs-pagination'));
        return view('back.admins.logs.index',compact('logs'));
    }

    /**
     * Remove the specified log from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $log = Log::findOrFail($id);
            $log->delete();
            return redirect()->back()
                ->with('message', 'L\'érreur a bien été supprimée.')
                ->with('type', 'success');
        }catch (Exception $e){
            // Store message in log
            supervisor('Impossible de supprimer un log. (AdminsLogsController@destroy)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Impossible de supprimer l\'érreur!')
                ->with('type', 'danger');
        }
    }

}
