<?php

namespace App\Http\Controllers\Back;

use App\Deal\Shops\ShopRepository;
use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\DeleteAdRequest;
use App\Http\Requests\DeleteShopRequest;
use App\Models\Shop;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminsShopsController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $shopRepository;
    protected $auth;

    /**
     * @param ShopRepository $shopRepository
     * @param Guard $auth
     */
    public function __construct(ShopRepository $shopRepository, Guard $auth)
    {
        $this->shopRepository = $shopRepository;
        $this->auth = $auth;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $isAdmin = true;
        $shops = Shop::paginate(config('back-shop-index'));
        return view('back.shops.index', compact('shops','isAdmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateShopRequest $req
     * @param  int $id
     * @return Response
     * @internal param Request $request
     */
    public function update(CreateShopRequest $req, $id)
    {
        $data = [
            'name' => $req->input('name'),
            'description' => $req->input('description'),
            'phone' => $req->input('phone'),
            'email' => $req->input('email'),
            'address1' => $req->input('address1'),
            'category_name' => $req->input('category'),
            'city_name' => $req->input('city')
        ];

        $data['address2'] = !empty($req->input('address2')) ? $req->input('address2') : '';
        $data['address3'] = !empty($req->input('address3')) ? $req->input('address3') : '';
        $data['logo'] = !empty($req->file('logo')) ? $req->file('logo') : '';
        $data['site'] = !empty($req->input('site')) ? $req->input('site') : '';

        if ($this->shopRepository->updateShop($data, $id, true)) {
            return redirect()->route('admin.shops')
                ->with('message', 'La boutique a bien été mise à jour.')
                ->with('type', 'success');
        } else {
            return redirect()->route('admin.shops')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Remove the specified shop from storage.
     *
     * @param DeleteShopRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(DeleteShopRequest $req, $id)
    {
        if($req->input('shop_id') == $id && !is_null(Shop::where('id','=',$id)->firstOrFail())){
            if($this->shopRepository->destroyShopAdmin($id)){
                return redirect()->back()
                    ->with('message', 'La boutique a bien été supprimée.')
                    ->with('type', 'success');
            } else {
                return redirect()->back()
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        } else{
            // Store message in log
            supervisor('Impossible de supprimer une boutique inexistante ou non propriétaire. (AdminsShopsController@destroy)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. La boutique que vous éssayez de supprimer n\'existe pas ou ne vous appartient pas!')
                ->with('type', 'danger');
        }
    }

    /**
     * Approve shop.
     *
     * @param $id
     * @return mixed
     */
    public function approve($id)
    {
        try{
            $shop = Shop::findOrFail($id);
            $shop->status = true;
            $shop->published_at = Carbon::now();
            $shop->save();
            return redirect()->back()
                ->with('message', 'La boutique a bien été approvée!')
                ->with('type', 'success');

        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible d\'approurver une boutique. (AdminsShopsController@approve)');
            return redirect()->back()
                ->with('message', 'La boutique n\'a pas pu être approuvée. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }
    }

    /**
     * Disapprove show.
     *
     * @param $id
     * @return mixed
     */
    public function disapprove($id)
    {
        try{
            $shop = Shop::findOrFail($id);
            $shop->status = false;
            $shop->save();
            return redirect()->back()
                ->with('message', 'La boutiquea bien été désapprovée!')
                ->with('type', 'success');
        }catch (Exception $ex){
            // Store message in log
            supervisor('Impossible désapprourver une boutique. (AdminsShopsController@disapprove)');
            return redirect()->back()
                ->with('message', 'La boutique n\'a pas pu être désapprouvée. Merci de rééssayer plus tard!')
                ->with('type', 'warning');
        }
    }
}
