<?php

namespace App\Http\Controllers\Back;

use App\Models\Ad;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use \Khill\Lavacharts\Lavacharts;



class AdminsStatsController extends Controller
{
    /**
     * Display user's view.
     *
     * @return Response
     */
    public function user()
    {
        $chartUsers = User::select([
            DB::raw('DATE_FORMAT(created_at, "%Y-%m") as date'),
            DB::raw('COUNT(id) as count'),
        ])
            ->whereBetween('created_at', [Carbon::now()->subMonth(12), Carbon::now()])
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();

        $registerByMonth = [];

        $date = Carbon::now();
        for ($i = 0; $i < 12; $i++) {
            $dateString = $date->format('Y-m');
            if (isset($chartUsers[$i]['date']))
                $registerByMonth[$dateString] = $chartUsers[$i]['count'];
            else
                $registerByMonth[$dateString] = 0;

            $date->subMonth();
        }
        $registerByMonth = array_reverse($registerByMonth);

        $users = User::all();
        $months = ["", "Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"];

        $labels=[];
        foreach(array_keys($registerByMonth) as $date){
            $labels[] = $months[(int)explode("-", $date)[1]].' '.explode("-", $date)[0];
        }

        $datasRegister=[];
        foreach(array_values($registerByMonth) as $data){
            $datasRegister[] = $data;
        }

        return view('back.admins.stats.user',compact('users','labels','datasRegister'));
    }


    /**
     * Display ad's stats.
     *
     * @return Response
     */
    public function ad()
    {
        $chartAds = Ad::select([
            DB::raw('DATE_FORMAT(created_at, "%Y-%m") as date'),
            DB::raw('COUNT(id) as count'),
        ])
            ->whereBetween('created_at', [Carbon::now()->subMonth(12), Carbon::now()])
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();

        $adInMonth = [];

        $date = Carbon::now();
        for ($i = 0; $i < 12; $i++) {
            $dateString = $date->format('Y-m');
            if (isset($chartAds[$i]['date']))
                $adInMonth[$dateString] = $chartAds[$i]['count'];
            else
                $adInMonth[$dateString] = 0;

            $date->subMonth();
        }
        $adInMonth = array_reverse($adInMonth);

        $ads = Ad::all();
        $months = ["", "Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"];

        $labels="";
        foreach(array_keys($adInMonth) as $date){
            $labels[] = $months[(int)explode("-", $date)[1]].' '.explode("-", $date)[0];
        }

        $datasAds="";
        foreach(array_values($adInMonth) as $data){
            $datasAds[] = $data;
        }

        return view('back.admins.stats.ad',compact('ads','labels','datasAds'));
    }
}
