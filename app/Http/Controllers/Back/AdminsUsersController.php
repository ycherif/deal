<?php

namespace App\Http\Controllers\Back;

use App\Deal\Mailers\ContactMailer;
use App\Deal\Users\UserRepository;
use App\Http\Requests\AdminsCreateUsersRequest;
use App\Http\Requests\AdminsDeleteUsersRequest;
use App\Http\Requests\AdminsUpdateUsersRequest;
use App\Http\Requests\ContactModeratorRequest;
use App\Http\Requests\Request;
use App\Models\Role;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminsUsersController extends Controller
{
    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     * @param ContactMailer $contactMailer
     */
    public function __construct(UserRepository $userRepository,ContactMailer $contactMailer)
    {
        $this->userRepository = $userRepository;
        $this->contactMailer = $contactMailer;

    }

    /**
     * Display all users(without admins).
     *
     * @return Response
     */
    public function index()
    {
        $users = User::orderBy('created_at','DESC')
            ->whereHas('roles',function($query){
                $query->where('slug','!=','admin');
            })
            ->with('roles')
            ->withTrashed()
            ->paginate(config('deal.back-admin-users-pagination'));
        return view('back.admins.users.index', compact('users'));
    }

    public function moderators()
    {
        $users = User::orderBy('created_at','DESC')
            ->whereHas('roles',function($query){
                $query->where('slug','=','moderator');
            })
            ->with('roles')
            ->withTrashed()
            ->paginate(config('deal.back-admin-users-pagination'));
        return view('back.admins.users.moderators', compact('users'));
    }

    /**
     *
     * Admin contact a moderator by Email
     * @param ContactModeratorRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function revokeModerator($id){
        $user = User::findOrFail($id);
        $role = Role::where('slug','=','moderator')->get();
         $user->removeRole($role);


    }
    public function contactModerator(ContactModeratorRequest $req)
    {

        $data = [
            'receiver'  =>$req->receiver,
            'username'  =>$req->username,
            'subject'   =>'Administrateur PinsDeal '.config('deal.country'),
            'sender'    =>$req->sender,
            'phone'     =>$req->phone,
            'message'   =>$req->message,
        ];

        if ($this->contactMailer->contactModerators($data)){
            return redirect()->back()
                ->with('type', 'success')
                ->with('message', 'Le mail a bien été envoyé au modérateur');
        }else{
            supervisor('Impossible d\'envoyer un mail. (AdminsUsersController@contactModerator)');
            return redirect()->back()
                ->with('type', 'danger')
                ->with('message', 'Le mail n\'a pas été envoyé. Veuillez réessaye plus tard');
        }

    }
    /**
     * Show create page.
     *
     * @return Response
     */
    public function create()
    {
        return view('back.admins.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminsCreateUsersRequest $req
     * @return Response
     */
    public function store(AdminsCreateUsersRequest $req)
    {
        $isAdmin = true;
        $firstTime = false;

        $newUser = [
            'username'      => $req->input('username'),
            'firstname'     => $req->input('firstname'),
            'lastname'      => $req->input('lastname'),
            'email'         => $req->input('email'),
            'city_id'       => $req->input('city_id'),
            'type'          => $req->input('type'),
            'phone'         => $req->input('phone'),
            'gender'        => $req->input('gender'),
            'userRole'      => $req->input('role_slug')
        ];

        if($this->userRepository->register($newUser,$isAdmin,$firstTime)){
            return redirect()->route('admin.users.index')
                ->with('type', 'success')
                ->with('message', 'Le compte de l\'utilisateur a bien été créé. Nous lui avons envoyé un email avec ses identifiants.');
        } else{
            // Store message in log
            supervisor('Impossible de créer un compte. (AdminsUsersController@store)');
            return redirect()->route('admin.users.create')
                ->with('type', 'danger')
                ->with('message', 'Le compte n\'a pas été créé. Rééssayez plus tard!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('back.admins.users.edit',compact('user'));
    }

    /**
     * Update specified user.
     *
     * @param AdminsUpdateUsersRequest $req
     * @param  int $id
     * @return Response
     */
    public function update(AdminsUpdateUsersRequest $req, $id)
    {
        if($req->input('user_id') == $id){
            $updatedUser = [
                'username'      => $req->input('username'),
                'firstname'     => $req->input('firstname'),
                'lastname'      => $req->input('lastname'),
                'type'          => $req->input('type'),
                'email'         => $req->input('email'),
                'city_id'       => $req->input('city_id'),
                'phone'         => $req->input('phone'),
                'userRole'      => $req->input('role_slug'),
                'user_id'       => $id
            ];

            $updatedUser['changePassword'] = $req->input('changePassword') ? true : false;

            if($this->userRepository->updateProfile($updatedUser,true)){
                return redirect()->route('admin.users.index')
                    ->with('type', 'success')
                    ->with('message', 'Le compte de l\'utilisateur a bien été mis à jour. Nous luis avons envoyé un email avec ses nouvelles informations.');
            }
        }
        // Store message in log
        supervisor('Impossible de mettre à jour un compte. (AdminsUsersController@update)');
        return redirect()->route('admin.users.index')
            ->with('type', 'danger')
            ->with('message', 'Le compte n\'a pas été mis à jour. Rééssayez plus tard!');
    }

    /**
     * Remove specified user.
     *
     * @param AdminsDeleteUsersRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(AdminsDeleteUsersRequest $req,$id)
    {
        if($req->input('user_id') == $id)
        {
            if($this->userRepository->deleteAccount($id, true)){
                return redirect()->back()
                    ->with('message', 'Le compte a bien été supprimer.')
                    ->with('type', 'success');
            }
        }
        // Store message in log
        supervisor('Impossible de supprimer un compte. (AdminsUsersController@destroy)');
        return redirect()->back()
            ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
            ->with('type', 'danger');
    }

    /**
     * Restore trashed user
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        try{
            $user = User::where('id','=',$id)->onlyTrashed()->firstOrFail();
            $user->restore();
            $user->banned = false;
            $user->active = true;
            $user->save();
            return redirect()->route('admin.users.index')
                ->with('message', 'Le compte a bien été restaurer.')
                ->with('type', 'success');
        } catch(Exception $e){
            // Store message in log
            supervisor('Impossible de restaurer un compte. (AdminsUsersController@restore)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }
}
