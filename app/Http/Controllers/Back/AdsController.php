<?php

namespace App\Http\Controllers\Back;

use App\Deal\Ads\AdRepository;
use App\Http\Requests\CreateAdRequest;
use App\Http\Requests\DeleteAdRequest;
use App\Http\Requests\UpdateAdRequest;
use App\Models\Ad;
use App\Models\Category;
use App\Models\City;
use App\Models\Shop;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redirect;

class AdsController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $adRepository;

    public function __construct( Guard $auth, AdRepository $adRepository )
    {
        $this->auth = $auth;
        $this->adRepository = $adRepository;
    }

    /**
     * Display all user's ads.
     *
     * @return Response
     */
    public function index()
    {
        $ads = $this->auth->user()->ads()->where('shop_slug')->with('medias')->orderBy('created_at', 'DESC')->paginate(config('deal.back-ads-pagination'));
        return view('back.ads.index',compact('ads'));
    }

    /**
     * Create Ad page.
     *
     * @param $isShop
     * @return Response
     */
    public function create($hasShops = null)
    {
        $userCity = City::find($this->auth->user()->city_id);
        $categories = Category::isParent()->with('children')->get();
        $cities = City::all();
        if($hasShops){
            $shops = Shop::where('user_id','=',$this->auth->user()->id)->get();
            return view('back.ads.create',compact('userCity','categories','cities','shops'));
        }
        return view('back.ads.create',compact('userCity','categories','cities'));
    }

    /**
     * Store a newly created ad.
     *
     * @param CreateAdRequest $req
     * @return Response
     */
    public function store(CreateAdRequest $req)
    {
        if($req->file('medias')[0] && count($req->file('medias')) > 5){
            return redirect()->back()
                ->withErrors(['Désolé vous ne pouvez pas ajouter plus de 5 images !!'])
                ->withInput();
        }

        $data = [
            'title' => $req->input('title'),
            'description' => $req->input('description'),
            'price' => $req->input('price'),
            'type' => $req->input('type'),
            'category_id' => $req->input('category'),
            'city_id' => $req->input('city'),
            'hide_phone' => ($req->input('hidephone')) ? true : false,
            'negotiable' => ($req->input('negotiable')) ? true : false,
            'medias' => $req->file('medias')
        ];

        // if the slug shop exist and the auth user is the owner
        if(!empty($req->input('shop'))){
            $shop = Shop::where('slug',$req->input('shop'))->where('user_id',$this->auth->user()->id)->first();
            if(!is_null($shop))
                $data['shop_slug'] = !empty($req->input('shop')) ? $req->input('shop') : '';
        }
       // dd($data);
        if($this->adRepository->storeAd($data)){
            return redirect()->back()
                ->with('message', 'Félicitation! Votre annonce sera en ligne après vérification par nos services de modération.')
                ->with('type', 'success');
        } else {
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Get published ads pages
     *
     * @return \Illuminate\View\View
     */
    public function published()
    {
        $ads = $this->auth->user()->ads()->where('shop_slug')->with('medias')->where('status',true)->orderBy('created_at', 'DESC')->paginate(config('deal.back-published-pagination'));
        return view('back.ads.published',compact('ads'));
    }

    /**
     * Get pending ads pages
     *
     * @return \Illuminate\View\View
     */
    public function pending()
    {
        $ads = $this->auth->user()->ads()->where('shop_slug')->with('medias')->where('status',false)->orderBy('created_at', 'DESC')->paginate(config('deal.back-pending-pagination'));
        return view('back.ads.pending',compact('ads'));
    }

    /**
     * Display the specified ad.
     *
     * @param $slug
     * @return Response
     */
    public function preview($slug)
    {
        $ad = Ad::preview($slug)->first();
        if(is_null($ad)){
            return Redirect::back()
                ->with('message', 'L\'annonce n\'existe pas!')
                ->with('type', 'warning');
        }elseif($this->auth->user()->hasRole('moderator') || $this->auth->user()->hasRole('admin') || $ad->user_id === $this->auth->user()->id){
            $parentCategory = Category::findOrFail($ad->category->parent_id);
            return view('back.ads.preview',compact('ad','parentCategory'));
        }else{
            return Redirect::back()
                ->with('message', 'Vous n\'êtes pas autorisé à prévisualiser cette annonce!')
                ->with('type', 'warning');
        }
    }

    /**
     * Show the form for editing the specified ad.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $ad = Ad::where('id','=',$id)->where('user_id','=',$this->auth->user()->id)->first();
        if(!is_null($ad)){
            $userCity = City::findOrFail($ad->city_id);
            $categories = Category::isParent()->where('id','!=','1')->with('children')->get();
            $cities = City::where('id','!=','1')->get();
            if(!empty($ad->shop_slug)){
                $shops = Shop::where('user_id','=',$this->auth->user()->id)->get();
                return view('back.ads.edit',compact('userCity','categories','cities','ad','shops'));
            }
            return view('back.ads.edit',compact('userCity','categories','cities','ad'));
        }else{
            return redirect()->route('ads.index')
                ->with('message', 'Une érreur est survenue. L\'annonce que vous éssayez d\'éditer n\'existe pas ou plus!')
                ->with('type', 'danger');
        }
    }

    /**
     * Update the specified ad in storage.
     *
     * @param UpdateAdRequest $req
     * @param  int $id
     * @return Response
     */
    public function update(UpdateAdRequest $req, $id)
    {
        $data = [
            'title' => $req->input('title'),
            'description' => $req->input('description'),
            'price' => $req->input('price'),
            'type' => $req->input('type'),
            'category_id' => $req->input('category'),
            'city_id' => $req->input('city'),
            'hide_phone' => ($req->input('hidephone')) ? true : false,
            'negotiable' => ($req->input('negotiable')) ? true : false,
            'medias' => $req->file('medias')
        ];

        // if the slug shop exist and the auth user is the owner
        if(!empty($req->input('shop'))){
            $shop = Shop::where('slug',$req->input('shop'))->where('user_id',$this->auth->user()->id)->first();
            if(!is_null($shop))
                $data['shop_slug'] = !empty($req->input('shop')) ? $req->input('shop') : '';
        }

        if($this->adRepository->updateAd($data,$id)){
            return redirect()->back()
                ->with('message', 'Votre annonce a bien été mise à jour. Elle sera en ligne après vérification par nos services de modération.')
                ->with('type', 'success');
        } else {
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Remove the specified ad from storage.
     *
     * @param DeleteAdRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(DeleteAdRequest $req, $id)
    {
        if($req->input('ad_id') == $id && !is_null(Ad::where('id','=',$id)->where('user_id','=',$this->auth->user()->id)->first())){
            if($this->adRepository->destroyAd($id)){
                return redirect()->route('ads.index')
                    ->with('message', 'Votre annonce a bien été supprimée.')
                    ->with('type', 'success');
            } else {
                return redirect()->route('ads.index')
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        } else{
            // Store message in log
            supervisor('Impossible de supprimer une annonce inexistante ou non propriétaire, côté administrateur');
            return redirect()->route('ads.index')
                ->with('message', 'Une érreur est survenue. L\'annonce que vous éssayez de supprimer n\'existe pas ou ne vous appartient pas!')
                ->with('type', 'danger');
        }
    }
}
