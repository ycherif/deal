<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests\DeleteAdRequest;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Favorite;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class FavoritesController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $adRepository;

    public function __construct( Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Display a listing of the favorites.
     *
     * @return Response
     */
    public function index()
    {
        $favorites = $this->auth->user()->favorites()->pluck('ad_id')->all();
        $ids = array_values($favorites);
        $ads = Ad::whereIn('id',$ids)->orderBy('created_at', 'DESC')->paginate(config('deal.back-favorites-pagination'));
        return view('back.favorites.index',compact('ads'));
    }

    /**
     * Store a newly created favorite in storage.
     *
     * @param $id
     * @return Response
     */
    public function create($id)
    {
        $fav = Favorite::where('user_id','=',$this->auth->user()->id)->where('ad_id','=',$id)->first();
        if($fav){
            return redirect()->back()
                ->with('message', 'Vous avez déjà ajouter cette annonce à vos favoris!')
                ->with('type', 'warning');
        } else{
            $favorite = new Favorite();
            $favorite->user_id = $this->auth->user()->id;
            $favorite->ad_id = $id;

            if($favorite->save()){
                return redirect()->back()
                    ->with('message', 'L\'annonce a bien été ajouter à vos favoris.')
                    ->with('type', 'success');
            } else{
                // Store message in log
                supervisor('Impossible d\'ajouter un favori. (FavoritesController@create)','warning');
                return redirect()->back()
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        }
    }

    /**
     * Display the specified favorite.
     *
     * @param $slug
     * @return Response
     */
    public function show($slug)
    {
        if(Ad::where('slug','=',$slug)->firstOrFail()->status == false){
            return Redirect::back()
                ->with('message', 'L\'annonce est en cours de modération par nos services. Merci de rééssayer quelques heures plus tard!')
                ->with('type', 'warning');
        } else {
            $ad = Ad::detail($slug)->firstOrFail();
            if(!is_null($ad)){
                $parentCategory = Category::findOrFail($ad->category->parent_id);
                return view('pages.detail',compact('ad','parentCategory'));
            } else {
                return redirect()->route('home')
                    ->with('message','Aucune annonce trouvée')
                    ->with('type','warning');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteAdRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(DeleteAdRequest $req, $id)
    {
        $favorite = Favorite::where('user_id','=',$this->auth->user()->id)->where('ad_id','=',$id)->first();
        if($req->input('ad_id') == $id && !is_null($favorite)){
            if($favorite->delete()){
                return redirect()->route('favorites.index')
                    ->with('message', 'L\'annonce a bien été supprimer de vos favoris.')
                    ->with('type', 'success');
            } else {
                return redirect()->route('favorites.index')
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        } else{
            // Store message in log
            supervisor('Impossible de supprimer une annonce favori inexistante ou non propriétaire. (FavoritesController@destroy)');
            return redirect()->route('favorites.index')
                ->with('message', 'Une érreur est survenue. L\'annonce favorite que vous éssayez de supprimer n\'existe pas ou ne vous appartient pas!')
                ->with('type', 'danger');
        }
    }
}
