<?php

namespace App\Http\Controllers\Back;

use App\Deal\Mailers\AccountMailer;
use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Requests\ShopCodeValidationRequest;
use App\Models\Invoice;
use App\Models\Shop;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class InvoicesController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    public function __construct( Guard $auth, AccountMailer $accountMailer)
    {
        $this->accountMailer = $accountMailer;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $invoices = Invoice::where('user_id',$this->auth->user()->id)->orderBy('created_at','Desc')->with('shop')->get();
        return view('back.invoices.index',compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $shops = Shop::where('user_id',$this->auth->user()->id)->get();
        return view('back.invoices.create',compact('shops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateInvoiceRequest $req
     * @return Response
     * @internal param Request $request
     */
    public function store(CreateInvoiceRequest $req)
    {
        $invoice = new Invoice();
        $invoice->shop_id = $req->input('shop');
        $invoice->user_id = $this->auth->user()->id;
        $invoice->code = $req->input('code');
        $invoice->mode = $req->input('mode');
        $invoice->plan = $req->input('plan');

        try {
            $invoice->save();
            return redirect()->route('invoice.index')
                ->with('message', 'Votre paiement a bien été effectué. Votre plan sera effectif dés la vérification du code par nos services.')
                ->with('type', 'success');
        }
        catch(Exception $e){
            // Store message in log
            supervisor('Impossible de créer un paiement. (InvoicesController@store)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Reéssayer plus tard!')
                ->with('type', 'danger');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(ShopCodeValidationRequest $request)
    {

        $invoice = Invoice::find($request->invoiceId);
        try{
            //dd($invoice);
            if ($request->invoiceCode === $invoice->code){
                $shop = Shop::find($invoice->shop_id);
                //dd($shop);
                $shop->status = true;
                $invoice->status = true;

                $data = [
                    'subject'=>'Confirmation Paiement boutique ',
                    'shop' => $shop->name,
                    'begin'=> $invoice->updated_at->toDayDateTimeString(),
                    'end'=>Carbon::parse($invoice->updated_at)->addMonth($invoice->plan)->toDayDateTimeString()
                ];

                $invoice->save();
                $shop->save();
                $this->accountMailer->shopInvoice($shop->email,$data);
                return redirect()->back()
                    ->with('message', 'Le paiement est validé')
                    ->with('type', 'success');
            }else
                return redirect()->back()
                    ->with('message', 'Le code de verification n\'est pas correct')
                    ->with('type', 'danger');
        }catch (Exception $ex){
            echo 'error';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
