<?php

namespace App\Http\Controllers\Back;

use App\Deal\Users\UserRepository;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\City;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;

class ProfilesController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct( Guard $auth, UserRepository $userRepository )
    {
        $this->auth = $auth;
        $this->userRepository = $userRepository;
    }

    /**
     * Show profile home page
     *
     * @return \Illuminate\View\View
     */
    public function getHome()
    {
        $cities = City::all();
        return view('back.profiles.index',compact('cities'));
    }

    /**
     * Update profile information
     *
     * @param UpdateProfileRequest $req
     * @return mixed
     */
    public function postProfile(UpdateProfileRequest $req)
    {
        $data = [
            'username'      => $req->input('username'),
            'firstname'     => $req->input('firstname'),
            'lastname'      => $req->input('lastname'),
            'city_id'       => $req->input('city_id'),
            'phone'         => $req->input('phone'),
            'description'   => $req->input('description'),
            'address'       => $req->input('address'),
            'avatar'        => $req->file('avatar')
        ];

        if($this->userRepository->updateProfile($data)){
            return redirect()->route('profile.index')
                ->with('message', 'Votre profil a bien été mis à jour.')
                ->with('type', 'success');
        } else{
            return redirect()->route('profile.index')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Update profile password
     *
     * @param UpdatePasswordRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPassword(UpdatePasswordRequest $req)
    {
        try{
            $actualPassword = $req->input('actualPassword');
            $user = User::whereId($this->auth->user()->id)->firstOrFail();
            if(Hash::check($actualPassword,$user->password)){
                $user->password = Hash::make($req->input('password'));
                $user->save();
                return redirect()->back()
                    ->with('message', 'Votre mot de passe a bien été mis à jour.')
                    ->with('type', 'success');
            } else{
                throw new ModelNotFoundException();
            }

        } catch(Exception $e){
            return redirect()->back()
                ->with('message', 'Le mot de passe actuel est incorrect!')
                ->with('type', 'warning');

        }
    }

    /**
     * Show application delete account page.
     *
     * @return \Illuminate\View\View
     */
    public function delete()
    {
        $city = City::find($this->auth->user()->city_id);
        return view('back.profiles.close',compact('city'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
        if($this->userRepository->deleteAccount()){
            return redirect()->route('home')
                ->with('message', 'Votre compte a bien été supprimer.')
                ->with('type', 'success');
        } else{
            return redirect()->route('user.home')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }
}
