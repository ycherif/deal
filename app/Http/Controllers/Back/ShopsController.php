<?php

namespace App\Http\Controllers\Back;

use App\Deal\Shops\ShopRepository;
use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\DeleteShopRequest;
use App\Models\Ad;
use App\Models\Category;
use App\Models\City;
use App\Models\Shop;
use Illuminate\Contracts\Auth\Guard;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{

    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $shopRepository;
    protected $auth;

    /**
     * @param ShopRepository $shopRepository
     * @param Guard $auth
     */
    public function __construct(ShopRepository $shopRepository, Guard $auth)
    {
        $this->shopRepository = $shopRepository;
        $this->auth = $auth;
    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $shops = Shop::where('user_id',$this->auth->user()->id)->paginate(config('back-shop-index'));
        return view('back.shops.index',compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::isParent()->get();
        $cities = City::all();
        return view('back.shops.create', compact('categories','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateShopRequest $req
     * @return Response
     * @internal param Request $request
     */
    public function store(CreateShopRequest $req)
    {

        $data = [
            'name' => $req->input('name'),
            'description' => $req->input('description'),
            'phone' => $req->input('phone'),
            'email' => $req->input('email'),
            'address1' => $req->input('address1'),
            'category_name' => $req->input('category'),
            'city_name' => $req->input('city')
        ];


        $data['address2'] = !empty($req->input('address2')) ? $req->input('address2') : '';
        $data['address3'] = !empty($req->input('address3')) ? $req->input('address3') : '';
        $data['logo'] = !empty($req->file('logo')) ? $req->file('logo') : '';
        $data['site'] = !empty($req->input('site')) ? $req->input('site') : '';

        if ($this->shopRepository->storeShop($data)) {
            return redirect()->route('shop.index')
                ->with('message', 'Votre boutique a bien été crée. Merci d\'aller dans paiement pour selectionner le pack')
                ->with('type', 'success');
        } else {
            // Store message in log
            supervisor('Impossible de créer une boutique. (ShopsController@store)');
            return redirect()->route('shop.index')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }

    }

    /**
     * Display the boutique resource.
     *
     * @param $slug
     * @return Response
     * @internal param int $id
     */
    public function preview($slug)
    {
        $shop = Shop::where('slug', $slug)->firstOrFail();
        if(is_null($shop)){
            return redirect()->back()
                ->with('message', 'La boutique n\'existe pas!')
                ->with('type', 'warning');
        }elseif(!$this->auth->user()->hasRole('member') || $shop->user_id === $this->auth->user()->id){
            $ads = Ad::where('shop_slug',$slug)->with('medias')->paginate(config('deal.back-shop-ad-preview-pagination'));
            return view('back.shops.preview',compact('shop','ads'));
        }else{
            return redirect()->back()
                ->with('message', 'Vous n\'êtes pas autorisé à prévisualiser cette boutique!')
                ->with('type', 'warning');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $categories = Category::isParent()->get();
        $cities = City::all();
        $shop = Shop::findOrFail($id);
        return view('back.shops.edit', compact('shop','categories','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateShopRequest $req
     * @param  int $id
     * @return Response
     * @internal param Request $request
     */
    public function update(CreateShopRequest $req, $id)
    {
        $data = [
            'name' => $req->input('name'),
            'description' => $req->input('description'),
            'phone' => $req->input('phone'),
            'email' => $req->input('email'),
            'address1' => $req->input('address1'),
            'category_name' => $req->input('category'),
            'city_name' => $req->input('city')
        ];

        $data['address2'] = !empty($req->input('address2')) ? $req->input('address2') : '';
        $data['address3'] = !empty($req->input('address3')) ? $req->input('address3') : '';
        $data['logo'] = !empty($req->file('logo')) ? $req->file('logo') : '';
        $data['site'] = !empty($req->input('site')) ? $req->input('site') : '';

        if ($this->shopRepository->updateShop($data, $id)) {
            return redirect()->route('shop.index')
                ->with('message', 'Votre boutique a bien été mise à jour.')
                ->with('type', 'success');
        } else {
            // Store message in log
            supervisor('Impossible de mettre à jour une boutique. (ShopsController@update)');
            return redirect()->route('shop.index')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteShopRequest $req
     * @param  int $id
     * @return Response
     */
    public function destroy(DeleteShopRequest $req, $id)
    {
        if($req->input('shop_id') == $id && !is_null(Shop::where('id','=',$id)->where('user_id','=',$this->auth->user()->id)->first())){
            if($this->shopRepository->destroyShop($id)){
                return redirect()->back()
                    ->with('message', 'Votre boutique a bien été supprimer.')
                    ->with('type', 'success');
            } else {
                return redirect()->back()
                    ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                    ->with('type', 'danger');
            }
        } else{
            // Store message in log
            supervisor('Impossible de supprimer une boutique inexistante ou non propriétaire. (ShopsController@destroy)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. La boutique que vous éssayez de supprimer n\'existe pas ou ne vous appartient pas!')
                ->with('type', 'danger');
        }
    }

    /**
     * Get Shop ads.
     *
     * @return Response
     */
    public function getAds()
    {
        $hasShops = false;
        $shops = $this->auth->user()->shops()->where('status',true)->get();

        if (sizeof($shops) > 0){
            $hasShops = true;
        }
        $ads = $this->auth->user()->ads()->whereNotNull('shop_slug')->with('medias')->orderBy('created_at', 'DESC')->paginate(config('deal.back-ads-pagination'));
        return view('back.shops.ad',compact('ads','hasShops'));
    }
}
