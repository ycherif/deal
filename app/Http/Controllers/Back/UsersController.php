<?php

namespace App\Http\Controllers\Back;

use App\Deal\Users\UserRepository;
use App\Http\Requests\UpdateUserInfoRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use App\Models\City;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Exception;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Auth instance
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Users repository instance
     *
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct( Guard $auth, UserRepository $userRepository )
    {
        $this->auth = $auth;
        $this->userRepository = $userRepository;
    }

    /**
     * Show user home page
     *
     * @return \Illuminate\View\View
     */
    public function getHome()
    {
        $city = City::find($this->auth->user()->city_id);
        $cities = City::where('id','!=','1')->get();
        dd($cities);
        return view('back.profiles.index',compact('city','cities'));
    }

    /**
     * Update user information
     *
     * @param UpdateUserInfoRequest $req
     * @return mixed
     */
    public function postInfo(UpdateUserInfoRequest $req)
    {
        $data = [
            'username'      => $req->input('username'),
            'firstname'     => $req->input('firstname'),
            'lastname'      => $req->input('lastname'),
            'city_id'       => $req->input('city_id'),
            'phone'         => $req->input('phone'),
            'description'   => $req->input('description'),
            'address'       => $req->input('address'),
            'avatar'        => $req->file('avatar')
        ];

        if($this->userRepository->updateInfo($data)){
            return redirect()->route('user.home')
                ->with('message', 'Votre profil a bien été mis à jour.')
                ->with('type', 'success');
        } else{
            // Store message in log
            supervisor('Impossible de mettre à jour un profil. (UsersController@postInfo)');
            return redirect()->route('user.home')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Update user password
     *
     * @param UpdateUserPasswordRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPassword(UpdateUserPasswordRequest $req)
    {
        $user = User::findOrFail($this->auth->user()->id);
        $user->password = Hash::make($req->input('password'));
        try{
            $user->save();
            return redirect()->route('user.home')
                ->with('message', 'Votre mot de passe a bien été mis à jour.')
                ->with('type', 'success');
        } catch(Exception $e){
            // Store message in log
            supervisor('Impossible de mettre à jour un mode passe de compte. (UsersController@postPassword)');
            return redirect()->route('user.home')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }

    /**
     * Show application delete account page.
     *
     * @return \Illuminate\View\View
     */
    public function delete()
    {
        return view('users.close');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
        if($this->userRepository->deleteAccount()){
            return redirect()->route('home')
                ->with('message', 'Votre compte a bien été supprimer.')
                ->with('type', 'success');
        } else{
            // Store message in log
            supervisor('Impossible de supprimer un utilisateur. (UsersController@destroy)');
            return redirect()->route('user.home')
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard!')
                ->with('type', 'danger');
        }
    }
}
