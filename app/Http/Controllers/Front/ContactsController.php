<?php

namespace App\Http\Controllers\Front;


use App\Deal\Mailers\ContactMailer;
use App\Http\Requests\SendToAdminRequest;
use App\Http\Requests\SendToModeratorRequest;
use App\Http\Requests\SendToUserRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{

    /**
     * Mailer service instance
     *
     * @var ContactMailer
     */
    protected $contactMailer;

    /**
     * @param ContactMailer $contactMailer
     */
    public function __construct(ContactMailer $contactMailer)
    {
        $this->contactMailer = $contactMailer;
    }

    /**
     * Send email to user.
     *
     * @param SendToUserRequest $req
     * @param $email
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendMember(SendToUserRequest $req, $email, $slug)
    {
        try {
            if($req->input('receiver') == $email && $req->input('slug') == $slug){
                $data = [
                    'receiver' => $req->input('receiver'),
                    'name' => $req->input('name'),
                    'sender' => $req->input('sender'),
                    'phone' => $req->input('phone'),
                    'body' => $req->input('message'),
                    'slug' => $req->input('slug')
                ];

                $this->contactMailer->contactMember($data);
                return redirect()->back()
                    ->with('message', 'Votre email a bien été envoyé.')
                    ->with('type', 'success');
            }
        } catch (\Exception $e) {
            return redirect()->back()
            ->with('message', 'Votre email n\' pas pu être envoyé!')
            ->with('type', 'warning');
        }
        

    }

    /**
     * Send email to moderator.
     *
     * @param SendToModeratorRequest $req
     * @param $id
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendModerator(SendToModeratorRequest $req, $id, $slug)
    {
        try {
            if($req->input('moderator') == $id && $req->input('slug') == $slug){
                $data = [
                    'reason' => $req->input('reason'),
                    'sender' => $req->input('email'),
                    'body' => $req->input('message'),
                    'id_receiver' => $req->input('moderator'),
                    'slug' => $req->input('slug')
                ];
                $this->contactMailer->contactModerator($data);
                return redirect()->back()
                    ->with('message', 'Votre email a bien été envoyé.')
                    ->with('type', 'success');
            }
        } catch (\Exception $e) {
            return redirect()->back()
            ->with('message', 'Votre email n\' pas pu être envoyé!')
            ->with('type', 'warning');
        }
        
    }

    /**
     * Send email to user.
     *
     * @param SendToUserRequest $req
     * @param $email
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendShop(SendToUserRequest $req, $email, $slug)
    {
        try {
            if($req->input('receiver') == $email && $req->input('slug') == $slug){
                $data = [
                    'receiver' => $req->input('receiver'),
                    'name' => $req->input('name'),
                    'sender' => $req->input('sender'),
                    'phone' => $req->input('phone'),
                    'body' => $req->input('message'),
                    'slug' => $req->input('slug')
                ];

                $this->contactMailer->contactShop($data);
                return redirect()->back()
                    ->with('message', 'Votre email a bien été envoyé.')
                    ->with('type', 'success');
            }
        } catch (\Exception $e) {
            return redirect()->back()
            ->with('message', 'Votre email n\' pas pu être envoyé!')
            ->with('type', 'warning');
        }
        

    }

    /**
     * Send email to admin.
     *
     * @param SendToAdminRequest $req
     */
    public function sendAdmin(SendToAdminRequest $req)
    {

    }
}
