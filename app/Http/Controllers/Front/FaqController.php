<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\CreateFaqRequest;
use App\Models\Faq;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $faqs = Faq::published()->paginate(config('deal.front-faq-pagination'));
        return view('front.pages.faq',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateFaqRequest $req
     * @return Response
     * @internal param Request $request
     */
    public function store(CreateFaqRequest $req)
    {
        $newFaq = new Faq();
        $newFaq->question = $req->input('question');
        $newFaq->email = $req->input('email');

        //Start transaction
        DB::beginTransaction();
        try {
            $newFaq->save();
            DB::commit();
            return redirect()->back()
                ->with('message', 'Votre question a bien été envoyée.')
                ->with('type', 'success');
        } catch (Exception $e) {
            DB::rollback();
            // Store message in log
            supervisor('Impossible de poster une question dans la faq. (FaqController@store)');
            return redirect()->back()
                ->with('message', 'Une érreur est survenue. Rééssayer plus tard !')
                ->with('type', 'danger');
        }
    }
}
