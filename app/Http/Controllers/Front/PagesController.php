<?php

namespace App\Http\Controllers\Front;

use App\Deal\Mailers\ContactMailer;
use App\Http\Requests\CreateContactMsgRequest;
use App\Models\Ad;
use App\Models\Category;
use App\Models\City;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Search_View;
use App\Models\Shop;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;



class PagesController extends Controller
{
    /**
     * ContactMailer instance
     *
     * @var ContactMailer
     */
    protected $contactMailer;

    /**
     * @param ContactMailer $contactMailer
     */
    public function __construct(ContactMailer $contactMailer)
    {
        $this->contactMailer = $contactMailer;
    }



    /**
     * Show front home page.
     *
     * @return View
     */
    public function getHome()
    {
        $categories = Category::isParent()->with('children')->get();
        $cities = City::all();
        $ads = Ad::published()->orderBy('visitors', 'desc')->with('medias')->limit(12)->get();
        $nbrAds = DB::select('select count(id) as number from ads where ads.status is true');
        return view('front.pages.index', compact('categories','cities','ads','nbrAds'));
    }


    /**
     * Show contact page.
     *
     * @return View
     */
    public function getContact()
    {
        return view('front.pages.contact');
    }

    /**
     * Show about page.
     *
     * @return View
     */
    public function getAbout()
    {
        return view('front.pages.about');
    }

    /**
     * Show condition page.
     *
     * @return View
     */
    public function getCondition()
    {
        return view('front.pages.term');
    }

    /**
     * Send contact message.
     *
     * @param CreateContactMsgRequest $req
     * @return View
     */
    public function sendContactMsg(CreateContactMsgRequest $req)
    {
        $data=[
            'sender' => $req->input('email'),
            'message' => $req->input('message')
        ];
        if(!empty($req->input('firstname')))
            $data['firstname'] = $req->input('firstname');
        if(!empty($req->input('lastname')))
            $data['lastname'] = $req->input('lastname');
        if(!empty($req->input('enterprise')))
            $data['enterprise'] = $req->input('enterprise');

        if($this->contactMailer->contactAdmin($data))
            return redirect()->back()
                ->with('message', 'Votre message a bien été envoyé.')
                ->with('type', 'success');
        else{
            return redirect()->back()
                ->with('message', 'Votre message n\'a pas pu être envoyer. Merci de rééssayer plus tard !')
                ->with('type', 'warning');
        }
    }




    /**
     * Display the specified ad.
     *
     * @param $slug
     * @return Response
     */
    public function adDetail($slug)
    {
        $ad = Ad::detail($slug)->with('medias','user')->firstOrFail();
        $ad->visitors += 1;
        $ad->save();
        $parentCategory = Category::findOrFail($ad->category->parent_id);
        if(isset($ad->shop_slug)){
            $shop = Shop::where('slug', $ad->shop_slug)->firstOrFail();
            return view('front.pages.detail',compact('ad','parentCategory','shop'));
        }
        return view('front.pages.detail',compact('ad','parentCategory'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function adsSearch(){

        //Create categories view if not exits
        try{
            $categories = DB::table('categories_view')->orderBy('id','asc')->get();
        }catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createCategoriesParentView();
                $categories=DB::table("categories_view")->get();
            }
        }

        //Create cities vies if not exits
        try{
            $cities = DB::table("cities_view")->get();
        }catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createCitiesView();
                $cities=DB::table("cities_view")->get();
            }
        }

        foreach($categories as $key => $cat){
            if($cat->parent_id !=0){
                foreach($categories as $pkey => $pcat){
                    if($pcat->id==$cat->parent_id){
                        $categories[$pkey]->count +=$categories[$key]->count;
                    }
                }
            }
        }

        return view('front.pages.search',compact('categories','cities'));
    }
    /**
     * @return \Illuminate\View\View
     */
    public function shopSearch(){
        $categories=Category::select('name','slug','slug_search')->where('parent_id',0)->get();
        $cities=City::select('name','slug')->get();
        return view('front.pages.shop',compact('categories','cities'));
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function shopDetailSearch($slug){

        $shop = Shop::where('slug', $slug)->Published()->first();
        if(!is_null($shop)){
            $categories=Category::select('name','slug','slug_search')->whereRaw('slug_search LIKE ? and parent_id !=?',[$shop->slug_search.'%',0])->get();
            $cities=City::select('name','slug')->get();
            return view('front.pages.detailShop',compact('shop','categories','cities'));
        }
        if(str_contains(URL::previous(),'/annonces'))
            return redirect()->back()
                ->with('message', 'Cette Boutique n\'existe plus ou n\'est plus en ligne . ')
                ->with('type', 'warning');
        return redirect()->route('home')
            ->with('message', 'Cette Boutique n\'existe plus ou n\'est plus en ligne . ')
            ->with('type', 'warning');
    }


    /**
     *
     */
    private function createAdsView()
    {
        DB::statement("CREATE VIEW ads_media AS
                         select ad_id , url, count(*) as medias_count from medias
                         group by ad_id;
                     ");

        DB::statement("CREATE VIEW ads_view AS
                            SELECT a.id,a.title,a.slug,a.description,a.price,a.type,a.user_type,
                                   a.hide_phone,a.negotiable,a.visitors,a.published_at,a.shop_slug,
                                   a.city_id,c.name as city_name,c.slug as city_slug,
                                   a.category_id,f.slug as cat_slug,f.slug_search,f.name as cat_name,
                                   f.parent_id,p.name as parent_name,p.slug as parent_slug,
                                   m.url,m.medias_count
                            FROM ads a
                                    INNER JOIN categories f ON a.category_id=f.id
                                    INNER JOIN categories p ON p.id=f.parent_id
                                    INNER JOIN cities c ON c.id=a.city_id
                                    INNER JOIN ads_media m ON m.ad_id=a.id
                            WHERE a.status=1 AND a.deleted_at IS NULL
                            ORDER BY published_at DESC
                     ");

    }

    /**
     *
     */
    private function createCategoriesParentView()
    {
        try{
            DB::statement("CREATE VIEW categories_view AS
                            (SELECT  c.id,c.name,c.slug,c.slug_search,c.parent_id,0 as count
                                    FROM categories c left JOIN ads_view a ON a.slug_search = c.slug_search
                                    where   a.slug_search is null
                                    GROUP BY c.id )
                                UNION
                            (SELECT  c.id,c.name,c.slug,c.slug_search,c.parent_id,count(*) as count
                                    FROM categories c left JOIN ads_view a ON a.slug_search = c.slug_search
                                    where   a.slug_search !=''
                                    GROUP BY c.id )
                     ");

            DB::statement("CREATE VIEW categories_parent_view AS
                            SELECT c.id,c.parent_id,c.name,c.slug,c.slug_search, SUM(cv.count) as count
                                FROM
                                categories_view cv,categories c
                                WHERE cv.parent_id=c.id
                                GROUP BY cv.parent_id
                                ORDER BY c.name ASC
                     ");
        } catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createAdsView();
                $this->createCategoriesParentView();
            }
        }

    }

    /**
     *
     */
    private function createCitiesView()
    {
        try{
            DB::statement("CREATE VIEW cities_view AS
                            (SELECT c.id,c.name,c.slug,count(*) as count
                                FROM cities c , ads_view a
                                where a.city_slug = c.slug
                                GROUP BY c.id
                               )
                                UNION
                           ( SELECT c.id,c.name,c.slug,0 as count
                                FROM cities c LEFT JOIN ads_view a ON a.city_slug=c.slug
                                where a.city_slug is null
                                GROUP BY c.id
                                )
                     ");
        }catch(Exception $e){
            if($e->getCode()=="42S02"){
                $this->createAdsView();
                $this->createCitiesView();
            }
        }
    }

    /**
     * Search ads
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param $params
     */

    public function apiSearchAds($data)
    {
        $data = json_decode($data,true);
        try{

            $term = isset($data['term']) ? $data['term'] : '';
            $city = isset($data['city']) ? $data['city'] : '';
            $category = isset($data['category']) ? $data['category'] : '';
            $order_by = isset($data['order_by']) ? $data['order_by'] : '';
            $user_type = isset($data['user_type']) ? $data['user_type'] : '';
            $type = isset($data['type']) ? $data['type'] : '';
            $price_min = isset($data['price_min']) ? $data['price_min'] : '';
            $price_max = isset($data['price_max']) ? $data['price_max'] : '';
            $shop = isset($data['shop']) ? $data['shop'] : '';

            $query = "";
            $params = [];
            $stats=[];

            if(!empty($term)){
                $wordInQ = preg_split("/[\\s\\\\*()\\[\\]{}\\|\\%]+/",$term);
                $query.=" CONCAT(title,description) regexp" ;
                foreach($wordInQ as $word){
                    $query.=" ? & " ;
                    $params[] = $word;
                }
                $query= trim($query, "& ")." And " ;
            }

            if(!empty($city)){
                if ($city != 'ivoire'){
                    $query.="city_slug = ? And " ;
                    $params[] = $city;
                }
            }


            if(!empty($category)){
                $query.="slug_search LIKE ? And " ;
                $params[] = "%$category%";
            }
            if(!empty($type)){
                $query.="type = ? And " ;
                $params[] = $type;
            }
            if(!empty($price_min)){
                $query.="price >= ? And " ;
                $params[] = $price_min;
            }
            if(!empty($price_max)){
                $query.="price <= ? And " ;
                $params[] = $price_max;
            }

            if(!empty($shop)){
                $query.="shop_slug = ? And " ;
                $params[] = $shop;
            }



            if(!empty($query)){
                $stats = DB::table("ads_view")->select(DB::raw('count(*) as count, user_type'))->whereRaw(trim($query, 'And '),$params)->groupBy('user_type')->get();
            }else{
                $stats = DB::table("ads_view")->select(DB::raw('count(*) as count, user_type'))->groupBy('user_type')->get();
            }

            if(!empty($user_type)){
                $query.="user_type = ? And " ;
                $params[] = $user_type;
            }

            $orderBy = [];
            if(!empty($order_by)){
                $orderBy=preg_split("/[,]/",$order_by);
            }

           if(empty($query)){
               if(count($orderBy) == 2){
                   $ads = DB::table("ads_view")->orderBy($orderBy[0],$orderBy[1])->paginate(config('deal.front-search-ads-pagination'));
               }else
                   $ads = DB::table("ads_view")->paginate(config('deal.front-search-ads-pagination'));
           } elseif(count($orderBy) == 2){
               $ads = DB::table("ads_view")->whereRaw(trim($query, 'And '),$params)->orderBy($orderBy[0],$orderBy[1])->paginate(config('deal.front-search-ads-pagination'));
           } else{
               $ads = DB::table("ads_view")->whereRaw(trim($query, 'And '), $params)->paginate(config('deal.front-search-ads-pagination'));
                }

        }catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createAdsView();
                $ads=DB::table("ads_view")->paginate(config('deal.front-search-ads-pagination'));
            }else{
                return $e;
            }

        }
        $tab=[];
        $tab['ads']=$ads->toArray();
        $tab['stats'] = $stats;
        return $tab;
    }


    /**
     * Search ads
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param $params
     */

    public function apiSearchShops($data)
    {
        $data = json_decode($data,true);
        try{

            $term = isset($data['term']) ? $data['term'] : '';
            $city = isset($data['city']) ? $data['city'] : '';
            $category = isset($data['category']) ? $data['category'] : '';
            $order_by = isset($data['order_by']) ? $data['order_by'] : '';


            $query = "";
            $params = [];

            if(!empty($term)){
                $wordInQ = preg_split("/[\\s\\\\*()\\[\\]{}\\|]+/",$term);
                $query.=" CONCAT(name,description) regexp" ;
                foreach($wordInQ as $word){
                    $query.=" ? & " ;
                    $params[] = $word;
                }
                $query= trim($query, "& ")." And " ;
            }

            if(!empty($city)){
                if ($city != 'ivoire')
                {
                    $query.="city_slug = ? And " ;
                    $params[] = $city;
                }
            }

            if(!empty($category)){
                $query.="slug_search LIKE ? And " ;
                $params[] = "%$category%";
            }

            $orderBy=[];

            if(!empty($order_by)){
                $orderBy=preg_split("/[,]/",$order_by);
            }

           if(empty($query)){
               $shops = DB::table("shops")->where('status', true)->paginate(config('deal.front-search-ads-pagination'));
           } elseif(count($orderBy)==2){
               $shops = DB::table("shops")->where('status', true)->whereRaw(trim($query, 'And '),$params)->orderBy($orderBy[0],$orderBy[1])->paginate(config('deal.front-search-ads-pagination'));
           } else{
               $shops = DB::table("shops")->where('status', true)->whereRaw(trim($query, 'And '), $params)->paginate(config('deal.front-search-ads-pagination'));
           }

        }catch (Exception $e){
            return $e;
            if($e->getCode()=="42S02"){

                //$this->createAdsView();
                //$ads=DB::table("ads_view")->paginate(config('deal.front-search-ads-pagination'));
            }
        }


        return $shops ;
    }

    /**
     * Get categories and cities
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiCategoriesAndCities()
    {


        //Create categories view if not exits
        try{
            $categories = DB::table('categories_view')->get();

        }catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createCategoriesParentView();
                $categories=DB::table("categories_view")->get();
            }
        }


        //Create cities vies if not exits
        try{
            $cities = DB::table("cities_view")->get();
        }catch (Exception $e){
            if($e->getCode()=="42S02"){
                $this->createCitiesView();
                $cities=DB::table("cities_view")->get();
            }
        }


        foreach($categories as $key => $cat){
            if($cat->parent_id!=0){
                foreach($categories as $pkey => $pcat){
                    if($pcat->id==$cat->parent_id){
                        $categories[$pkey]->count +=$categories[$key]->count;
                    }
                }
            }
        }

        return response()->json(compact('categories','cities'));
    }

    /**
     * Get categories and cities
     *
     * @param $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiShopDetail($slug)
    {


        //Create categories view if not exits
        try{

            $shop = Shop::where('slug', $slug)->Published()->firstOrFail();

        }catch (Exception $e){
            if($e->getCode()=="42S02"){

            }
        }

        return response()->json(compact('shop'));
    }

}