<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    /**
     * Get image
     *
     * @return mixed
     */
    public function getImage($item, $tp, $imagename)
    {
        $imagePath = "images/{$item}/{$tp}/{$imagename}";
        $image = Storage::get($imagePath);
        $imageExt = pathinfo($imagename, PATHINFO_EXTENSION);
        return response($image, 200)->header('Content-Type',"image/{$imageExt}");
    }
}
