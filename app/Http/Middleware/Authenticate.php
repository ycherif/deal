<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param $roles
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if(!$this->auth->check()) {
            return redirect()->route('auth.login')
                ->with('type', 'danger')
                ->with('message', 'Vous devriez être connecté avant d\'y accéder à cette page!');
        }


       /* if(is_array(explode('|',$role))){
            $this->access = explode('|',$role);
        } else{
            $this->access = [$role];
        }*/

        if(!$this->auth->user()->hasRole($roles)){
            abort(403);
        }

        return $next($request);
    }
}
