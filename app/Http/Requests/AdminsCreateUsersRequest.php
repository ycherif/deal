<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminsCreateUsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:3|unique:users|max:50|alpha_num',
            'firstname'=> 'required|min:3',
            'lastname'=> 'required|min:3',
            'email'=> 'required|unique:users|email',
            'city_id' => 'required|integer',
            'type' => 'required',
            'phone' => 'required|digits:8',
            'gender' => 'required',
            'role_slug' => 'required|alpha'
        ];
    }
}
