<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminsUpdateUsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::input('user_id') == Request::segment(3)){
            $user_id = Request::input('user_id');

            $rules = [
                'user_id' => 'required|integer',
                'firstname'=> 'required|min:3',
                'lastname'=> 'required|min:3',
                'city_id' => 'required|integer',
                'phone' => 'required|digits:8',
                'role_slug' => 'required||alpha'
            ];

            $rules['username'] = 'required|min:3|unique:users,email,{$user_id}|max:50||alpha_num';

            return $rules;
        }

        return [
            'username' => 'required|min:3|unique:users|max:50||alpha_num',
            'user_id' => 'required|integer',
            'firstname'=> 'required|min:3',
            'lastname'=> 'required|min:3',
            'city_id' => 'required|integer',
            'phone' => 'required|digits:8',
            'role_slug' => 'required|alpha'
        ];

    }
}
