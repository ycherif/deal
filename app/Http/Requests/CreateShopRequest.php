<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateShopRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|unique:shops,name,".Request::input('id'),
            'description' => 'required',
            'phone' => 'required|digits:'.config('deal.phone'),
            'email' => "required|email|unique:shops,email,".Request::input('id'),
            'address1' => 'required',
            'category' => 'required',
            'city' => 'required'
        ];
    }
}
