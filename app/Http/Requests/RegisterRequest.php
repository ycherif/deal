<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;


class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:3|max:50|alpha',
            'firstname'=> 'required|min:3',
            'lastname'=> 'required|min:3',
            'email'=> 'required|email',
            'city_id' => 'required|integer',
            'password'=>'required|min:6',
            'confirmation_password'=>'same:password',
            'condition' => 'required',
            'type' => 'required',
            'phone' => 'required|digits:8',
            'gender' => 'required'
        ];
    }
}
