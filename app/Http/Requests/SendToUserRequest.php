<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SendToUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3email|',
            'sender' => 'required|email',
            'phone' => 'digits:8',
            'message' => 'required|min:5',
            'receiver' => 'required|email',
            'slug' => 'required'
        ];
    }
}
