<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateAdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|integer',
            'type' => 'required',
            'title' => "required|min:3|max:60",
            'description' => 'required|max:2000',
            'price' => 'required|integer',
            'city' => 'required|integer'
        ];
    }
}
