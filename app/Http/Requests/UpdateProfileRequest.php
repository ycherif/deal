<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'min:3|unique:users,email,{Auth::user()->id}|max:50|alpha_num',
            'firstname' => 'min:3|max:50',
            'lastname'  => 'min:3|max:50',
            'phone'    => 'digits:8',
            'description'=>'min:5|max:1000',
            'avatar' => 'mimes:jpeg,png,jpg|max:1000'
        ];
    }
}
