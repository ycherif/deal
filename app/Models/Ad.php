<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "ads";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = ['category_id','type','title','description','price','phone','city_id'];

    /**
     * Detail data scope
     *
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopeDetail($query, $slug)
    {
        return $query->where('slug','=',$slug)->where('status','=',true);
    }

    /**
     * preview data scope
     *
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopePreview($query, $slug)
    {
        return $query->where('slug','=',$slug);
    }

    /**
     * Scope for published ads
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->where('status', true);
    }

    /**
     * Scope for pending ads
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where('status', false);
    }

    /**
     * OneToOne relationship (Ad and Category)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){

        return $this->belongsTo('App\Models\Category');
    }

    /**
     * HasMany relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany (Ad and Media)
     */
    public function medias(){

        return $this->hasMany('App\Models\Media');
    }

    /**
     * BelongsTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo (Ad and Users)
     */
    public function user(){

        return $this->belongsTo('App\Models\User');
    }

    /**
     * BelongsTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo (Ad and City)
     */
    public function city(){

        return $this->belongsTo('App\Models\City');
    }



}
