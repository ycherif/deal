<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','parent_id'];

    /**
     *
     *
     * @param $query
     * @return mixed
     */
    public function scopeisParent($query)
    {
        return $query->whereParentId(0);
    }

    /**
     *
     *
     * @param $query
     * @param $parentId
     * @return mixed
     */
    public function scopeChildren($query, $parentId)
    {
        return $query->whereParentId($parentId);
    }

    public function checkParent($slug)
    {
        return (Category::where('slug',$slug)->first()->parent_id == 0) ? true  : false;
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function children_ads()
    {
        return $this->hasManyThrough('App\Models\Ad', 'App\Models\Category', 'parent_id','category_id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    /**
     * HasMany relationship (Category and Ad)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads(){

        return $this->hasMany('App\Models\Ad');
    }

}
