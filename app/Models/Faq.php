<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faqs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question','response','published','email'];

    /**
     *
     * Scope for published ads
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query){
        return $query->where('status', true);
    }
}
