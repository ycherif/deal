<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "favorites";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = ['user_id','ad_id'];

    /**
     * Detail data scope
     *
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopeDetail($query, $slug)
    {
        return $query->where('slug','=',$slug)->where('status','=',true);
    }

    /**
     *
     * BelongsTo relationship (Favorite and Users)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo('App\Models\User');
    }
}
