<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "logs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = ['message','type','created_at'];

    /**
     * Disable laravel auto generate timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * BelongsTo relationship (Log and User)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
