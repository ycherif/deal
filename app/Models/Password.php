<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'password_resets';

    /**
     * Disable timestamps methods
     *
     * @var bool
     */
    public $timestamps = false;
}