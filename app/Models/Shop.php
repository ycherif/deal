<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shops';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','phone','email','description','address1','address2','address3','category_name','city_name','logo','site'];

    /**
     * BelongsTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo (Shop and User)
     */
    public function user(){

        return $this->belongsTo('App\Models\User');
    }
    /**
     *
     * Scope for published ads
     *
     * @param $query
     * @return mixed
     */
    public function scopePublished($query){
        return $query->where('status', true);
    }
}
