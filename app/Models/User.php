<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes, Notifiable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $access;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname','lastname','username','email','password','type','active','active_token','avatar','phone','gender','address','description','city_id','last_login'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * BelongsToMany relationship (Users and Role)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role')->withTimestamps();
    }

    /**
     *
     * BelongsTo RelationShip (Users and City)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city(){

        return $this->belongsTo('App\Models\City');

    }

    /**
     * HasMany relationship (Users and Social)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    /**
     * HasMany relationship (Users and Ad)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany('App\Models\Ad');
    }

    /**
     *
     * HasMany RelationShip (User and Shop)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shops()
    {
        return $this->hasMany('App\Models\Shop');
    }

    /**
     * HasMany relationship (Users and Favorite)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany (Users and Favorite)
     */
    public function favorites(){

        return $this->hasMany('App\Models\Favorite');
    }

    /**
     * HasMany relationship (User and Shop)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * HasMany relationship (User and Log)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany('App\Models\Log');
    }


    /**
     * Check if has specified role
     *
     * @param $slugs
     * @return bool
     * @internal param $name
     */
    public function hasRole($slugs)
    {
        if(is_array(explode('|', $slugs))){
            $this->access = explode('|',$slugs);
        } else {
            $this->access = [$slugs];
        }

        foreach($this->roles as $role)
        {
            if(in_array($role->slug,$this->access)) return true;
        }

        return false;
    }

    /**
     * Assign specified role
     *
     * @param $role
     */
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    /**
     * Remove specified role
     *
     * @param $role
     * @return int
     */
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }
}
