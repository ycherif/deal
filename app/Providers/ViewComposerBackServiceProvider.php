<?php

namespace App\Providers;

use App\Models\City;
use App\Models\Role;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerBackServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * _form view composer
         */
        View::composer(['back.admins.users._form'], function($view){
            $roles = Role::where('slug','!=','admin')->get();
            $cities = City::all();
            $view->with(['roles' => $roles, 'cities' => $cities]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
