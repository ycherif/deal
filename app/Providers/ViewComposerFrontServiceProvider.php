<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerFrontServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * _searchBar, _leftSidebar and index view composer
         */
//        View::composer(['front.pages.search'], function($view){
//            $categories = Category::isParent()->with('children')->with(['children_ads'=> function($query){
//                $query->published();
//            }])->get();
//            $cities = City::all();
//            $view->with(['categories' => $categories, 'cities' => $cities]);
//        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
