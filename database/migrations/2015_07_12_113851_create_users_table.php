<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',50)->nullable();
            $table->string('lastname',50)->nullable();
            $table->string('username',50)->unique()->nullable()->index();
            $table->string('email',50)->nullable()->unique()->index();
            $table->string('password',70)->nullable();
            $table->enum('type',['P','B'])->default('P');
            $table->boolean('active')->default(false);
            $table->string('active_token', 120)->nullable();
            $table->string('avatar')->nullable();
            $table->string('phone',8)->nullable();
            $table->enum('gender',['M','F'])->nullable();
            $table->string('address',200)->nullable();
            $table->text('description',1000)->nullable();
            $table->boolean('banned')->default(false);
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamp('last_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
