<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',70)->index();
            $table->string('slug')->unique()->index();
            $table->string('description',2000);
            $table->integer('price');
            $table->enum('type',['D','O']);
            $table->enum('user_type',['B','P']);
            $table->boolean('status')->default(false);
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('moderated_by')->unsigned();
            $table->foreign('moderated_by')->references('id')->on('users');
            $table->boolean('hide_phone')->default(false);
            $table->boolean('negotiable')->default(false);
            $table->integer('visitors')->nullable();
            $table->string('shop_slug')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
   {
//        Schema::table('ads', function($table) {
//            $table->dropIndex('search');
//        });
        Schema::drop('ads');
    }
}
