<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('phone',8);
            $table->string('email',60)->unique();
            $table->text('address1',200);
            $table->text('address2',200)->nullable();
            $table->text('address3',200)->nullable();
            $table->string('site',70)->nullable();
            $table->string('logo',250)->nullable();
            $table->text('description',1000);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('category_name',200);
            $table->string('slug_search');
            $table->string('city_name',200);
            $table->string('city_slug');
            $table->timestamp('published_at')->nullable();
            $table->boolean('status')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}
