<?php

use App\Models\Ad;
use App\Models\City;
use Illuminate\Database\Seeder;

class AdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userCity = City::find(1);

        $faker = Faker\Factory::create();

        //Ad::truncate();


        foreach(range(1,50) as $index)
        {
            Ad::create([
                'title'=> $faker->unique()->title,
                'slug'=> $faker->unique()->slug,
                'description'=>$faker->text($maxNbChars = 200),
                'price'=>$faker->buildingNumber,
                'type'=>'D',
                'user_type'=>'D',
                'category_id'=>1,
                'moderated_by'=>2,
                'user_id'=>1,
                'city_id'=>$userCity->id,
            ]);


        }
    }
}
