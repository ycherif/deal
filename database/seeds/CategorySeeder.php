<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        Category::create([
            'name'=>'INFORMATIQUE ET MULTIMEDIA',
            'icon'=> Str::slug('INFORMATIQUE ET MULTIMEDIA').'.png',
            'slug'=> Str::slug('INFORMATIQUE ET MULTIMEDIA'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA'),
            'parent_id'=>0
        ]);

        Category::create([
            'name'=>'Téléphones',
            'slug'=> Str::slug('Téléphones'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Téléphones'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Tablettes',
            'slug'=> Str::slug('Tablettes'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Tablettes'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Ordinateurs portables',
            'slug'=> Str::slug('Ordinateurs portables'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Ordinateurs portables'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Ordinateurs de bureau',
            'slug'=> Str::slug('Ordinateurs de bureau'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Ordinateurs de bureau'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Accessoires informatique et Gadgets',
            'slug'=> Str::slug('Accessoires informatique et Gadgets'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Accessoires informatique et Gadgets'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Jeux vidéos et Consoles',
            'slug'=> Str::slug('Jeux vidéos et Consoles'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Jeux vidéos et Consoles'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Appareils photos et Caméras',
            'slug'=> Str::slug('Appareils photos et Caméras'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Appareils photos et Caméras'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Télévisions',
            'slug'=> Str::slug('Télévisions'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Télévisions'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'Images et Son',
            'slug'=> Str::slug('Images et Son'),
            'slug_search'=> Str::slug('INFORMATIQUE ET MULTIMEDIA Images et Son'),
            'parent_id'=>1
        ]);
        Category::create([
            'name'=>'VEHICULE',
            'icon'=> Str::slug('VEHICULE').'.png',
            'slug'=> Str::slug('VEHICULE'),
            'slug_search'=> Str::slug('VEHICULE'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Voitures',
            'slug'=> Str::slug('Voitures'),
            'slug_search'=> Str::slug('VEHICULE Voitures'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'Motos',
            'slug'=> Str::slug('Motos'),
            'slug_search'=> Str::slug('VEHICULE Motos'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'Vélos',
            'slug'=> Str::slug('Vélos'),
            'slug_search'=> Str::slug('VEHICULE Vélos'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'Véhicules professionels',
            'slug'=> Str::slug('Véhicules professionels'),
            'slug_search'=> Str::slug('VEHICULE Véhicules professionels'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'Bateaux',
            'slug'=> Str::slug('Bateaux'),
            'slug_search'=> Str::slug('VEHICULE Bateaux'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'Pièces et Accessoires pour véhicule',
            'slug'=> Str::slug('Pièces et Accessoires pour véhicule'),
            'slug_search'=> Str::slug('VEHICULE Pièces et Accessoires pour véhicule'),
            'parent_id'=>11
        ]);
        Category::create([
            'name'=>'IMMOBILIER',
            'icon'=> Str::slug('IMMOBILIER').'.png',
            'slug'=> Str::slug('IMMOBILIER'),
            'slug_search'=> Str::slug('IMMOBILIER'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Appartements',
            'slug'=> Str::slug('Appartements'),
            'slug_search'=> Str::slug('IMMOBILIER Appartements'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Maisons et Villas',
            'slug'=> Str::slug('Maisons et Villas'),
            'slug_search'=> Str::slug('IMMOBILIER Maisons et Villas'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Bureaux et Plateaux',
            'slug'=> Str::slug('Bureaux et Plateaux'),
            'slug_search'=> Str::slug('IMMOBILIER Bureaux et Plateaux'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Magasins et Commerces',
            'slug'=> Str::slug('Magasins et Commerces'),
            'slug_search'=> Str::slug('IMMOBILIER Magasins et Commerces'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Terrains et Fermes',
            'slug'=> Str::slug('Terrains et Fermes'),
            'slug_search'=> Str::slug('IMMOBILIER Terrains et Fermes'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Locations de vacances',
            'slug'=> Str::slug('Locations de vacances'),
            'slug_search'=> Str::slug('IMMOBILIER Locations de vacances'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'Colocations',
            'slug'=> Str::slug('Colocations'),
            'slug_search'=> Str::slug('IMMOBILIER Colocations'),
            'parent_id'=>18
        ]);
        Category::create([
            'name'=>'POUR LA MAISON ET LE JARDIN',
            'icon'=> Str::slug('POUR LA MAISON ET LE JARDIN').'.png',
            'slug'=> Str::slug('POUR LA MAISON ET LE JARDIN'),
            'slug_search'=> Str::slug('POUR LA MAISON ET LE JARDIN'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Électroménagers et Vaisselles',
            'slug'=> Str::slug('Électroménagers et Vaisselles'),
            'slug_search'=> Str::slug('POUR LA MAISON ET LE JARDIN Électroménagers et Vaisselles'),
            'parent_id'=>26
        ]);
        Category::create([
            'name'=>'Meubles et Décorations',
            'slug'=> Str::slug('Meubles et Décorations'),
            'slug_search'=> Str::slug('POUR LA MAISON ET LE JARDIN Meubles et Décorations'),
            'parent_id'=>26
        ]);
        Category::create([
            'name'=>'Jardins et Outils de bricolage',
            'slug'=> Str::slug('Jardins et Outils de bricolage'),
            'slug_search'=> Str::slug('POUR LA MAISON ET LE JARDIN Jardins et Outils de bricolage'),
            'parent_id'=>26
        ]);
        Category::create([
            'name'=>'VÊTEMENTS ET BIEN ETRE',
            'icon'=> Str::slug('HABILLEMENT ET BIEN ETRE').'.png',
            'slug'=> Str::slug('VÊTEMENTS ET BIEN ETRE'),
            'slug_search'=> Str::slug('VÊTEMENTS ET BIEN ETRE'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Vêtements',
            'slug'=> Str::slug('Vêtements'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Vêtements'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Chaussures',
            'slug'=> Str::slug('Chaussures'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Chaussures'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Montres et Bijoux',
            'slug'=> Str::slug('Montres et Bijoux'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Montres et Bijoux'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Sacs et Accessoires',
            'slug'=> Str::slug('Sacs et Accessoires'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Sacs et Accessoires'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Vêtements pour enfants et bébés',
            'slug'=> Str::slug('Vêtements pour enfants et bébés'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Vêtements pour enfants et bébés'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Equipements pour enfants et bébés',
            'slug'=> Str::slug('Equipements pour enfants et bébés'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Equipements pour enfants et bébés'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'Produits de beauté',
            'slug'=> Str::slug('Produits de beauté'),
            'slug_search'=> Str::slug('HABILLEMENT ET BIEN ETRE Produits de beauté'),
            'parent_id'=>30
        ]);
        Category::create([
            'name'=>'LOISIRS ET DIVERTISSEMENT',
            'icon'=> Str::slug('LOISIRS ET DIVERTISSEMENT').'.png',
            'slug'=> Str::slug('LOISIRS ET DIVERTISSEMENT'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Sports et Loisirs',
            'slug'=> Str::slug('Sports et Loisirs'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Sports et Loisirs'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'Animaux',
            'slug'=> Str::slug('Animaux'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Animaux'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'Instruments de musique',
            'slug'=> Str::slug('Instruments de musique'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Instruments de musique'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'Art et Collections',
            'slug'=> Str::slug('Art et Collections'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Art et Collections'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'Voyages et Billetterie',
            'slug'=> Str::slug('Voyages et Billetterie'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Voyages et Billetterie'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'Films, Livres, Magazines',
            'slug'=> Str::slug('Films, Livres, Magazines'),
            'slug_search'=> Str::slug('LOISIRS ET DIVERTISSEMENT Films, Livres, Magazines'),
            'parent_id'=>38
        ]);
        Category::create([
            'name'=>'EMPLOI ET SERVICES',
            'slug'=> Str::slug('EMPLOI ET SERVICES'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES'),
            'icon'=> Str::slug('EMPLOI ET SERVICES').'.png',
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Offres d\'emploi',
            'slug'=> Str::slug('Offres d\'emploi'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES Offres d\'emploi'),
            'parent_id'=>45
        ]);
        Category::create([
            'name'=>'Demandes d\'emploi',
            'slug'=> Str::slug('Demandes d\'emploi'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES Demandes d\'emploi'),
            'parent_id'=>45
        ]);
        Category::create([
            'name'=>'Stages',
            'slug'=> Str::slug('Stages'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES Stages'),
            'parent_id'=>45
        ]);
        Category::create([
            'name'=>'Services',
            'slug'=> Str::slug('Services'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES Services'),
            'parent_id'=>45
        ]);
        Category::create([
            'name'=>'Cours et Formations',
            'slug'=> Str::slug('Cours et Formations'),
            'slug_search'=> Str::slug('EMPLOI ET SERVICES Cours et Formations'),
            'parent_id'=>45
        ]);
        Category::create([
            'name'=>'ENTREPRISES',
            'icon'=> Str::slug('ENTREPRISES').'.png',
            'slug'=> Str::slug('ENTREPRISES'),
            'slug_search'=> Str::slug('ENTREPRISES'),
            'parent_id'=>0
        ]);
        Category::create([
            'name'=>'Business et Affaires commerciales',
            'slug'=> Str::slug('Business et Affaires commerciales'),
            'slug_search'=> Str::slug('ENTREPRISES Business et Affaires commerciales'),
            'parent_id'=>51
        ]);
        Category::create([
            'name'=>'Matériels professionnels',
            'slug'=> Str::slug('Matériels professionnels'),
            'slug_search'=> Str::slug('ENTREPRISES Matériels professionnels'),
            'parent_id'=>51
        ]);
        Category::create([
            'name'=>'Stocks et vente en gros',
            'slug'=> Str::slug('Stocks et vente en gros'),
            'slug_search'=> Str::slug('ENTREPRISES Stocks et vente en gros'),
            'parent_id'=>51
        ]);

    }
}
