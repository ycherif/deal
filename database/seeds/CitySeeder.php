<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use Illuminate\Support\Str;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();
        foreach ($this->cities() as $city) {
            City::create([
                'name'   => $city,
                'slug'   => Str::slug($city)
            ]);
        };
    }

    public function cities()
    {
        return [
            'Abengourou',
            'Abidjan',
            'Aboisso',
            'Adzopé',
            'Agboville',
            'Ányama',
            'Biankouma',
            'Bingerville',
            'Bondoukou',
            'Bouaflé',
            'Bouaké',
            'Bouna',
            'Boundiali',
            'Bugu',
            'Dabakala',
            'Dabou',
            'Daloa',
            'Danané',
            'Dimbokro',
            'Divo',
            'Ferkessédougou',
            'Gagnoa',
            'Grand Bassam',
            'Grand Lahou',
            'Jacqueville',
            'Katiola',
            'Kong',
            'Korhogo',
            'Kouto',
            'Man',
            'Marahoué',
            'Odienné',
            'Oumé',
            'San Pédro',
            'Sassandra',
            'Séguéla',
            'Sinfra',
            'Soubré',
            'Tiagba',
            'Touba',
            'Yamoussoukro',
        ];
    }
}
