<?php

use App\Models\Faq;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faqs')->delete();
        Faq::create([
            'question'   => 'Comment puis-je poster une annonce ?',
            'response'   => 'il suffit d\'être connecté et d\'aller sur le lien suivant : <a href="'.route('ads.create').'">Poster une annonce</a> .'
        ]);

        Faq::create([
            'question'   => 'Combien de temps mon annonce restera sur le site ?',
            'response'   => 'L\'annonce sera disponible 3 mois renouvelable sous demande de l\'annonceur.'
        ]);

        Faq::create([
            'question'   => 'J\'ai vendu mon article. Comment puis-je supprimer mon annonce ?',
            'response'   => 'Acceder à vos annonces dans votre profil en suivant ce lien : <a href="'.route('ads.index').'">Mes Annonces</a> puis cliquer sur le bouton representant une "Corbeille" pour supprimer l\'annonce liée. '
        ]);
    }
}
