<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        Role::create([
            'name'   => 'Member',
            'slug'   => 'member'
        ]);
        Role::create([
            'name'   => 'Moderator',
            'slug'   => 'moderator'
        ]);
        Role::create([
            'name'   => 'Administrator',
            'slug'   => 'admin'
        ]);
    }
}
