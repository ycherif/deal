<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\City;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $memberRole = Role::whereSlug('member')->first();
        $moderatorRole = Role::whereSlug('moderator')->first();
        $adminRole = Role::whereSlug('admin')->first();
        $userCity = City::find(1);

        $userAdmin = User::create(array(
            'firstname'    => 'Pinsdeal',
            'lastname'     => 'Orus',
            'username'     => 'pinsdeal',
            'email'        => 'support@pinsdeal.ci',
            'password'     => Hash::make('pinsdeal'),
            'type'         => 'P',
            'active'       => true,
            'phone'        => '89827796',
            'gender'       => 'M',
            'city_id'      => $userCity->id
        ));

        $userAdmin->assignRole($adminRole);
    }
}
