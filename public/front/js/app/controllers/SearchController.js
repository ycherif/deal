angular.module('dealApp')
    .controller('SearchCtrl',['$scope','$http','$filter','$window',function($scope, $http, $filter,$window){
        $('button').css('outline', 'none');
        // Start loading
        $scope.loading = true;

        // Store ads
        $scope.ads = [];
        // Store total pages  of result
        $scope.totalPages = 0;
        // Store current Page
        $scope.currentPage = 1;
        // Store range
        $scope.range = [];

        // Store ads count by user_type
        $scope.stats = [];

        // Store categories
        $scope.categories = [];

        // Store parent categories
        $scope.parents = [];

        // Store windows local url
        $scope.localUrl = [];

        // Store search filter param
        $scope.search = {
            'term' : '',
            'city' : '',
            'category' : '',
            'order_by' : '',
            'user_type' : '',
            'type' : '',
            'price_min' : '',
            'price_max' : '',
            'shop' : ''
        };

        // Store cities
        $scope.cities = [];

        // The api url for ads
        var apiAdsPath = '/api/v1/products/';
        // var apiAdsPath = window.location.origin + '/api/v1/ads/';

        /**
         * Get all ads
         */
        $scope.searchAds = function(pageNumber){
            if(pageNumber === undefined){
                pageNumber = '1';
            }
            // console.log($scope.search);
            //call api and init all json list
            $http.get(apiAdsPath + JSON.stringify($scope.search) + '?page=' + pageNumber).success(function(data){

                // console.log(data);
                if(data){
                  if(data.stats){
                      $scope.stats['B']=0;
                      $scope.stats['P']=0;
                      var total=0;
                      angular.forEach(data.stats,function(val,key){
                          $scope.stats[val['user_type']]=val['count'];
                          total+=parseInt(val['count']);
                      });
                      $scope.stats['T']=total;
                  }
                   if(data.ads){
                      // $scope.stats['T'] = data.ads.total;
                       $scope.ads = data.ads.data;
                       $scope.totalPages = data.ads.last_page;
                       $scope.currentPage = data.ads.current_page;
                       var pages = [];
                       if(data.ads.total>0){
                           // Pagination Range

                           var current=data.ads.current_page;
                           var last = data.ads.current_page+10  ;
                           if(last > data.ads.last_page){
                               last=data.ads.last_page;
                               current=last-10 < 1 ? 1 : last-10;
                           }
                           for(var i=current; i <= last; i++) {
                               pages.push(i);
                           }

                       }
                       $scope.range = pages;
                   }

               }
                $scope.loading = false;
            }).error(function(error){
                console.log("Sorry ", error);
                $("#ad-block-prevent").fadeIn();
            });


        };


        /**
         * Get all parameters in url to make search
         */
        $scope.getDealUrlPath = function(){
            //get urlpath
            $scope.localUrl=window.location.pathname.replace('/annonces/',"");
            $scope.localUrl=$scope.localUrl.split('/');
            var nbrParm=$scope.localUrl.length;
            if(nbrParm>0) {
                $scope.search.city = $('.deal-country').attr('id')==  $scope.localUrl[0] ? '': $scope.localUrl[0];
            }
            if(nbrParm>1) {
                $scope.search.category=$scope.localUrl[1]=='tous'?'':$filter('filter')($scope.categories,{slug:$scope.localUrl[1]},true)[0].slug_search;
            }
            if(nbrParm>2) {
                $scope.search.term = decodeURI($scope.localUrl[2]).split('-').join(' ');
            }

        };

        /**
         * Set all parameters in url with models values
         */
        $scope.setDealUrlPath=function(){
            //set url
            var parmCity=jQuery.isEmptyObject($scope.search.city) ? '/' + $('.deal-country').attr('id') : '/' +$scope.search.city;
            //get category
            var parmCategory= jQuery.isEmptyObject($scope.search.category)? "/tous" : '/'+$filter('filter')($scope.categories,{slug_search:$scope.search.category})[0].slug;
            //get query
            var parmQuery= jQuery.isEmptyObject($scope.search.term)? '' : $scope.search.term ;
            parmQuery='/'+encodeURIComponent(parmQuery.split(" ").join("-"));
            var stateObj = { annonces: "/annonces" };
            history.pushState(stateObj, "PinsDeal Ivoire : Recherche", '/annonces'+parmCity+parmCategory+parmQuery);

        };

        /**
         * Set the category with leftSidbar parameters
         * @param cat
         */
        $scope.setCategory = function(cat_slug_search){
            $scope.search.category=cat_slug_search;
            $scope.setDealUrlPath();
            $scope.searchAds(1);
            $scope.gototop();
        }

        /**
         * Set the city with leftSidbar parameters
         * @param ville
         */
        $scope.setCity = function(ville_slug){
            $scope.search.city=ville_slug;
            $scope.setDealUrlPath();
            $scope.searchAds(1)
            $scope.gototop();
        }

        $scope.gototop=function(){

            //$window.scrollTo(0, 0);
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        }


        /**
         * Format  category name  in select box
         * @param category
         * @returns {*}
         */
        $scope.formatCategoryName = function(category){
            if(category['parent_id']==0){
                //I am parent
                return '-- '+category.name+' --';
            }
            return category.name;
        };


        $scope.searchPage = function(numPage){
            $scope.searchAds(numPage);
        };


        angular.element(document).ready(function () {
            $scope.getDealUrlPath();
            $scope.searchAds(1);

        });



    }]);