angular.module('dealApp')
    .controller('ShopCtrl',['$scope','$http','$filter',function($scope,$http,$filter){
        $('button').css('outline', 'none');
        // Start loading
        $scope.loading = true;

        // Store ads
        $scope.shops = [];
        // Store total pages  of result
        $scope.totalPages = 0;
        // Store current Page
        $scope.currentPage = 1;
        // Store range
        $scope.range = [];

        // Store ads count by user_type
        $scope.stats = [];

        // Store categories
        $scope.categories = [];

        // Store parent categories
        $scope.parents = [];

        // Store windows local url
        $scope.localUrl = [];

        // Store search filter param
        $scope.search = {
            'term' : '',
            'city' : '',
            'category' : '',
            'order_by' : '',
        };

        // Store cities
        $scope.cities = [];

        // The api url for ads
        var apiAdsPath = window.location.origin + '/api/v1/shops/';


        /**
         * Get all ads
         */
        $scope.searchShop = function(pageNumber){

            if(pageNumber === undefined){
                pageNumber = '1';
            }
            //console.log($scope.search);
            //call api and init all json list
            $http.get(apiAdsPath + JSON.stringify($scope.search) + '?page=' + pageNumber).success(function(data){
                 //console.log(data);
                if(data){
                    if(data.data){
                        $scope.shops=data.data;
                        $scope.totalPages = data.last_page;
                        $scope.currentPage = data.current_page;
                        var pages = [];
                        if(data.total>0){
                            // Pagination Range
                            var current=data.current_page;
                            var last = data.current_page+10  ;
                            if(last > data.last_page){
                                last=data.last_page;
                                current=last-10 < 1 ? 1 : last-10;
                            }
                            for(var i=current; i <= last; i++) {
                                pages.push(i);
                            }

                        }
                        $scope.range = pages;
                    }

                }
                $scope.loading = false;
            }).error(function(error){
                console.log("Sorry ", error);
            });


        };



        /**
         * Get all parameters in url to make search
         */
        $scope.getDealUrlPath = function(){
            //get urlpath
            $scope.localUrl=window.location.pathname.replace('/boutiques/',"");
            $scope.localUrl=$scope.localUrl.split('/');
            var nbrParm=$scope.localUrl.length;
            if(nbrParm>0) {
                $scope.search.city = $('.deal-country').attr('id')==  $scope.localUrl[0] ? '': $scope.localUrl[0];
            }
            if(nbrParm>1) {
                $scope.search.category=$scope.localUrl[1]=='tous'?'':$filter('filter')($scope.categories,{slug:$scope.localUrl[1]},true)[0].slug_search;
            }
            if(nbrParm>2) {
                $scope.search.term = decodeURI($scope.localUrl[2]).split('-').join(' ');
            }

        };

        /**
         * Set all parameters in url with models values
         */
        $scope.setDealUrlPath=function(){
            //set url
            var parmCity=jQuery.isEmptyObject($scope.search.city) ? '/' + $('.deal-country').attr('id') : '/' +$scope.search.city;
            //get category
            var parmCategory= jQuery.isEmptyObject($scope.search.category)? "/tous" : '/'+$filter('filter')($scope.categories,{slug_search:$scope.search.category})[0].slug;
            //get query
            var parmQuery= jQuery.isEmptyObject($scope.search.term)? '' : $scope.search.term ;
            parmQuery='/'+encodeURIComponent(parmQuery.split(" ").join("-"));
            var stateObj = { annonces: "/boutiques" };
            history.pushState(stateObj, "PinsDeal Ivoire: Recherche", '/boutiques'+parmCity+parmCategory+parmQuery);

        };

        /**
         * Format  category name  in select box
         * @param category
         * @returns {*}
         */
        $scope.formatCategoryName = function(category){
            return category.name;
        };


        $scope.searchPage = function(numPage){
           $scope.searchShop(numPage);
        }

        angular.element(document).ready(function(){
            $scope.getDealUrlPath();
            $scope.searchPage(1);
        });


    }]);




