angular.module('dealApp')
    .controller('ShopDeailCtrl',['$scope','$http','$filter','$locale',function($scope, $http, $filter){
        $('button').css('outline', 'none');

        // Start loading
        $scope.loading = true;

        // Store ads
        $scope.ads = [];

        // Store ads
        $scope.shop = [];

        // Store total pages  of result
        $scope.totalPages = 0;
        // Store current Page
        $scope.currentPage = 1;
        // Store range
        $scope.range = [];

        // Store ads count by user_type
        $scope.stats = [];

        // Store categories
        $scope.categories = [];

        // Store parent categories
        $scope.parents = [];

        // Store windows local url
        $scope.localUrl = [];


        // Store search filter param
        $scope.search = {
            'term' : '',
            'city' : '',
            'category' : '',
            'order_by' : '',
            'user_type' : '',
            'type' : '',
            'price_min' : '',
            'price_max' : '',
            'shop' : ''
        };

        // Store cities
        $scope.cities = [];

        // The api url for ads
        var apiAdsPath = window.location.origin + '/api/v1/products/';
        // var apiAdsPath = window.location.origin + '/api/v1/ads/';




        /**
         * Get all ads
         */
        $scope.searchAds = function(pageNumber){

            if(pageNumber === undefined){
                pageNumber = '1';
            }
            console.log($scope.search);
            //call api and init all json list
            $http.get(apiAdsPath + JSON.stringify($scope.search) + '?page=' + pageNumber).success(function(data){
                if(data){
                    console.log('ads');
                    console.log(data);


                    if(data.stats){
                        $scope.stats['P'] = jQuery.isEmptyObject(data.stats[0])? 0 : data.stats[0].count;
                        $scope.stats['B'] = jQuery.isEmptyObject(data.stats[1])? 0 : data.stats[1].count;;

                    }
                    if(data.ads){
                        $scope.stats['T'] = data.ads.total;
                        $scope.ads = data.ads.data;
                        console.log(data);
                        $scope.totalPages = data.ads.last_page;
                        $scope.currentPage = data.ads.current_page;
                        var pages = [];
                        if(data.ads.total>0){
                            // Pagination Range

                            var current=data.ads.current_page;
                            var last = data.ads.current_page+10  ;
                            if(last > data.ads.last_page){
                                last=data.ads.last_page;
                                current=last-10 < 1 ? 1 : last-10;
                            }

                            for(var i=current; i <= last; i++) {
                                pages.push(i);
                            }

                        }
                        $scope.range = pages;
                    }

                }
                $scope.loading = false;
            }).error(function(error){
                console.log("Sorry ", error);
            });


        };



        /**
         * Set all parameters in url with models values
         */
        $scope.setDealUrlPath=function(){
            //set url
            var parmShop=jQuery.isEmptyObject($scope.search.shop) ? '' : '/' +$scope.search.shop;

            var parmCity=jQuery.isEmptyObject($scope.search.city) ? '/' + $('.deal-country').attr('id') : '/' +$scope.search.city;
            //get category
            var parmCategory= jQuery.isEmptyObject($scope.search.category)? "/tous" : '/'+$filter('filter')($scope.categories,{slug_search:$scope.search.category})[0].slug;
            //get query
            var parmQuery= jQuery.isEmptyObject($scope.search.term)? '' : $scope.search.term ;
            parmQuery='/'+encodeURIComponent(parmQuery.split(" ").join("-"));
            var stateObj = { annonces: "/boutique-detail" };

            history.pushState(stateObj, "PinsDeal : Recherche", '/boutique-detail'+parmShop+parmCity+parmCategory+parmQuery);

        };

        /**
         * Get all parameters in url to make search
         */
        $scope.getDealUrlPath = function(){
            //get urlpath
            $scope.localUrl=window.location.pathname.replace('/boutique-detail/',"");
            $scope.localUrl=$scope.localUrl.split('/');
            var nbrParm=$scope.localUrl.length;
            if(nbrParm>0) {
                $scope.search.shop = $('.deal-country').attr('id')==  $scope.localUrl[0] ? '': $scope.localUrl[0];
            }

            if(nbrParm>1) {
                $scope.search.city = $('.deal-country').attr('id')==  $scope.localUrl[1] ? '': $scope.localUrl[1];
            }
            if(nbrParm>2) {
                $scope.search.category=$scope.localUrl[2]=='tous'?'':$filter('filter')($scope.categories,{slug:$scope.localUrl[2]},true)[0].slug_search;
            }
            if(nbrParm>3) {
                $scope.search.term = decodeURI($scope.localUrl[3]).split('-').join(' ');
            }
        };



        $scope.searchPage = function(numPage){
            $scope.searchAds(numPage);
        };

        angular.element(document).ready(function(){
            $scope.getDealUrlPath();
            $scope.searchPage(1);
        });




    }]);