@extends('front.master')



@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="PINSSERVICE AFRICA">
    <meta name="description" lang="fr" content="{{ str_limit(config('deal.conn_desc'), 250, '...') }}">
    <meta property="og:description" content="{{ str_limit(config('deal.conn_desc'), 250, '...') }}"/>
    <meta name="category" content="{{config('deal.category')}}">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="{{ str_limit(config('deal.home_title'), 50, '...') }}" />
    <meta property="og:site_name" content="{{ str_slug(config('deal.country')).'.pinsdeal.com' }}"/>
    @endsection

    @section('content')

    <!-- Start header -->
    @include('front.partials._header')
    <!-- End header -->

    <!-- Start main container -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('front.errors._message')
            </div>
            <div class="row">
                <div class="col-sm-5 login-box">
                    <div class="panel panel-default">
                        <div class="panel-intro text-center">
                            <img class="connexion-logo" src="{{asset('/front/images/pinsdeal.png')}}" alt="Pinsdeal Ivoire">
                            <!--<h2 class="logo-title">
                                <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> PINS<span>DEAL </span> </h2>-->
                        </div>
                        <div class="panel-body">
                            @include('front.errors._list')
                            <form role="form" method="post" action="{{route('auth.login-post')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="sender-email" class="control-label">Nom d'utilisateur / Email:</label>
                                    <div class="input-icon"> <i class="icon-user fa"></i>
                                        <input name="login" id="sender-email" type="text" placeholder="Nom d'utilisateur ou Email" class="form-control email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user-pass" class="control-label">Mot de passe:</label>
                                    <div class="input-icon"> <i class="icon-lock fa"></i>
                                        <input name="password" type="password" class="form-control" placeholder="Mot de passe" id="user-pass">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary  btn-block" value="Connexion">
                                </div>
                                <div class="form-group">
                                    <p class="text-center"> OU </p>
                                </div>
                                <div class="form-group">
                                    <a style="background-color: #3b5998" class="btn icon-facebook-rect btn-default btn-block" href="{{ route('social.redirect', ['provider' => 'facebook']) }}">Se connecter avec facebook</a>
                                </div>
                                <div class="form-group">
                                    <a style="background-color: #DD4B39" class="btn icon-googleplus-rect btn-default btn-block" href="{{ route('social.redirect', ['provider' => 'google']) }}">Se connecter avec google+</a>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <label class="checkbox pull-left">
                                <input type="checkbox" id="remember" name="remember"> Rester Connecté
                            </label>
                            <p class="text-center pull-right"> <a href="{{ route('auth.password') }}"> Mot de passe perdu ? </a> </p>
                            <div style=" clear:both"></div>
                        </div>
                    </div>
                    <div class="login-box-btm text-center">
                        <p> Pas de compte? <br>
                            <a href="{{route('auth.register')}}"><strong>Inscrivez-Vous !</strong> </a> </p>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- End main container -->

    <!-- Start footer -->
    @include('front.partials._footer')
    <!-- End footer -->

@endsection
