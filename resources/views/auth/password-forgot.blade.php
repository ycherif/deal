@extends('front.master')

@section('content')

    <!-- Start header -->
    @include('front.partials._header')
    <!-- End header -->

    <!-- Start main container -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-offset-4">
                    @include('front.errors._message')
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 login-box">
                    <div class="panel panel-default">
                        <div class="panel-intro text-center">
                            <h2 class="logo-title">
                                <img class="connexion-logo" src="{{asset('/front/images/pinsdeal.png')}}" alt="logo pinsdeal ivoire"></h2>
                        </div>
                        <div class="panel-body">
                            @include('front.errors._list')
                            <form role="form" method="post" action="{{ route('auth.password') }}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label for="sender-email" class="control-label">Email:</label>
                                    <div class="input-icon"> <i class="icon-user fa"></i>
                                        <input id="sender-email" type="text" name="email" value="{{ old('email') }}" placeholder="Entez votre email" class="form-control email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Réinitialiser le mot de passe</button>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <p class="text-center "> <a href="{{ route('auth.login') }}"> Connexion </a> </p>
                            <div style=" clear:both"></div>
                        </div>
                    </div>
                    <div class="login-box-btm text-center">
                        <p> Vous n'aviez pas de compte? <br>
                            <a href="{{ route('auth.register') }}"><strong>Inscrivez-vous maintenant!</strong> </a> </p>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- End main container -->

    <!-- Start footer -->
    @include('front.partials._footer')
    <!-- End footer -->

@endsection