@extends('front.master')

@section('content')

    <!-- Start header -->
    @include('front.partials._header')
    <!-- End header -->

    <!-- Start main container -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 login-box">
                    <div class="panel panel-default">
                        <div class="panel-intro text-center">
                            <h2 class="logo-title">
                                <img class="connexion-logo" src="{{asset('/front/images/pinsdeal.png')}}" alt="logo Pinsdeal Ivoire"></h2>
                        </div>
                        @include('front.errors._list')
                        @include('front.errors._message')
                        <div class="panel-body">
                            <form role="form" method="post" action="{{ route('auth.reset-post', ['token' => $token]) }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label for="sender-pass" class="control-label">Nouveau mot de passe:</label>
                                    <div class="input-icon"> <i class="icon-user fa"></i>
                                        <input id="sender-pass" type="password" name="password"  class="form-control" placeholder="Entez votre nouveau mot de passe">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sender-pass-confirm" class="control-label">Confirmer le mot de passe:</label>
                                    <div class="input-icon"> <i class="icon-user fa"></i>
                                        <input id="sender-pass-confirm" type="password" name="password_confirmation" class="form-control" placeholder="Confirmer le mot de passe">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Modifier mon mot de passe</button>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <p class="text-center "> <a href="{{ route('auth.login') }}"> Connexion </a> </p>
                            <div style=" clear:both"></div>
                        </div>
                    </div>
                    <div class="login-box-btm text-center">
                        <p> Vous n'avez pas de compte ? <br>
                            <a href="{{ route('auth.register') }}"><strong>Inscription !</strong> </a> </p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End main container -->

    <!-- Start footer -->
    @include('front.partials._footer')
    <!-- End footer -->

@endsection