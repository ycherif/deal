@extends('front.master')


@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="PINSSERVICE AFRICA">
    <meta name="description" lang="fr" content="{{ str_limit(config('deal.reg_desc'), 250, '...') }}">
    <meta property="og:description" content="{{ str_limit(config('deal.reg_desc'), 250, '...') }}"/>
    <meta name="category" content="{{config('deal.category')}}">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="{{ str_limit(config('deal.home_title'), 50, '...') }}" />
    <meta property="og:site_name" content="{{ str_slug(config('deal.country')).'.pinsdeal.com' }}"/>
@endsection

@section('style')

    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
    <!-- Start header -->
    @include('front.partials._header')
    <!-- End header -->
    <!-- Start main container -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-8 page-content">
                    <div class="inner-box category-content">
                        <h2 class="title-2"> <i class="icon-user-add"></i> Cr&eacute;er votre compte, c'est gratuit !!! </h2>
                        <div class="row">
                            <div class="col-sm-12">
                                @include('front.errors._list')
                                <form class="form-horizontal" method="post" action="{{ route('auth.register-post') }}">
                                    {!! csrf_field() !!}
                                    <fieldset>
                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Vous êtes <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="type" id="optionsRadios2" value="P" checked>
                                                        Particulier </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="type" id="optionsRadios1" value="B" >
                                                        Professionel </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Nom d'utilisateur <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input name="username" value="{{old('username')}}" placeholder="Nom d'utilisateur" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Nom <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input name="lastname" value="{{old('lastname')}}" placeholder="Nom" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Prénom <sup> *</sup></label>
                                            <div class="col-md-6">
                                                <input name="firstname" value="{{old('firstname')}}" placeholder="Prénom" class="form-control input-md"  type="text">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label for="email" class="col-md-4 control-label">Email <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input type="text" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="Email">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Numéro de téléphone <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input name="phone" value="{{old('phone')}}" placeholder="Numéro de téléphone" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Ville <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <select name="city_id"  class="form-control">
                                                    @if($cities)
                                                        @foreach($cities as $city)
                                                            <option value="{{$city->id}}"> {{$city->name}} </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Genre <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <label for="Gender-0">
                                                        <input name="gender" id="Gender-0" value="M" checked="checked" type="radio">
                                                        Homme </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="Gender-1">
                                                        <input name="gender" id="Gender-1" value="F" type="radio">
                                                        Femme </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label for="password" class="col-md-4 control-label">Mot de passe <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Mot de passe">
                                                <p class="help-block">Au moins 6 caractères  </p>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label for="password_confirmation" class="col-md-4 control-label">Confirmer le mot de passe <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirmer votre mot de passe">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-md-4 control-label">Confirmer <sup>*</sup></label>
                                            <div class="g-recaptcha col-md-6" data-sitekey="{{ env('GG_RECAP_SITE') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <div class="termbox mb10">
                                                    <label class="checkbox-inline" for="checkboxes-1">
                                                        <input name="condition" id="checkboxes-1" value="1" type="checkbox">
                                                        J'ai lu et j'accepte les <a href="{{route('condition')}}"> Conditions d'utilisation</a> </label>
                                                </div>
                                                <div style="clear:both"></div>
                                                <input type="submit" class="btn btn-primary" value="Enregistrer maintenant">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p class="text-center"> OU </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <a style="background-color: #3b5998" class="btn icon-facebook-rect btn-default btn-block" href="{{route('social.redirect','facebook')}}">Se connecter avec facebook</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <a style="background-color: #DD4B39" class="btn icon-googleplus-rect btn-default btn-block" href="{{route('social.redirect','google')}}">Se connecter avec google+</a>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 reg-sidebar">
                    <div class="reg-sidebar-inner text-center">
                        <div class="promo-text-box"> <i class=" icon-picture fa fa-4x icon-color-1"></i>
                            <h3><strong>Poster une annonce gratuite</strong></h3>
                            <p>PinsDeal {{config('deal.country')}}, Déposez vos annonces en ligne gratuitement, simplement et rapidement. </p>
                        </div>
                        <div class="promo-text-box"> <i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
                            <h3><strong>Créer et gérer les annonces</strong></h3>
                            <p> Sur PinsDeal {{config('deal.country')}},Vous pouvez créer, modifier ou supprimer vos annonces à tout moment dans votre tableau de bord.</p>
                        </div>
                        <div class="promo-text-box"> <i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
                            <h3><strong>Créer des listes d'annonces favorites</strong></h3>
                            <p> Vous pouvez cliquer simplement sur le petit coeur  présent sur l'annonce pour l'inclure dans votre liste de favoris</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- End main container -->

    <!-- Start footer -->
    @include('front.partials._footer')
    <!-- End footer -->

@endsection