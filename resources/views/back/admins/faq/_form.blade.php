<form class="form-horizontal" method="post" action="{{ !empty($faq) ? route('admin.faq.edit', $faq->id) : route('admin.faq.create') }}">
    <fieldset>
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Question</label>
            <div class="col-md-8">
                <input name="question" placeholder="La Question" class="form-control input-md" type="text" {!! !empty($faq->question) ? 'value="'.$faq->question.'"' : '' !!}>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Reponse</label>
            <div class="col-md-8">
                <textarea name="response" placeholder="La Reponse" class="form-control input-md" type="text">{!! !empty($faq->response) ? $faq->response : '' !!}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name"></label>
            <div class="col-md-8">
                <input class="btn btn-primary col-md-12" type="submit" value="{{ !empty($faq) ? 'Mettre à jour' : 'Envoyer' }}" >
            </div>
        </div>
    </fieldset>
</form>