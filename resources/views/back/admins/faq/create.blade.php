@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-user-plus"></i> Ajouter question</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Créer une nouvelle question</h3></div>
                    <div class="panel-body">
                        @include('back.admins.faq._form')
                    </div> <!-- panel-body -->
                </div> <!-- panel -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                </div>
            </div>
        </div>
    </div>

@stop