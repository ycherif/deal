@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-question-circle"></i> FAQ </h3>
        </div>
        <div class="row" style="padding-bottom: 10px;">
            <div class="col-md-12">
                <a href="{{ route('admin.faq.create') }}" class="btn btn-inverse pull-right btn-lg"><i class="fa fa-pencil"></i> Ajouter une question</a>
            </div>
        </div>

        @include('back.errors._list')
        @include('back.errors._message')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des questions</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Reponse</th>
                                            <th>Email</th>
                                            <th>Statut</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($faqs as $faq)
                                            <tr>
                                                <td>{{ str_limit($faq->question, 30, $end = '...') }}</td>
                                                <td>{{ str_limit($faq->response, 30, $end = '...') }}</td>
                                                <td>{{ $faq->email }}</td>
                                                <td><p><strong></strong> <small class="label label-{{ ($faq->status == true) ? 'success' : 'warning' }} adlistingtype"> {{ ($faq->status == true) ? 'Publiée':'Pas Publiée' }}</small></p></td>
                                                <td>
                                                    @if(!empty($faq->response))
                                                        @if($faq->status == true)
                                                            <a href="{{ route('admin.faq.unpublish',[$faq->id]) }}" class="btn btn-warning"><i class="fa fa-lock"> Retirer</i></a>
                                                        @else
                                                            <a href="{{ route('admin.faq.publish',[$faq->id]) }}" class="btn btn-warning"><i class="fa fa-unlock"></i> Publier</a>
                                                        @endif
                                                    @endif
                                                    &nbsp;&nbsp;
                                                    <a href="{{ route('admin.faq.edit',[$faq->id] ) }}" class="btn btn-primary "><i class="fa fa-pencil-square-o"></i></a>
                                                        @if(!empty($faq->response) && !empty($faq->email))
                                                            <form id="send-mail-confirm" style="display: inline-block;" method="post" action="{{ route('admin.faq.sendMail',[$faq->id]) }}">
                                                                {!! csrf_field() !!}
                                                                <button type="submit" class="btn btn-success "><i class="fa fa-envelope"></i></button>
                                                            </form>
                                                        @endif

                                                    @if(Auth::user()->hasRole('admin'))
                                                        &nbsp;&nbsp;
                                                        <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ route('admin.faq.destroy',[$faq->id]) }}">
                                                            {!! csrf_field() !!}
                                                            <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $faqs->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                        title: "Êtes-vous sûr?",
                        text: "Après suppression votre annonce ne sera plus disponible!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        cancelButtonText: "Non, annuler!",
                        confirmButtonText: "Oui, supprimer!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            form.submit();
                        } else {
                            swal("Annulation", "Vous avez annuler la suppression :)", "success");
                        }
                    });
        });

        $('#send-mail-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                        title: "Envoyer la reponse ?",
                        text: "Un email sera envoyè à celui qui a posé la question !",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        cancelButtonText: "Non, annuler!",
                        confirmButtonText: "Oui, envoyer!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            form.submit();
                        } else {
                            swal("Annulation", "Vous avez annuler l'envoie :)", "success");
                        }
                    });
        });

    </script>
@endsection