@extends('back.master')
@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-money"></i> Liste de mes paiements</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Liste des paiements</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Boutique</th>
                                            <th>Plan</th>
                                            <th>Date d'expiration</th>
                                            <th>Mode de paiement</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($invoices as $invoice)
                                            <tr>
                                                <td>{{ $invoice->shop->name }}</td>
                                                <td>{{ $invoice->plan.' mois' }}</td>
                                                <td>{{ $invoice->expiration_date }}</td>
                                                <td>{{ $invoice->mode }}</td>
                                                <td><p><strong></strong> <small class="label label-{{ ($invoice->status == true) ? 'success' : 'warning' }} adlistingtype"> {{ ($invoice->status == true) ? 'Paiement validé':'En attente' }}</small></p></td>
                                                <td>
                                                    @if($invoice->status == true )
                                                        @if ($invoice->expiration_date > date('NOW'))

                                                            <a href="{{ route('admin.invoice.disapprove',[$invoice->id]) }}" class="btn btn-danger"><i class="fa fa-lock"> Revoquer </i></a>
                                                        @else
                                                            <a data-toggle="modal" href="" class="btn btn-warning validationBtn" data_value="{{$invoice->id}}"><i class="fa fa-envelope">No Action </i></a>
                                                        @endif

                                                    @else
                                                        <a data-toggle="modal" href="#validationShop" class="btn btn-warning validationBtn" data_value="{{$invoice->id}}"><i class="fa fa-unlock"> Valider </i></a>
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="validationShop" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Valider le paiement de la boutique</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('admin.invoice.approve')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="invoiceId" value="" id="invoiceCode"/>

                        <div class="form-group">
                            <label for="sender-email" class="control-label">Code de vérification:</label>
                            <input  type="text" name="invoiceCode" placeholder="Le code de la boutique" class="form-control">
                        </div>

                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary pull-right">Envoyer!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{asset('front/js/jquery.min.js')}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('.validationBtn').click(function() {
                $("#invoiceCode").val($(this).attr('data_value'));
                //alert();
            });
        });
    </script>
@endsection
