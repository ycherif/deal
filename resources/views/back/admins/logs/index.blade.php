@extends('back.master')

@section('content')

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title"><i class="fa fa-exclamation-triangle"></i> Erreurs</h3>
    </div>
    @include('back.errors._list')
    @include('back.errors._message')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Liste des érreurs du site</h3>
                </div>
                <div class="panel-body">
                    <div class="row row-horizon">
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Message</th>
                                        <th>Type</th>
                                        <th>Auteur</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($logs as $log)
                                    <tr>
                                        <td>{{ $log->message  }}</td>
                                        <td><span class="text-{{ $log->type }}"> {{ $log->type }} </span></td>
                                        @if($log->user)
                                            <td>{{ $log->user->firstname .' '. $log->user->lastname .'  '.'( '.'Id : '.$log->user->id.' )' }}</td>
                                        @else
                                            <td>User Inconnu</td>
                                        @endif
                                        <td>{{ date('d-m-Y',strtotime($log->created_at)) }}</td>
                                        <td>
                                            @if(Auth::user()->hasRole('admin'))
                                            &nbsp;&nbsp;
                                            <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ route('admin.logs.destroy',[$log->id]) }}">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="text-center">
                {!! $logs->render() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>

    $('.delete-ad-confirm').click('submit', function(e) {
        var form = this;
        e.preventDefault();
        swal({
                title: "Êtes-vous sûr?",
                text: "Après suppression le log ne sera plus disponible !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonText: "Non, annuler!",
                confirmButtonText: "Oui, supprimer!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal("Annulation", "Vous avez annuler la suppression :)", "success");
                }
            });
    });

</script>
@endsection