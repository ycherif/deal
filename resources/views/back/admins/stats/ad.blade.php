@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-bullhorn"></i> Statistiques des annonces</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet"><!-- /primary heading -->
                    <div class="portlet-heading">
                        <h3 class="portlet-title text-dark text-uppercase">
                            Graphe des publications
                        </h3>
                        <div class="portlet-widgets">
                            <span class="divider"></span>
                            <a data-toggle="collapse" data-parent="#accordion1" href="#portlet1" class="collapsed"><i class="ion-minus-round"></i></a>
                            <span class="divider"></span>
                            <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="portlet1" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <canvas id="canvas" class="col-sm-1" ></canvas>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet"><!-- /primary heading -->
                    <div class="portlet-heading">
                        <h3 class="portlet-title text-dark text-uppercase">
                            Diagramme des états
                        </h3>
                        <div class="portlet-widgets">
                            <span class="divider"></span>
                            <a data-toggle="collapse" data-parent="#accordion2" href="#portlet2" class="collapsed"><i class="ion-minus-round"></i></a>
                            <span class="divider"></span>
                            <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="portlet2" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-sm-7">
                                    <canvas id="chart-area" class="col-sm-10"></canvas>
                                </div>
                                <div class="col-md-4 text-center">
                                    <h4 class="m-b-15 text-uppercase">Label:</h4>
                                    <p class="label label-primary" style="font-size: 15px"> Publiée </p><br><br>
                                    <p class="label label-warning" style="font-size: 15px"> En Attente </p>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                </div>
            </div>
        </div>
    </div>

@endsection

{{--*/ $nbreAdsPublish = 0 /*--}}
{{--*/ $nbreAdsPending = 0 /*--}}
@foreach($ads as $ad)
    @if($ad->status == true)
        {{--*/ $nbreAdsPublish++ /*--}}
    @else
        {{--*/ $nbreAdsPending++ /*--}}
    @endif
@endforeach

@section('script')
    <script src="{{ asset('back/assets/chartjs/Chart.min.js') }}"></script>
    <script>

        var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

        var barChartData = {
            labels : [@foreach($labels as $label)
                          "{!! $label !!}",
                @endforeach],
            datasets : [
                {
                    fillColor : "rgba(151,187,205,0.5)",
                    strokeColor : "rgba(151,187,205,0.8)",
                    highlightFill : "rgba(151,187,205,0.75)",
                    highlightStroke : "rgba(151,187,205,1)",
                    data : [ @foreach($datasAds as $count)
                                {!! $count !!},
                        @endforeach ]
                }
            ]

        };

        var doughnutData = [

            {
                value: {!! $nbreAdsPublish !!},
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Publiée"
            },
            {
                value: {!! $nbreAdsPending !!},
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "En Attente"
            }

        ];

        window.onload = function(){
            var cty = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(cty).Bar(barChartData, {
                responsive : true
            });

            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
        }
    </script>

@endsection