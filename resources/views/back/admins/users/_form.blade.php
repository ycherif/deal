<div class="form-group required">
        <label class="col-md-4 control-label">Il est <sup>*</sup></label>
        <div class="col-md-6">
            <div class="radio">
                <label>
                    <input type="radio" name="type" id="optionsRadios2" value="P" {{ isset($user)  ? ( $user->type == 'P' ? 'checked' : '') : 'checked'}}>
                    Particulier </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="type" id="optionsRadios1" value="B" {{ isset($user) ? ( $user->type == 'B' ? 'checked' : '') : ''}} >
                    Professionel </label>
            </div>
        </div>
</div>

<div class="form-group required">
    <label class="col-md-4 control-label">Nom d'utilisateur <sup>*</sup></label>
    <div class="col-md-6">
        <input name="username" value="{{ isset($user)  ? $user->username : old('username')}}" placeholder="Nom d'utilisateur" class="form-control input-md" type="text">
    </div>
</div>

<div class="form-group required">
    <label class="col-md-4 control-label">Nom <sup>*</sup></label>
    <div class="col-md-6">
        <input name="lastname" value="{{ isset($user)  ? $user->lastname : old('lastname')}}" placeholder="Nom" class="form-control input-md" type="text">
    </div>
</div>

<div class="form-group required">
    <label class="col-md-4 control-label">Prénom <sup> *</sup></label>
    <div class="col-md-6">
        <input name="firstname" value="{{ isset($user)  ? $user->firstname : old('firstname')}}" placeholder="Prénom" class="form-control input-md"  type="text">
    </div>
</div>
@if(!isset($user))
    <div class="form-group required">
        <label for="email" class="col-md-4 control-label">Email <sup>*</sup></label>
        <div class="col-md-6">
            <input type="text" name="email" value="{{ old('email')}}" class="form-control" id="email" placeholder="Email">
        </div>
    </div>
@endif

<div class="form-group required">
    <label class="col-md-4 control-label">Numéro de téléphone <sup>*</sup></label>
    <div class="col-md-6">
        <input name="phone" value="{{ isset($user)  ? $user->phone : old('phone')}}" placeholder="Numéro de téléphone" class="form-control input-md" type="text">
    </div>
</div>

<div class="form-group required">
    <label class="col-md-4 control-label">Ville <sup>*</sup></label>
    <div class="col-md-6">
        <select name="city_id"  class="form-control">
            @if($cities)
                @foreach($cities as $city)
                    @if(isset($user))
                        @if($user->city_id == $city->id)
                            <option value="{{$city->id}}" selected> {{$city->name}} </option>
                        @else
                            <option value="{{$city->id}}"> {{$city->name}} </option>
                        @endif
                    @else
                        <option value="{{$city->id}}"> {{$city->name}} </option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
@if(!isset($user))
    <div class="form-group required">
        <label class="col-md-4 control-label">Genre <sup>*</sup></label>
        <div class="col-md-6">
            <div class="radio">
                <label for="Gender-0">
                    <input name="gender" id="Gender-0" value="M" checked="checked" type="radio">
                    Homme </label>
            </div>
            <div class="radio">
                <label for="Gender-1">
                    <input name="gender" id="Gender-1" value="F" type="radio">
                    Femme </label>
            </div>
        </div>
    </div>
@endif
<div class="form-group required">
    <label class="col-md-4 control-label">Rôle <sup>*</sup></label>
    <div class="col-md-6">
        <select name="role_slug"  class="form-control">
            @foreach($roles as $role)
                @if(isset($user))
                    @if($user->roles[0]->id == $role->id)
                        <option value="{{$role->slug}}" selected> {{$role->slug == 'member' ? 'Membre' : 'Moderateur'}} </option>
                    @else
                        <option value="{{$role->slug}}"> {{$role->slug == 'member' ? 'Membre' : 'Moderateur'}} </option>
                    @endif
                @else
                    <option value="{{$role->slug}}"> {{$role->slug == 'member' ? 'Membre' : 'Moderateur'}} </option>
                @endif
            @endforeach
        </select>
    </div>
</div>