@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-user-plus"></i> Ajouter utilisateur</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Créer un nouvel utilisateur</h3></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="{{route('admin.users.store')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <fieldset>
                                @include('back.admins.users._form')
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-8">
                                        <div style="clear:both"></div>
                                        <input type="submit" class="btn btn-primary" value="Enregistrer maintenant">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div> <!-- panel-body -->
                </div> <!-- panel -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                </div>
            </div>
        </div>
    </div>

@stop