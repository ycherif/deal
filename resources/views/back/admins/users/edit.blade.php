@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-pencil"></i> Mise à jour</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Mettre à jour un utilisateur</h3></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="{{route('admin.users.update',[$user->id])}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <fieldset>
                                @include('back.admins.users._form')
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-8">
                                        <div class="termbox mb10">
                                            <label class="checkbox-inline" for="checkboxes-1">
                                                <input name="changePassword" id="checkboxes-1" value="1" type="checkbox">
                                                Modifier le mot de passe  </label>
                                        </div>
                                        <div style="clear:both"></div>
                                        <hr>
                                        <input type="submit" class="btn btn-primary" value="Metre à jour maintenant">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div> <!-- panel-body -->
                </div> <!-- panel -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                </div>
            </div>
        </div>
    </div>

@stop