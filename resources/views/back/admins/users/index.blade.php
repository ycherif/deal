@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-list"></i> Utilisateurs</h3>
        </div>
        <div class="row" style="padding-bottom: 10px;">
            <div class="col-md-12">
                <a href="{{ route('admin.users.create') }}" class="btn btn-inverse pull-right btn-lg"><i class="fa fa-pencil"></i> Créer un utilisateur</a>
            </div>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des utilisateurs</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Email</th>
                                            <th>Rôle</th>
                                            <th>Statut</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            @if(!$user->hasRole('admin'))
                                                <tr>
                                                    <td>{{ $user->firstname }}</td>
                                                    <td>{{ $user->lastname }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->roles[0]->name }}</td>
                                                    <td>{{ $user->active ? 'Actif' : 'Inactif' }}</td>
                                                    <td>
                                                        @if($user->deleted_at == null)
                                                            <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></a>
                                                            &nbsp;&nbsp;
                                                            <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ route('admin.users.destroy',[$user->id]) }}">
                                                                {!! csrf_field() !!}
                                                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                                <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('admin.users.restore',[$user->id]) }}" class="btn btn-primary"><i class="fa fa-reply-all"></i> Restorer</a>
                                                            &nbsp;&nbsp;
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $users->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="contactModerator" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Envoyer un mail collectif</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('admin.moderator.contact')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="receiver" id="receiver" value=""/>
                        <input type="hidden" name="username" id="name" value="">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nom:</label>
                            <input class="form-control required" id="recipient-name" name="name" placeholder="Votre nom" type="text" {{ Auth::user() ? 'value='.Auth::user()->lastname : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="sender-email" class="control-label">E-mail:</label>
                            <input id="sender-email" type="text" name="sender" placeholder="Votre adresse eamil" class="form-control email" {{ Auth::user() ? "value=".Auth::user()->email : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="recipient-Phone-Number" class="control-label">Téléphone (Facultatif):</label>
                            <input type="text" class="form-control" name="phone" id="recipient-Phone-Number" placeholder="Votre numéro de téléphone" {{ Auth::user() ? 'value='.Auth::user()->phone : '' }}>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span>(300) </span>:</label>
                            <textarea class="form-control" name="message" placeholder="Votre message ici.."></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary pull-right">Envoyer!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                title: "Êtes-vous sûr?",
                text: "Après suppression l'utilisateur n'aura plus accès à son compte !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonText: "Non, annuler!",
                confirmButtonText: "Oui, supprimer!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal("Annulation", "Vous avez annuler la suppression :)", "success");
                }
            });
        });

    </script>
@endsection