@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-list"></i> Modérateurs</h3>
        </div>
        <div class="row" style="padding-bottom: 10px;">
            <div class="col-md-12">
                <a href="{{ route('admin.users.create') }}" class="btn btn-inverse pull-right btn-lg"><i class="fa fa-pencil"></i> Créer un utilisateur</a>
            </div>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste des modérateurs</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Email</th>
                                            <th>Rôle</th>
                                            <th>Statut</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            @if(!$user->hasRole('admin'))
                                                <tr>
                                                    <td>{{ $user->firstname }}</td>
                                                    <td>{{ $user->lastname }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        @foreach($user->roles as $rule)
                                                            <input type="text" readonly value="{{$rule->name}}" class="role-show">
                                                        @endforeach
                                                    </td>
                                                    <td>{{ $user->active ? 'Actif' : 'Inactif' }}</td>
                                                    <td>
                                                        @if($user->deleted_at == null)
                                                            <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></a>
                                                            &nbsp;&nbsp;
                                                            <a class="btn btn-primary moderatorMail" data-toggle="modal" data_value="{{$user}}" href="#contactModerator"><i class="fa fa-envelope"></i> </a>&nbsp;&nbsp;
                                                            <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ route('admin.users.destroy',[$user->id]) }}">
                                                                {!! csrf_field() !!}
                                                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                                <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>

                                                            </form>

                                                        @else
                                                            <a href="{{ route('admin.users.restore',[$user->id]) }}" class="btn btn-primary"><i class="fa fa-reply-all"></i> Restorer</a>
                                                            &nbsp;&nbsp;
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $users->render() !!}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript" src="{{asset('front/js/jquery.min.js')}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('.moderatorMail').click(function() {
                var user = jQuery.parseJSON($(this).attr('data_value'));
                $("#receiver").val(user.email);
                $("#identity").val(user.firstname.charAt(0).toUpperCase()+ user.firstname.slice(1) + '  '+user.lastname.charAt(0).toUpperCase()+ user.lastname.slice(1));
                $("#name").val(user.firstname.charAt(0).toUpperCase()+ user.firstname.slice(1) + '  '+user.lastname.charAt(0).toUpperCase()+ user.lastname.slice(1));

            });
        });
    </script>
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                        title: "Êtes-vous sûr?",
                        text: "Après suppression l'utilisateur n'aura plus accès à son compte !",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        cancelButtonText: "Non, annuler!",
                        confirmButtonText: "Oui, supprimer!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            form.submit();
                        } else {
                            swal("Annulation", "Vous avez annuler la suppression :)", "success");
                        }
                    });
        });

    </script>
@endsection