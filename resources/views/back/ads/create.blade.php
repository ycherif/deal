@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-bullhorn"></i> Poster une nouvelle annonce</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Informations de l'annonce</h2>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" action="{{ route('ads.store') }}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            @if(!empty($shops))
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Boutique</label>
                                    <div class="col-md-10">
                                        <select name="shop" id="shop" class="form-control">
                                            @foreach ($shops as $shop)
                                                <option value="{{$shop->slug}}">{{$shop->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-2 control-label">Catégorie</label>
                                <div class="col-md-10">
                                    <select name="category" id="category" class="form-control">
                                            @if(!empty($shop))
                                                @foreach ($categories as $parent)
                                                    @if($parent['slug_search'] == $shop['slug_search'])
                                                        <optgroup label="{{$parent->name}}">
                                                            @foreach ($parent->children as $children)
                                                                <option value="{{$children->id}}"> {{$children->name}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach ($categories as $parent)
                                                    <optgroup label="{{$parent->name}}">
                                                        @foreach ($parent->children as $children)
                                                            <option value="{{$children->id}}"> {{$children->name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Type</label>
                                <div class="col-md-10">
                                    <label class="radio-inline" for="type">
                                        <input name="type" id="type" value="D" type="radio">
                                        Demande </label>
                                    <label class="radio-inline" for="radios-1">
                                        <input name="type" id="type" value="O" checked="checked" type="radio">
                                        Offre </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="title">Titre</label>
                                <div class="col-md-10">
                                    <input id="popovertext" name="title" placeholder="Titre de l'annonce" class="form-control input-md" type="text" value="{{ old('title') }}">
                                    <span class="help-block">Un titre bien descriptif a besoin d'au moins 60 caractères. </span> </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Description </label>
                                <div class="col-md-10">
                                    <textarea class="form-control" id="txteditor" name="description" rows="8" placeholder="Mettez une description de votre annonce" onkeyup="countChar(this)">{{ old('description') }}</textarea>
                                    <p class="help-block"><span>Il reste : </span><span id="actu"></span><span> caractères</span></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="price">Prix</label>
                                <div class="col-md-5">
                                    <div class="input-group"> <span class="input-group-addon">CFA </span>
                                        <input id="price" name="price" class="form-control" placeholder="Prix" type="text" value="{{ old('price') }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox checkbox-info">
                                        <label>
                                            <input type="checkbox" name="negotiable">
                                            Négociable </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="textarea"> Images </label>
                                <div class="col-md-10">
                                    <div class="mb10">
                                        <input id="input-upload" type="file" accept="image/*" name="medias[]" class="file" multiple data-preview-file-type="text" >
                                    </div>
                                    <p class="help-block">Ajouter plusieur images à la fois. Le maximum d'images est 5!.</p>
                                </div>
                            </div>

                            <div class="panel-heading m-b-10">
                                <h2 class="panel-title title-ads">Vos informations</h2>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="seller-Location">Ville</label>
                                <div class="col-md-10">
                                    <select id="seller-Location" name="city" class="form-control">
                                        @if(!is_null($cities))
                                            @foreach($cities as $city)
                                                @if(!is_null($userCity) && $userCity->id == $city->id)
                                                    <option selected value="{{ $city->id }}">{{ $city->name }}</option>
                                                @else
                                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="firstname">Prénom</label>
                                <div class="col-md-10">
                                    <input id="firstname" name="firstname" value="{{ Auth::user()->firstname }}" class="form-control input-md " disabled required="" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="email">Email</label>
                                <div class="col-md-10">
                                    <input id="email" name="email" class="form-control" value="{{ Auth::user()->email }}" disabled required="" type="text">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="hidephone">
                                            <small> Masquer mon numéro de téléphone sur l'annonce.</small> </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="seller-Number">Téléphone</label>
                                <div class="col-md-10">
                                    <input id="seller-Number" name="phone" value="{{ Auth::user()->phone }}" class="form-control input-md" disabled type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-10"> <input type="submit" id="button1id" class="btn btn-inverse btn-lg" value="Poster votre annonce"/> </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection