@extends('front.master')

@section('style')
    <?php
            use Barryvdh\Debugbar\Storage;
            ?>
    <link href="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet">

@endsection

@section('content')

    {{--start header--}}
    @include('front.partials._header')
    {{--end header --}}

    <div class="main-container">
        <div class="container">
            @include('front.errors._message')
            @include('front.errors._list')
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2> <small class="label label-danger adlistingtype" style="padding: 6px;">{{ ($ad->user->type == 'B') ? 'Professionnel' : 'Personnel' }}</small>  <small class="label label-warning adlistingtype " style="padding: 6px;">{{ ($ad->type == 'D') ? 'Demande' : 'Offre' }}</small> <small class="label label-info adlistingtype" style="padding: 6px;">{{ ($ad->negotiable == true) ? 'Négociable' : 'Prix fixe' }}</small></h2>
                        <h2 class="pull-right">
                            @if(Auth::User() && !Auth::User()->hasRole('member') && $ad->status == true )
                                <a href="{{ route('admin.ads.disapprove',[$ad->id]) }}" class="btn btn-danger"><i class="fa fa-lock"> Retirer </i></a>
                            @elseif(Auth::User() && !Auth::User()->hasRole('member'))
                                <a href="{{ route('admin.ads.approve',[$ad->id]) }}" class="btn btn-primary-dark"><i class="fa fa-unlock"> Publier </i></a>
                            @endif
                        </h2>
                        <h2>{{ $ad->title }}</h2>
                        <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> {{ 'Le ' . date('d M Y à H:i:s',strtotime($ad->created_at)) }} </span> - <span class="category">{{ $ad->category->name }} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> {{ $ad->city->name }} </span> </span>
                        <div class="ads-image">
                            <h1 class="pricetag">  {{ number_format($ad->price, 0, '', '.') }} CFA</h1>
                            <ul class="bxslider">
                                @foreach($ad->medias as $media)
                                    <li><img src="{{config('deal.image').$media->url}}" alt="img"/></li>
                                @endforeach
                            </ul>
                            <div id="bx-pager">
                                @foreach($ad->medias as $key => $media)
                                    <a class="thumb-item-link" data-slide-index="{{ $key }}" href="#"><img src="{{ config('deal.image').$media->url}}" alt="img"/></a>
                                @endforeach
                            </div>
                        </div>

                        <div class="Ads-Details">
                            <h5 class="list-title"><strong>Détails de l'annonce</strong></h5>
                            <div class="row">
                                <div class="ads-details-info col-md-8">
                                    <p>{{ $ad->description }}</p>
                                </div>
                                <div class="col-md-4">
                                    <aside class="panel panel-body panel-details">
                                        <ul>
                                            <li>
                                                <p class=" no-margin "><strong>Prix : </strong> {{ $ad->price }} CFA </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Type : </strong> {{ ($ad->type == 'D') ? 'Demande' : 'Offre' }}</p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Ville : </strong> {{ $ad->city->name }} </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Catégorie : </strong> {{ $ad->category->name }}</p>
                                            </li>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                            <div class="content-footer text-left">
                                @if($ad->user->phone && $ad->hide_phone == false)
                                    <a class="btn  btn-info"><i class=" icon-phone-1"></i> {{ $ad->user->phone }} </a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Contacts du vendeur</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{ $ad->user->firstname .' '. $ad->user->lastname }}</h3>
                                        <p>Ville : <strong>{{ $ad->city->name }}</strong></p>
                                        <p> Rejoint le : <strong>{{ date('d M Y',strtotime($ad->created_at)) }}</strong></p>
                                    </div>
                                    <div class="user-ads-action">
                                        @if($ad->user->phone && $ad->hide_phone == false)
                                            <a class="btn  btn-info btn-block"><i class=" icon-phone-1"></i> {{ $ad->user->phone }} </a>
                                        @endif
                                    </div>


                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

    {{--start footer--}}
    @include('front.partials._footer')
    {{-- end footer--}}

@endsection

@section('script')

    <script src="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.min.js')  }}" type="text/javascript"></script>
    <script>
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
    </script>

@endsection