@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-heart"></i> Favoris</h3>
        </div>
        <div class="row" style="padding-bottom: 10px;">
            <div class="col-md-12">
                <a href="{{ route('ads.create') }}" class="btn btn-inverse pull-right btn-lg"><i class="fa fa-pencil"></i> Poster une annonce</a>
            </div>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liste de mes favoris</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Vues</th>
                                            <th>Catégorie</th>
                                            <th>Statut</th>
                                            <th>Prix</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ads as $ad)
                                            <tr>
                                                <td><a class="text-info" target="_blank" href="{{ route('ads.detail',[$ad->slug]) }}" title="{{ $ad->title }}">{{ $ad->title }}</a></td>
                                                <td><span class="label label-inverse">{{ $ad->visitors ? $ad->visitors : 0}}</span></td>
                                                <td>{{ $ad->category->name }}</td>
                                                <td><p><strong></strong> <small class="label label-{{ ($ad->status == true) ? 'success' : 'warning' }} adlistingtype"> {{ ($ad->status == true) ? 'En ligne':'L\'annonce n\'est plus en ligne' }}</small></p></td>
                                                <td>{{ $ad->price }}</td>
                                                <td>
                                                    &nbsp;&nbsp;
                                                    <a target="_blank" href="{{ route('ads.detail',[$ad->slug]) }}" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                                                    <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ route('favorites.destroy',[$ad->id]) }}">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $ads->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                title: "Êtes-vous sûr?",
                text: "Après suppression ce favori ne sera plus disponible!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonText: "Non, annuler!",
                confirmButtonText: "Oui, supprimer!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal("Annulation", "Vous avez annuler la suppression :)", "success");
                }
            });
        });

    </script>
@endsection