@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-bullhorn"></i> Créer un nouveau paiment</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Informations de paiments</h2>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('invoice.store') }}">
                            <fieldset>
                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Boutiques <sup>*</sup></label>
                                    <div class="col-md-8">
                                        <select name="shop" id="shop" class="form-control">
                                            @foreach ($shops as $shop)
                                                <option value="{{$shop->id}}">{{$shop->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="textinput-name">Code de paiement <sup>*</sup></label>
                                    <div class="col-md-8">
                                        <input name="code" placeholder="Code" class="form-control input-md" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Mode paiement <sup>*</sup></label>
                                    <div class="col-md-8">
                                        <select name="mode" id="mode" class="form-control">
                                            <option value="ORANGE">Orange</option>
                                            <option value="AIRTEL">Airtel</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Plan <sup>*</sup></label>
                                    <div class="col-md-8">
                                        <select name="plan" id="plan" class="form-control">
                                            <option value="3">3 Mois (10.000 CFA)</option>
                                            <option value="6">6 Mois (20.000 CFA)</option>
                                            <option value="12">12 Mois (30.000 CFA)</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="textinput-name"></label>
                                    <div class="col-md-8">
                                        <input class="btn btn-primary col-md-12" type="submit" >
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection