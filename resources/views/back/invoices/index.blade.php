@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-money"></i> Liste de mes paiements</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Liste des paiements</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Boutique</th>
                                            <th>Code</th>
                                            <th>Mode de paiement</th>
                                            <th>Plan</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($invoices as $invoice)
                                            <tr>
                                                <td>{{ $invoice->shop->name }}</td>
                                                <td>{{ $invoice->code }}</td>
                                                <td>{{ $invoice->mode }}</td>
                                                <td>{{ $invoice->plan.' mois' }}</td>
                                                <td><p><strong></strong> <small class="label label-{{ ($invoice->status == true) ? 'success' : 'warning' }} adlistingtype"> {{ ($invoice->status == true) ? 'Paiement validé':'En attente' }}</small></p></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection