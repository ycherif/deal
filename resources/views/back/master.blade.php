<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="PinsDeal">

    <title>PinsDeal - Administration</title>

    <!-- Google-Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/back/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/back/css/bootstrap-reset.css') }}" rel="stylesheet">

    <!--Icon-fonts css-->
    <link href="{{ asset('/back/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('/back/assets/ionicon/css/ionicons.min.css') }}" rel="stylesheet" />

    <!-- sweet alerts -->
    <link href="{{ asset('/back/assets/sweet-alert/sweet-alert.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('/back/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/back/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('/back/css/style-responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('/back/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/back/css/main.css') }}" rel="stylesheet">

    @yield('style')
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/back/js/html5shiv.js') }}"></script>
    <script src="{{ asset('/back/js/respond.min.js') }}"></script>
    <![endif]-->

    <SCRIPT LANGUAGE="JavaScript">
        <!-- Disable
        function disableselect(e){
            return false
        }
        function reEnable(){
            return true
        }
        //if IE4+
        document.onselectstart=new Function ("return false")
        document.oncontextmenu=new Function ("return false")
        //if NS6
        if (window.sidebar){
            document.onmousedown=disableselect
            document.onclick=reEnable
        }
        //-->
    </script>

</head>


<body>

    <!-- Aside Start-->
    @include('back.partials._leftSidebar')
    <!-- Aside Ends-->

    <!--Main Content Start -->
    <section class="content">

        <!-- Header -->
        @include('back.partials._header')
        <!-- Header Ends -->

        <!-- Page Content Start -->
        <!-- ================== -->
        @yield('content')
        <!-- Page Content Ends -->
        <!-- ================== -->

        <!-- Footer Start -->
        <footer class="footer">
            {{date('Y')}} © PinsDeal {{config('deal.country')}}, Tous Droits Réservés.
        </footer>
        <!-- Footer Ends -->

    </section>
    <!-- Main Content Ends -->


<!-- js -->
<script src="{{ asset('/back/js/jquery.js') }}"></script>
<script src="{{ asset('/back/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/back/js/modernizr.min.js') }}"></script>
<script src="{{ asset('/back/js/wow.min.js') }}"></script>
<script src="{{ asset('/back/js/jquery.nicescroll.js') }}" type="text/javascript"></script>


<!-- sweet alerts -->
<script src="{{ asset('/back/assets/sweet-alert/sweet-alert.min.js') }}"></script>

<script src="{{ asset('/back/js/jquery.app.js') }}"></script>

@yield('script')
</body>
</html>
