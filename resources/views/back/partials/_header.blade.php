<header class="top-head container-fluid">
    <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <!-- Right navbar -->
    <ul class="list-inline navbar-right top-menu top-right-menu">
        <!-- user login dropdown start-->
        <li class="dropdown text-center">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                @if(!is_null(Auth::user()->avatar))
                    <img alt="" src="{{ url(Auth::user()->avatar) }}" class="img-circle profile-img thumb-sm">
                @else
                    <img alt="" src="{{ url('/back/img/avatars/default-profile.png') }}" class="img-circle profile-img thumb-sm">
                @endif
                <span class="username">{{ ucfirst(Auth::user()->firstname) .' '. ucfirst(Auth::user()->lastname) }}</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                <li><a href="#">Vous etes {{ucfirst(Auth::user()->roles[0]->name)}}</a></li>
                <li><a href="{{ route('profile.index') }}"><i class="fa fa-cog"></i> Profile</a></li>
                <li><a href="{{ route('auth.logout') }}"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!-- End right navbar -->

</header>