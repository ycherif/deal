<aside class="left-panel collapsed">

    <!-- Navbar Start -->
    <nav class="navigation">
        <ul class="list-unstyled">
            <li class="has-submenu"><a href="{{ route('home') }}"><i class="fa fa-home"></i><span class="nav-label"><strong>PINSDEAL {{config('deal.country')}}</strong></span></a>
            </li>
            <li class="has-submenu"><a href="#"><i class="fa fa-bookmark-o"></i> <span class="nav-label"> Mes annonces</span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('ads.index') }}"><i class="fa fa-bullhorn"></i> Annonces</a></li>
                    <li><a href="{{ route('favorites.index') }}"><i class="fa fa-heart"></i> Favoris</a></li>
                    <li><a href="{{ route('ads.published') }}"><i class="fa fa-bookmark"></i> Publiées</a></li>
                    <li><a href="{{ route('ads.pending') }}"><i class="fa fa-spinner"></i> En Attente</a></li>
                </ul>
            </li>
            <li class="has-submenu"><a href="#"><i class="fa fa-user"></i> <span class="nav-label"> Mon compte</span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('profile.index') }}"><i class="fa fa-cogs"></i> Profil</a></li>
                    {{-- Only member can delete their account --}}
                    @if(Auth::user()->hasRole('member'))
                        <li><a href="{{ route('profile.delete') }}"><i class="fa fa-times"></i> Fermer mon compte</a></li>
                    @endif
                </ul>
            </li>
            <!-- ici j'ai enlevé les accolades du debut-->
            <!--<li class="has-submenu"><a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label"> Boutiques</span></a>
                <ul class="list-unstyled">
                    <li><a href=" route('shop.index') }}"><i class="fa fa-th-list"></i> Mes Boutiques</a></li>
                    <li><a href=" route('shop.create') }}"><i class="fa fa-plus-circle"></i> Creation</a></li>
                    <li><a href=" route('shop.ads') }}"><i class="fa fa-bullhorn"></i> Annonces</a></li>
                </ul>
            </li>
            <li class="has-submenu"><a href="#"><i class="fa fa-money"></i> <span class="nav-label"> Paiement</span></a>
                <ul class="list-unstyled">
                    <li><a href=" route('invoice.index') }}"><i class="fa fa-history"></i> Historique</a></li>
                    <li><a href=" route('invoice.create') }}"><i class="fa fa-credit-card"></i> Nouveau</a></li>
                </ul>
            </li>-->

            {{-- Only moderator and administrator can manage site ads --}}
            @if(!Auth::user()->hasRole('member'))
                <li class="has-submenu"><a href="#"><i class="fa fa-cog"></i> <span class="nav-label"> Modération du site</span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.ads.index') }}"><i class="fa fa-bullhorn"></i> Toutes les Annonces</a></li>
                        <!--<li><a href="{{ route('admin.ads.shopAd') }}"><i class="fa fa-spinner"></i> Annonces boutiques</a></li>-->
                        <li><a href="{{ route('admin.ads.pending') }}"><i class="fa fa-spinner"></i> Annonces en attente</a></li>
                        <li><a href="{{ route('admin.ads.published') }}"><i class="fa fa-bookmark"></i> Annonces Publiées</a></li>
                        <li><a href="{{ route('admin.faq') }}"><i class="fa fa-question-circle"></i> Faq </a></li>
                    </ul>
                </li>
            @endif
            {{-- Only administrator can see site stats --}}
            @if(Auth::user()->hasRole('admin'))
                <li class="has-submenu"><a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label"> Statistiques</span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.stats.user') }}"><i class="fa fa-users"></i> Utilisateurs</a></li>
                        <li><a href="{{ route('admin.stats.ad') }}"><i class="fa fa-bullhorn"></i> Annonces</a></li>
                    </ul>
                </li>

                <li class="has-submenu"><a href="#"><i class="fa fa-users"></i> <span class="nav-label"> Gestion des utilisateurs</span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-th-list"></i> Lister</a></li>
                        <li><a href="{{ route('admin.moderators.index') }}"><i class="fa fa-th-list"></i> Liste Moderateurs</a></li>
                        <li><a href="{{ route('admin.users.create') }}"><i class="fa fa-user-plus"></i> Ajouter</a></li>
                    </ul>
                </li>

                <!--<li class="has-submenu"><a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label"> Gestion des boutiques</span></a>
                    <ul class="list-unstyled">
                        <li><a href=" route('admin.shops') }}"><i class="fa fa-th-list"></i> Liste</a></li>
                        <li><a href=" route('admin.invoice') }}"><i class="fa fa-money"></i> Paiement</a></li>
                    </ul>
                </li>-->

                <li class="has-submenu"><a href="#"><i class="fa fa-exclamation-triangle"></i> <span class="nav-label"> Érreurs du site</span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin.logs.index') }}"><i class="fa fa-exclamation"></i> Liste</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

</aside>
