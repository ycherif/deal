@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="bg-picture">
                    <span class="bg-picture-overlay"></span><!-- overlay -->
                    <!-- meta -->
                    <div class="box-layout meta bottom">
                        <div class="col-sm-12 clearfix">
                            <span class="img-wrapper pull-left m-r-15">
                                @if(!is_null(Auth::user()->avatar) && file_exists(asset(asset(Auth::user()->avatar))))
                                    <img class="br-radius" style="width:64px" src="{{ asset(Auth::user()->avatar) }}" alt="user">
                                @elseif(!is_null(Auth::user()->avatar) && !file_exists(asset(asset(Auth::user()->avatar))))
                                    <img class="br-radius" style="width:64px" src="{{ Auth::user()->avatar }}" alt="user">
                                @else
                                    <img class="br-radius" style="width:64px" src="{{ url('/back/img/avatars/default-profile.png') }}" alt="user">
                                @endif
                            </span>
                            <div class="media-body">
                                <h3 class="text-white mb-2 m-t-10 ellipsis">{{ ucfirst(Auth::user()->firstname) .' '. ucfirst(Auth::user()->lastname) }}</h3>
                                <h5 class="text-white"> {{  !empty(Auth::user()->city->name) ? ucfirst(Auth::user()->city->name) : '' }}</h5>
                            </div>
                        </div>
                    </div>
                    <!-- End meta -->
                </div>
            </div>
        </div>

        <div class="row m-t-30">
            <div class="col-sm-12">
                @if(Auth::user()->last_login)
                    <span class="text-primary">Votre dernière connexion fut le : {{ date('d M Y à H:i:s',strtotime(Auth::user()->last_login)) }}</span>
                @endif
            </div>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="title-2"><i class="fa fa-times"></i> Supprimer le compte </h2>
                        <p>Êtes-vous sûr de vouloir supprimer votre compte ?</p>
                        <p>Toutes vos données personnelles, vos annonces ainsi que toutes données liées à votre compte seront supprimées !</p>
                        <p>Un retour en arrière ne sera plus disponible une fois le compte supprimer !</p>
                        <br>

                        <form class="delete-ad-confirm" action="{{ route('profile.destroy') }}" method="post">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger btn-block delete-account-confirm">Supprimer mon compte maintenant</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                title: "Êtes-vous sûr?",
                text: "Après suppression votre compte, vous perdrez toutes vos données!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonText: "Non, annuler!",
                confirmButtonText: "Oui, supprimer!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal("Annulation", "Vous avez annuler la suppression :)", "success");
                }
            });
        });

    </script>
@endsection