@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="bg-picture">
                    <span class="bg-picture-overlay"></span><!-- overlay -->
                    <!-- meta -->
                    <div class="box-layout meta bottom">
                        <div class="col-sm-12 clearfix">
                            <span class="img-wrapper pull-left m-r-15">
                                @if(!is_null(Auth::user()->avatar))
                                    <img class="br-radius" style="width:64px" src="{{ asset(Auth::user()->avatar) }}" alt="user">
                                @else
                                    <img class="br-radius" style="width:64px" src="{{ url('/back/img/avatars/default-profile.png') }}" alt="user">
                                @endif
                            </span>
                            <div class="media-body">
                                <h3 class="text-white mb-2 m-t-10 ellipsis">{{ ucfirst(Auth::user()->firstname) .' '. ucfirst(Auth::user()->lastname) }}</h3>
                                <h5 class="text-white"> {{  (!empty(Auth::user()->city->name)) ? ucfirst(Auth::user()->city->name) : '' }}</h5>
                            </div>
                        </div>
                    </div>
                    <!-- End meta -->
                </div>
            </div>
        </div>

        <div class="row m-t-30">
            <div class="col-sm-12">
                @if(Auth::user()->last_login)
                    <span class="text-primary">Votre dernière connexion fut le : {{ date('d M Y à H:i:s',strtotime(Auth::user()->last_login)) }}</span>
                @endif
            </div>
        </div>

        @include('back.errors._list')
        @include('back.errors._message')

        <div class="row m-t-30">
            <div class="col-sm-12">
                <div class="panel panel-default p-0">
                    <div class="panel-body p-0">
                        <ul class="nav nav-tabs profile-tabs">
                            <li class="active"><a data-toggle="tab" href="#aboutme">À propos de moi</a></li>
                            <li class=""><a data-toggle="tab" href="#edit-profile">Mes informations</a></li>
                            <li class=""><a data-toggle="tab" href="#edit-password">Paramètres</a></li>
                        </ul>

                        <div class="tab-content m-0">

                            <div id="aboutme" class="tab-pane active">
                                <div class="profile-desk">
                                    <h1>{{ ucfirst(Auth::user()->firstname) .' '. ucfirst(Auth::user()->lastname)  }}</h1>
                                    <p>{{ Auth::user()->description }}</p>
                                    <table class="table table-condensed">
                                        <thead>
                                        <tr>
                                            <th colspan="3"><h3>Mes Informations</h3></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><b>Email</b></td>
                                            <td><b>{{ Auth::user()->email }}</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Téléphone</b></td>
                                            <td><b>{{ Auth::user()->phone }}</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- end profile-desk -->
                            </div> <!-- about-me -->

                            <!-- settings -->
                            <div id="edit-profile" class="tab-pane">
                                <div class="user-profile-content">
                                    <form role="form" method="post" action="{{ route('profile.info') }}" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label>Nom d'utilisateur</label>
                                            <input type="text" name="username" class="form-control" placeholder="Votre nom d'utilisateur" value="{{ Auth::user()->username }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" placeholder="Votre email" disabled value="{{ Auth::user()->email }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Prénom</label>
                                            <input type="text" name="firstname" class="form-control" placeholder="Votre prénom" value="{{ Auth::user()->firstname }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Nom</label>
                                            <input type="text" name="lastname" class="form-control" placeholder="Votre nom" value="{{ Auth::user()->lastname}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Ville</label>
                                            <select name="city_id" id="category-group" class="form-control">
                                                @if($cities)
                                                    @foreach($cities as $city)
                                                        @if(Auth::user()->city_id == $city->id)
                                                            <option value="{{$city->id}}" selected> {{$city->name}} </option>
                                                        @else
                                                            <option value="{{$city->id}}"> {{$city->name}} </option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Adresse</label>
                                            <input type="text" name="address" class="form-control" placeholder="Votre adresse" value="{{ Auth::user()->address}}">
                                        </div>
                                        <div class="form-group">
                                            <label>À propos de moi</label>
                                            <textarea style="height: 125px;" name="description" class="form-control" placeholder="Une description de vous">{{ Auth::user()->description }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Téléphone</label>
                                            <input type="text" name="phone" class="form-control" placeholder="Votre numéro de téléphone" value="{{ Auth::user()->phone}}">
                                        </div>
                                        <div class="form-group">
                                            <label> Avatar </label>
                                            <input type="file" accept="image/*" name="avatar" class="file">
                                            <p class="help-block">Changer votre avatar</p>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Mettre à jour</button>
                                    </form>
                                </div>
                            </div>

                            <!-- Password -->
                            <div id="edit-password" class="tab-pane">
                                <div class="user-profile-content">
                                    <form role="form" method="post" action="{{ route('profile.password') }}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label>Votre mot de passe actuel</label>
                                            <input type="password" placeholder="Votre mot de passe actuel" name="actualPassword" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Mot de passe</label>
                                            <input type="password" placeholder="Votre nouveau mot de passe" name="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmer le nouveau mot de passe</label>
                                            <input type="password" placeholder="Confirmer le nouveau mot de passe" name="password_confirmation" class="form-control">
                                        </div>
                                        <button class="btn btn-primary" type="submit">Mettre à jour</button>
                                    </form>
                                </div>
                            </div>
                            <!-- End password -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection