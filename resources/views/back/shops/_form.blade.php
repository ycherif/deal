<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ !empty($shop) ? ( Auth::user()->hasRole('admin') ? route('admin.shop.update', $shop->id) : route('shop.update', $shop->id)) : route('shop.store') }}">
    <fieldset>
        {!! csrf_field() !!}
        {!! !empty($shop) ? '<input type="hidden" name="id" value="'.$shop->id.'">' : ''!!}
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Nom de la Société<sup>*</sup></label>
            <div class="col-md-8">
                <input name="name" placeholder="Nom" class="form-control input-md" type="text" value="{{  !empty($shop) ? $shop->name : old('name') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Description <sup>*</sup></label>
            <div class="col-md-8">
                <textarea name="description" placeholder="Description" class="form-control input-md" type="text">{{  !empty($shop) ? $shop->description : old('description') }}</textarea>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-md-3 control-label">Email <sup>*</sup></label>
            <div class="col-md-8">
                <input name="email" placeholder="Email" class="form-control input-md" type="text" value="{{  !empty($shop) ? $shop->email : old('email') }}">
            </div>
        </div>
        <div class="form-group required">
            <label class="col-md-3 control-label">Numéro de téléphone <sup>*</sup></label>
            <div class="col-md-8">
                <input name="phone" placeholder="Numéro de téléphone" class="form-control input-md" type="text" value="{{  !empty($shop) ? $shop->phone : old('phone') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Adresse 1 <sup>*</sup></label>
            <div class="col-md-8">
                <textarea name="address1" placeholder="Adresse" class="form-control input-md" type="text">{{  !empty($shop) ? $shop->address1 : old('address1') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Adresse 2</label>
            <div class="col-md-8">
                <textarea name="address2" placeholder="Adresse" class="form-control input-md" type="text">{{  !empty($shop) ? $shop->address2 : old('address2') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Adresse 3</label>
            <div class="col-md-8">
                <textarea name="address3" placeholder="Adresse" class="form-control input-md" type="text">{{  !empty($shop) ? $shop->address3 : old('address3') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Catégorie <sup>*</sup></label>
            <div class="col-md-8">
                <select name="category" id="category" class="form-control">
                    @foreach ($categories as $parent)
                        @if(!empty($shop) && $shop->category_name == $parent->name)
                            <option selected value="{{$parent->name}}"	>{{$parent->name}}</option>
                        @else
                            <option value="{{$parent->name}}"	>{{$parent->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-md-3 control-label">Ville <sup>*</sup></label>
            <div class="col-md-8">
                <select name="city"  class="form-control">
                    @if($cities)
                        @foreach($cities as $city)
                            @if(!empty($shop) && $shop->city_name == $city->name)
                                <option selected value="{{$city->name}}"> {{$city->name}} </option>
                            @else
                                <option value="{{$city->name}}"> {{$city->name}} </option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="textarea"> Logo </label>
            <div class="col-md-8">
                <div class="mb10">
                    <input id="input-upload" type="file" accept="image/*" name="logo" class="file" data-preview-file-type="text" >
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name">Site</label>
            <div class="col-md-8">
                <input name="site" placeholder="Ex : www.pinsdeal.com" class="form-control input-md" type="text" value="{{  !empty($shop) ? $shop->site : old('site') }}">
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-3 control-label" for="textinput-name"></label>
            <div class="col-md-8">
                <input class="btn btn-primary col-md-12" type="submit" >
            </div>
        </div>
    </fieldset>
</form>