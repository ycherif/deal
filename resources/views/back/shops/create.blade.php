@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-bullhorn"></i> Créer une nouvelle boutique</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Informations de l'annonce</h2>
                    </div>
                    <div class="panel-body">
                        @include('back.shops._form')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection