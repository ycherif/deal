@extends('back.master')

@section('content')

    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title"><i class="fa fa-th-list"></i> {{ !empty($isAdmin) ? 'Liste des boutiques' : 'Liste de vos boutiques' }}</h3>
        </div>
        @include('back.errors._list')
        @include('back.errors._message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title-ads">Informations des boutiques</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row row-horizon">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>Logo</th>
                                            <th>Nom</th>
                                            <th>Téléphone</th>
                                            <th>Adresse Principale</th>
                                            <th>Categorie</th>
                                            <th>Ville</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($shops as $shop)
                                            <tr>
                                                <td><img src="{{ asset($shop->logo) }}" class="img-responsive" style="max-width: 40px" alt="logo"></td>
                                                <td><a class="text-info" target="_blank" href="{{ route('shop.preview', $shop->slug) }}" title="{{ $shop->name }}">{{ $shop->name }}</a></td>
                                                <td>{{ $shop->phone }}</td>
                                                <td>{{ $shop->address1 }}</td>
                                                <td>{{ $shop->category_name }}</td>
                                                <td>{{ $shop->city_name }}</td>
                                                <td><p><strong></strong> <small class="label label-{{ ($shop->status == true) ? 'success' : 'warning' }} adlistingtype"> {{ ($shop->status == true) ? 'Actif':'Aucun Plan' }}</small></p></td>
                                                <td>
                                                    <a href="{{ route('shop.edit',$shop->id) }}" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></a>
                                                    &nbsp;&nbsp;
                                                    <a target="_blank" href="{{ route('shop.preview', $shop->slug) }}"  class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                                                    &nbsp;&nbsp;
                                                    @if(Auth::user()->hasRole('admin') || $shop->user_id == Auth::user()->id)
                                                        <form class="delete-ad-confirm" style="display: inline-block;" method="post" action="{{ Auth::user()->hasRole('admin') ? route('admin.shop.destroy',[$shop->id]) : route('shop.destroy',[$shop->id]) }}">
                                                            {!! csrf_field() !!}
                                                            <input type="hidden" name="shop_id" value="{{ $shop->id }}">
                                                            <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $shops->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>

        $('.delete-ad-confirm').click('submit', function(e) {
            var form = this;
            e.preventDefault();
            swal({
                title: "Êtes-vous sûr?",
                text: "Après suppression la boutique ne sera plus disponible !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonText: "Non, annuler!",
                confirmButtonText: "Oui, supprimer!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal("Annulation", "Vous avez annuler la suppression :)", "success");
                }
            });
        });

    </script>
@endsection