@extends('front.master')

@section('style')

    <link href="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet">

@endsection

@section('content')

    {{--start header--}}
    @include('front.partials._header')
    {{--end header --}}

    <div class="main-container">
        <div class="container">
            @include('front.errors._message')
            @include('front.errors._list')
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 white-box">
                    <div class="col-sm-8 col-thin-right">
                        <div class="ads-details-wrapper">
                            <h2><strong>{{ $shop->name }}</strong></h2>
                            <span class="category">{{ $shop->category_name }} </span>
                            <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> {{ 'Le ' . date('d M Y à H:i:s',strtotime($shop->created_at)) }} </span> - <span class="item-location"><i class="fa fa-map-marker"></i> {{ $shop->city_name }} </span> </span>

                            <div class="Ads-Details">
                                <div class="row">
                                    <div class="ads-details-info col-md-10" style="font-size: 16px;">
                                        <p>{{ $shop->description }}</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <hr>
                                <h3>Adresse(s)</h3>
                                <div class="ads-details-info col-md-8">
                                    <ul>
                                        <li> * {{ $shop->address1 }}</li>
                                         {!! !empty($shop->address2) ? '<li>* '.$shop->address2.'</li>' : '' !!}
                                        {!! !empty($shop->address3) ? '<li>* '.$shop->address3.'</li>' : '' !!}
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-4 col-thin-left container white-box">
                        <div class="row">
                            <div class="ads-image col-sm-12 col-xs-12">
                                <ul>
                                    <li><img class="img-responsive" src="{{ asset($shop->logo) }}" alt="img"/></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <a class="btn btn-primary col-sm-12 col-xs-12" data-toggle="modal" href="#contactShop"><i class=" icon-mail-2"></i> Envoyer un mail </a><br><br>
                                <a class="btn btn-info col-sm-12 col-xs-12"><i class=" icon-phone-1"></i> {{ $shop->phone }} </a><br><br>
                                {!! !empty($shop->site) ? '<a class="btn btn-danger col-sm-12 col-xs-12" href="http://'.$shop->site.'" target="_blank  `"><i class="fa fa-link"></i> '.$shop->site.'</a>' : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row white-box">
                <div class="col-sm-12">
                    <h2>Toutes les annonces de la boutique </h2>
                    <hr>
                    <div class="adds-wrapper" >
                        @foreach($ads as $ad)
                            <div class="item-list">
                                <div class="col-sm-2 no-padding photobox">
                                    <div class="add-image">
                                        <span class="photo-count"><i class="fa fa-camera"></i> 2 </span>
                                        <a href="{{ route('ads.detail', $ad->slug) }}">
                                            <img class="thumbnail no-margin" src="{{ asset($ad->medias[0]->url) }}" alt="img">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title"> <a href="{{ route('ads.detail', $ad->slug) }}">
                                                {{ $ad->title }} </a> </h5>
                                        <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> {{ 'Le ' . date('d M Y à H:i:s',strtotime($ad->created_at)) }} </span> - <span class="category">{{ $ad->category->name }} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> {{ $ad->city->name }} </span> </span>
                                    </div>
                                </div>

                                <div class="col-sm-3 text-right price-box">
                                    <h2 class="item-price"> {{ number_format($ad->price, 0, '', '.') }} CFA </h2>
                                    <a class="btn btn-danger btn-sm make-favorite" href="{{ route('favorites.create', $ad->id) }}">
                                        <i class="fa fa-heart"></i>
                                        <span>Favori</span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="pagination-bar text-center">
                    {!! $ads->render() !!}
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="contactShop" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Contacter la boutique par mail </h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ route('contacts.sendShop',[$shop->email, $shop->slug]) }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="receiver" value="{{ $shop->email }}"/>
                        <input type="hidden" name="slug" value="{{ $shop->slug }}">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nom:</label>
                            <input class="form-control required" id="recipient-name" name="name" placeholder="Votre nom" type="text" {{ Auth::user() ? 'value='.Auth::user()->lastname : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="sender-email" class="control-label">E-mail:</label>
                            <input id="sender-email" type="text" name="sender" placeholder="Votre adresse eamil" class="form-control email" {{ Auth::user() ? "value=".Auth::user()->email : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="recipient-Phone-Number" class="control-label">Téléphone (Facultatif):</label>
                            <input type="text" class="form-control" name="phone" id="recipient-Phone-Number" placeholder="Votre numéro de téléphone">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span>(300) </span>:</label>
                            <textarea class="form-control" name="message" placeholder="Votre message ici.."></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary pull-right">Envoyer!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script src="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.min.js')  }}" type="text/javascript"></script>
    <script>
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
    </script>

@endsection