<div style="border:5px solid #B6B6B6; border-radius: 5px;font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Geneva, Verdana, sans-serif;font-style: italic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <div style="padding: 20px 15px 10px">
        <p style="text-align: left;font-size:16px;">Bonjour {{ $firstname }},</p>
        <p style="text-align: left;font-size:16px;"> Bienvenue sur PinsDeal {{config('deal.country')}}</p>
        <p style="text-align: left;font-size:16px;">Cliquer sur le lien suivant pour activer votre compte : <a target="_blank" href="{{ route('auth.active-link', ['token' => $token]) }}" class="btn btn-default">Activer maintenant</a>.</p>
        <p style="text-align: left;font-size:15px;">Cordialement,</p>
        <p style="text-align: left;font-size:15px;">L'équipe PinsDeal {{config('deal.country')}} </p>
        <p style="text-align: left;font-size:15px;">Adresse : {{config('deal.contact_address')}} </p>
        <p style="text-align: left;font-size:15px;">Contact : {{config('deal.contact_phone')}} </p>
    </div>
    <div style="min-height: 120px;background-color: #B6B6B6; padding-top: 20px">
        <p style="text-align: center;color: white;font-family: 'Trebuchet', 'Trebuchet', 'Trebuchet';font-weight: bolder;font-size: 15px;font-style: italic">Suivez Nous</p>
        <p style="text-align: center;">
            <a style="display: inline-block;" href="https://www.facebook.com/benin.pinsdeal"><i class="fa fa-facebook-square fa-3x"></i></a>
            <a style="display: inline-block;" href="https://plus.google.com/105509084180419326640/about"><i class="fa fa-google-plus-square fa-3x"></i></a>
        </p>
    </div>
    <p style="text-align: center;"><img style="display: inline-block;" src="{{asset('images/benin.png')}}"></p>
</div>