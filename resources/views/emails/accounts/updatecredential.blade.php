<div style="border:5px solid #B6B6B6; border-radius: 5px;font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Geneva, Verdana, sans-serif;font-style: italic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <div style="padding: 20px 15px 10px">
        <p style="text-align: left;font-size:16px;">Bonjour {!! $firstname !!} ,</p>
        <p style="text-align: left;font-size:16px;">Votre compte a été mis à jour par nos services. </p>
        <p style="text-align: left;font-size:16px;">Ci-dessous la liste de vos information actuelle : </p>
        <p style="text-align: left;font-size:16px;"> Rôle : {{ $role == "membre" ? 'Membre' : 'Moderateur' }}</p>
        <p style="text-align: left;font-size:16px;"> Type : {{ $type == "P" ? 'Particulier' : 'Professionnel' }}</p>
        <p style="text-align: left;font-size:16px;"> Nom : {!! $lastname !!}</p>
        <p style="text-align: left;font-size:16px;"> Prénom : {!! $firstname !!}</p>
        <p style="text-align: left;font-size:16px;"> Téléphone : {!! $phone !!}</p>
        <p style="text-align: left;font-size:16px;"> Ville : {!! $city !!}</p>
        <p style="text-align: left;font-size:16px;"> Nom d'utilisateur : {!! $username !!}</p>
        <p style="text-align: left;font-size:16px;"> Email : {!! $email !!}</p>
        @if(isset($password))
            <p style="text-align: left;font-size:16px;"> Mot de passe : {!! $password !!}</p>
        @endif<p style="text-align: left;font-size:15px;">Cordialement,</p>
        <p style="text-align: left;font-size:15px;">L'équipe PinsDeal {{config('deal.country')}} </p>
        <p style="text-align: left;font-size:15px;">Adresse : {{config('deal.contact_address')}} </p>
        <p style="text-align: left;font-size:15px;">Contact : {{config('deal.contact_phone')}} </p>
    </div>
    <div style="min-height: 120px;background-color: #B6B6B6; padding-top: 20px">
        <p style="text-align: center;color: white;font-family: 'Trebuchet', 'Trebuchet', 'Trebuchet';font-weight: bolder;font-size: 15px;font-style: italic">Suivez Nous</p>
        <p style="text-align: center;">
            <a style="display: inline-block;" href="https://www.facebook.com/benin.pinsdeal"><i class="fa fa-facebook-square fa-3x"></i></a>
            <a style="display: inline-block;" href="https://plus.google.com/105509084180419326640/about"><i class="fa fa-google-plus-square fa-3x"></i></a>
        </p>
    </div>
    <p style="text-align: center;"><img style="display: inline-block;" src="{{asset('/front/images/pinsdeal.png')}}"></p>
</div>





