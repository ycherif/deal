{{
setlocale(LC_TIME, "fr_FR.UTF-8")
}}
<p style="text-align: left;font-size:16px;">Bonjour,</p>
<p style="text-align: left;font-size:16px;">Votre paiement pour la boutique <h3 style="font-weight: bolder; color: orange">{!! ucfirst($shop) !!}</h3> vient d'être validé par nos services pour la durée allant du <h3 style="font-weight: bolder; color: orange">{!! $begin !!}</h3> au <h3 style="font-weight: bolder; color: orange">{!! $end !!}</h3></p>
<p style="text-align: left;font-size:15px;">L'équipe PinsDeal {{config('deal.country')}} </p>
<p style="text-align: left;font-size:15px;">Adresse : {{config('deal.contact_address')}} </p>
<p style="text-align: left;font-size:15px;">Contact : {{config('deal.contact_phone')}} </p>
