<p style="text-align: left;font-size:16px;">Bonjour,</p>
<p style="text-align: left;font-size:16px;">Voici un message de {!! $name !!} à propos de votre boutique : {!! url('/boutique-detail/'.$slug) !!}</p>
<p style="text-align: left;font-size:16px;">{!! $body !!}</p>
<p style="text-align: left;font-size:16px;">Ci-après mon contact:</p>
<p style="text-align: left;font-size:15px;"> {!! $email !!} {!! $phone ?  '/' . $phone : '' !!} </p>
<p style="text-align: left;font-size:15px;">Cordialement.</p>