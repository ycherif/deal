@if( $errors->count())
    <div class="alert alert-dismissable alert-{{ Session::get('type') ? Session::get('type') : 'danger' }}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
