@if(Session::has('message'))
    <div class="col-md-12 page-content">
        <div class="inner-box category-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-{{ Session::get('type') }} pgray  alert-lg" role="alert">
                        <h4 class="no-margin no-padding">&#10004; {{ Session::get('message') }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif