<!DOCTYPE html>
<html lang="fr" @yield('dealApp')>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PINSDEAL {{config('deal.country')}} | {{config('deal.title')}}</title>

    @yield('meta')

    <link href="{{ asset('/front/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/assets/css/main.css') }}" rel="stylesheet">

    <link href="{{ asset('/front/css/custom.css') }}" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="{{asset('favicon-32x32.png')}}" />


    @yield('style')

    <script src="{{ asset('/front/js/angular.min.js') }}"></script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TCWLWR8');</script>
<!-- End Google Tag Manager -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Google Analytics -->

</head>
<body>
<div id="wrapper">
    @yield('content')
</div>


<script src="{{ asset('/front/js/jquery.min.js') }}"></script>
<!--<script src="{{asset('/front/js/user-action.js')}}"></script>-->

<script src="{{ asset('/front/assets/bootstrap/js/bootstrap.min.js') }}"></script>
@yield('script')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-103215200-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCWLWR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>
