@extends('front.master')
@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="PINSSERVICE AFRICA">
    <meta name="description" lang="fr" content="{{ str_limit(config('deal.about_desc'), 250, '...') }}">
    <meta property="og:description" content="{{ str_limit(config('deal.about_desc'), 250, '...') }}"/>
    <meta name="category" content="{{config('deal.category')}}">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="{{ str_limit(config('deal.home_title'), 50, '...') }}" />
    <meta property="og:site_name" content="{{ str_slug(config('deal.country')).'.pinsdeal.com' }}"/>
    @endsection

    @section('content')


{{--start header--}}
@include('front.partials._header')
{{--end header--}}
        <!-- Start map -->
    <div class="intro-inner">
        <div class="about-intro" style="background:url({{ asset('front/images/bg2.jpg') }}) no-repeat center; background-size:cover;">
            <div class="dtable hw100">
                <div class="dtable-cell hw100">
                    <div class="container text-center">
                        <h1 class="intro-title animated fadeInDown"> Créer une relation avec le client </h1>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- end intro  -->

    <!-- start main-container -->
    <div class="main-container inner-page">
        <div class="container">
            <div class="section-content">
                <div class="row ">
                    <h1 class="text-center title-1"> Qu'est-ce qui nous rend spécial ? </h1>
                    <hr class="center-block small text-hr">
                    <div class="col-sm-6">
                        <div class="text-content has-lead-para text-left">
                            <p class="lead">PinsDeal Ivoire est une plate-forme d'annonces, centrée sur les marchés émergents.</p>
                            <p class="lead"> Nous sommes convaincus que pinsdeal.com constitue le choix le plus sûr et le plus pratique pour tout vendre ou acheter en Afrique de l'ouest.</p>
                            <p class="lead">Ce service permet à tout utilisateur de vendre ou d'acheter en toute simplicité dans sa région, sans procédure compliquée.</p>
                            <p class="lead">pinsdeal.com met en rapport des vendeurs et des acheteurs dans leur pays et offre une expérience utilisateur exceptionnelle.</p>
                        </div>
                    </div>
                    <div class="col-sm-6"> <img src="{{ asset('front/images/info.png') }}" alt="imfo" class="img-responsive"> </div>
                </div>
            </div>
        </div>
    </div> <!-- end of main-container -->

    <!-- start parallaxbox -->
    <div class="parallaxbox about-parallax-bottom">
        <div class="container">
            <div class="row text-center featuredbox">
                <div class="col-sm-3 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class="icon-book-open ln-shadow-box shape-8"></i></div>
                        <h3 class="title-4">Il est 100% gratuit</h3>
                        <p>PINSDEAL {{config('deal.country')}} est complètement libre, à la fois pour le vendeur et l'acheteur.</p>
                    </div>
                </div>
                <div class="col-sm-3 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class=" icon-lightbulb ln-shadow-box shape-7"></i></div>
                        <h3 class="title-4">Des offres uniques</h3>
                        <p>Avec PINSDEAL {{config('deal.country')}} vous économisez de l'argent parce que vous avez accès à des offres uniques.</p>
                    </div>
                </div>
                <div class="col-sm-3 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class="icon-search ln-shadow-box shape-3"></i></div>
                        <h3 class="title-4">Trouver tous </h3>
                        <p>Avec PINSDEAL {{config('deal.country')}}, vous trouvererai tout: l'électronique, immobilier, véhicules, biens ou services. </p>
                    </div>
                </div>
                <div class="col-sm-3 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class="icon-money ln-shadow-box shape-4"></i></div>
                        <h3 class="title-4">Gagner de l'argent </h3>
                        <p>Avec PINSDEAL {{config('deal.country')}}, vous pouvez accéder à une nouvelle source de revenu en vendant des articles que vous ne voulez plus.</p>
                    </div>
                </div>
            </div>

           {{--  <div class="row text-center featuredbox">
                <div class="col-sm-6 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class="icon-map ln-shadow-box shape-5"></i></div>
                        <h3 class="title-4">Accès à tout moment, n'importe où</h3>
                        <p>Achat et vente sont facile, vous devez vous enregistrer. Vous pouvez publier simplement, ajouter des photos, ajouter une description et les coordonnées. Ensuite, vous avez juste à attendre des réponses.</p>
                    </div>
                </div>
                {{-- <div class="col-sm-6 xs-gap">
                    <div class="inner">
                        <div class="icon-box-wrap"> <i class="icon-smiley-circled ln-shadow-box shape-6"></i></div>
                        <h3 class="title-4">C'est simple</h3>
                        <p></p>
                    </div>
                </div>
            </div> --}}
        </div>
    </div> <!-- end parallaxbox -->

    {{--start footer--}}
    @include('front.partials._footer')
    {{-- end footer--}}

@endsection