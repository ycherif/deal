@extends('front.master')


@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="Pinsdeal, Inc.">
    <meta name="description" lang="fr" content="{{ str_limit(config('deal.reg_desc'), 250, '...') }}">
    <meta property="og:description" content="{{ str_limit(config('deal.reg_desc'), 250, '...') }}"/>
    <meta name="category" content="{{config('deal.category')}}">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="{{ str_limit(config('deal.home_title'), 50, '...') }}" />
    <meta property="og:site_name" content="{{ str_slug(config('deal.country')).'.pinsdeal.com' }}"/>
    @endsection



@section('content')
{{--start header--}}
@include('front.partials._header')
{{--end header--}}
<!-- Start map -->
<div class="intro-inner">
    <div class="contact-intro">
        <div class="w100 map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.330165606652!2d-3.935015585534512!3d5.3665085370194126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ed3bec51b8a1%3A0x9fe21f4ec608d019!2sOrus+Headquarters!5e0!3m2!1sfr!2sci!4v1494158567104" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div><!-- End map -->

<!-- Main container -->
<div class="main-container">
    <div class="container">
        <div class="row clearfix">
            {{--start message--}}
            @include('front.errors._message')
            @include('front.errors._list')
            {{--end message--}}
            <div class="col-md-4">
                <div class="contact_info">
                    <h5 class="list-title gray"><strong>Contactez-nous</strong></h5>
                    <div class="contact-info ">
                        <div class="address">
                            <p class="p1">Côte d'Ivoire - Afrique de l'Ouest</p>
                            <p class="p1">Abidjan</p>
                            <p class="p1">E-mail : support@pinsdeal.ci</p>
                            <p class="p1">Ligne Directe : +225 36 87 28 77</p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <div class="social-list">
                        <a target="_blank" href="{{ env('FB_PAGE')}}"><i class="fa fa-facebook fa-lg "></i></a>
                        {{--<a target="_blank" href="https://twitter.com/"><i class="fa fa-twitter fa-lg "></i></a>--}}
                        <a target="_blank" href="{{env('GP_PAGE')}}"><i class="fa fa-google-plus fa-lg "></i></a>
                        {{--<a target="_blank" href="https://www.pinterest.com/"><i class="fa fa-pinterest fa-lg "></i></a>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <h5 class="list-title gray"><strong>Contactez-nous</strong></h5>
                    <form class="form-horizontal" method="post" action="{{ route('contact.sendContactMsg') }}">
                        <fieldset>
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="firstname" name="firstname" type="text" placeholder="Prénom" class="form-control" {!! Auth::user() ? 'value='.Auth::user()->firstname : ''!!}>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="lastname" name="lastname" type="text" placeholder="Nom" class="form-control" {!! Auth::user() ? 'value='.Auth::user()->lastname : ''!!}>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="enterprise" name="enterprise" type="text" placeholder="Nom de l'entreprise" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input name="email" type="text" placeholder="Adresse email (obligatoire)" class="form-control" {!! Auth::user() ? 'value='.Auth::user()->email : ''!!}>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea class="form-control" name="message" placeholder="Entrer votre message pour nous ici. Vous aurez un retour au plutard dans les 2 jours ouvrables à venir." rows="7"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <button type="submit" class="btn btn-primary col-sm-12 col-xs-12">Envoyer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Main container -->

<!-- Start footer -->
@include('front.partials._footer')
        <!-- End footer -->

@endsection