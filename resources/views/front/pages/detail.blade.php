@extends('front.master')

@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="Pinsdeal">
    <meta name="description" content="{{ str_limit($ad->description, 50, '...') }}">
    <meta property="og:description" content="{{ str_limit($ad->description, 50, '...') }}"/>
    <meta property="og:title" content="{{ str_limit($ad->title, 50, '...') }}" />
    <meta property="og:site_name" content="Pinsdeal"/>
    <meta property="og:url" content="{{ url('/annonce-detail/'.$ad->slug) }}" />
    <meta property="og:image" content="{{ asset($ad->medias[0]->url) }}">
    <meta name="twitter:card" content="summary" />

@endsection

@section('style')

    <link href="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet">
@endsection

@section('content')

    {{--start header--}}
    @include('front.partials._header')
    {{--end header --}}

    <div class="main-container">
        <div class="container">
            @include('front.errors._message')
            @include('front.errors._list')
            <ol class="breadcrumb pull-left">
                <li><a href="{{ route('home') }}" style="text-transform: uppercase;"><i class="icon-home fa"></i></a></li>
                <li><a href="{{ route('ads.search',[config('deal.country')]) }}" style="text-transform: uppercase;">Tout le {{config('deal.country')}}</a></li>
                <li><a href="{{ route('ads.search',[$ad->city->slug]) }}" style="text-transform: uppercase;"> {{ $ad->city->name }}</a></li>
                <li><a href="{{ route('ads.search',[config('deal.country'),$parentCategory->slug]) }}">{{ $parentCategory->name }}</a></li>
                <li class="active" style="text-transform: uppercase;"><a href="{{ route('ads.search',[config('deal.country'),$ad->category->slug]) }}">{{ $ad->category->name }}</a></li>
            </ol>
            <div class="pull-right backtolist"><a href="{{ URL::previous() }}"> <i class="fa fa-angle-double-left"></i> Retourner à la recherche</a></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2>
                            <small class="label label-danger adlistingtype" style="padding: 6px;">{{ ($ad->user_type == 'B') ? 'Professionnel' : 'Personnel' }}</small>
                            <small class="label label-warning adlistingtype " style="padding: 6px;">{{ ($ad->type == 'D') ? 'Demande' : 'Offre' }}</small>
                            <small class="label label-info adlistingtype" style="padding: 6px;">{{ ($ad->negotiable == true) ? 'Négociable' : 'Prix fixe' }}</small>
                        </h2>
                        <h2 class="pull-right">
                            @if(Auth::user() && !Auth::user()->hasRole('member'))
                                <a href="{{ route('admin.ads.disapprove',[$ad->id]) }}" class="btn btn-danger"><i class="fa fa-lock"></i> Retirer</a>
                            @endif
                        </h2>
                        <h2>{{ ucfirst($ad->title) }}</h2>
                        <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> {{ 'Le ' . date('d M Y à H:i:s',strtotime($ad->created_at)) }} </span> - <span class="category">{{ $ad->category->name }} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> {{ $ad->city->name }} </span> </span>
                        <div class="ads-image">
                            <h1 class="pricetag">  {{ number_format($ad->price, 0, '', '.') }} CFA</h1>
                            <ul class="bxslider">
                                @foreach($ad->medias as $media)
                                    <li><img src="{{ config('deal.image').$media->url }}" alt="img"/></li>
                                @endforeach
                            </ul>
                            <div id="bx-pager">
                                @foreach($ad->medias as $key => $media)
                                    <a class="thumb-item-link" data-slide-index="{{ $key }}" href="#"><img src="{{ config('deal.image').$media->url }}" alt="img"/></a>
                                @endforeach
                            </div>
                        </div>

                        <div class="Ads-Details">
                            <h5 class="list-title"><strong>Détails de l'annonce</strong></h5>
                            <div class="row">
                                <div class="ads-details-info col-md-8">
                                    <p>{{ $ad->description }}</p>
                                </div>
                                <div class="col-md-4">
                                    <aside class="panel panel-body panel-details">
                                        <ul>
                                            <li>
                                                <p class=" no-margin "><strong>Prix : </strong> {{ $ad->price }} CFA </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Type : </strong> {{ ($ad->type == 'D') ? 'Demande' : 'Offre' }}</p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Ville : </strong> {{ $ad->city->name }} </p>
                                            </li>
                                            <li>
                                                <p class="no-margin"><strong>Catégorie : </strong> {{ $ad->category->name }}</p>
                                            </li>
                                        </ul>
                                    </aside>
                                    <div class="ads-action">
                                        <ul class="list-border">
                                            <li><a href="#shareAd" data-toggle="modal"> <i class="fa fa-link"></i> Partager </a> </li>
                                            @if(Auth::user())
                                                <li><a href="{{ route('favorites.create',[$ad->id]) }}"> <i class=" fa fa-heart"></i> Ajouter à mes favoris</a></li>
                                            @else
                                                <li><a href="{{ route('auth.login') }}"> <i class=" fa fa-heart"></i> Ajouter à mes favoris</a></li>
                                            @endif
                                            <li><a href="#abus" data-toggle="modal"> <i class="fa icon-info-circled-alt"></i> Signaler un abus </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-footer text-left">
                                <a class="btn  btn-default" data-toggle="modal" href="#contactUser"><i class=" icon-mail-2"></i> Envoyer un mail au vendeur</a>
                                @if(isset($ad->user->phone) && ($ad->hide_phone == false))
                                    <a class="btn  btn-info"><i class=" icon-phone-1"></i> {{ $ad->user->phone }} </a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-3  page-sidebar-right">
                    <aside>
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Contacts du vendeur</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{ isset($shop->name) ? $shop->name : $ad->user->firstname .' '. $ad->user->lastname }}</h3>
                                        <p>Ville : <strong>{{ isset($shop->city_name) ? $shop->city_name : $ad->city->name }}</strong></p>
                                        <p> Ajouté le : <strong>{{ isset($shop->created_at) ?  date('d M Y',strtotime($shop->created_at)) : date('d M Y',strtotime($ad->created_at)) }}</strong></p>
                                    </div>
                                    <div class="user-ads-action">
                                        <a href="#contactUser" data-toggle="modal" class="btn   btn-default btn-block"><i class=" icon-mail-2"></i> Envoyer un mail </a>
                                        @if(isset($ad->user->phone) && $ad->hide_phone == false)
                                            <a class="btn  btn-info btn-block"><i class=" icon-phone-1"></i> {{ $ad->user->phone }} </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel sidebar-panel">
                            <div class="panel-heading">Conseils de sécurité</div>
                            <div class="panel-content">
                                <div class="panel-body text-left">
                                    <ul class="list-check">
                                        <li> Rencontrer le vendeur dans un lieu public </li>
                                        <li> Vérifiez l'objet avant d'acheter</li>
                                        <li> Payez seulement après avoir recueilli l'article</li>
                                    </ul>
                                    <p><a class="pull-right" href="{{ route('condition') }}"> En savoir plus <i class="fa fa-angle-double-right"></i> </a></p>
                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="abus" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title"><i class="fa icon-info-circled-alt"></i> Y'a t'il un problème avec cette annonce? </h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="{{ route('contacts.sendModerator',[$ad->moderated_by,$ad->slug]) }}">
                        {!! csrf_field() !!}
                        <input type="hidden" value="{{ $ad->moderated_by }}" name="moderator">
                        <input type="hidden" name="slug" value="{{ $ad->slug }}">
                        <div class="form-group">
                            <label for="report-reason" class="control-label">Raison:</label>
                            <select name="reason" class="form-control">
                                <optgroup label="Sélectionner une raison">
                                    <option value="Indisponibilité">Indisponible</option>
                                    <option value="Fraude">Fraude</option>
                                    <option value="Dupliquée">Dupliquée</option>
                                    <option value="Mauvaise catégorie">Mauvaise catégorie</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Votre E-mail:</label>
                            <input type="text" name="email" class="form-control" placeholder="Entrer votre adresse email" {{ Auth::user() ? "value=".Auth::user()->email : '' }}>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span class="text-count">(300) </span>:</label>
                            <textarea class="form-control" name="message" ></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary">Envoyer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="contactUser" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Contacter l'annonceur par mail </h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ isset($shop) ? route('contacts.sendMember',[$shop->email,$ad->slug]) : route('contacts.sendMember',[$ad->user->email,$ad->slug]) }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="receiver" value="{{ isset($shop) ? $shop->email : $ad->user->email }}"/>
                        <input type="hidden" name="slug" value="{{ $ad->slug }}">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nom:</label>
                            <input class="form-control required" id="recipient-name" name="name" placeholder="Votre nom" type="text" {{ Auth::user() ? 'value='.Auth::user()->lastname : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="sender-email" class="control-label">E-mail:</label>
                            <input id="sender-email" type="text" name="sender" placeholder="Votre adresse eamil" class="form-control email" {{ Auth::user() ? "value=".Auth::user()->email : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="recipient-Phone-Number" class="control-label">Téléphone (Facultatif):</label>
                            <input type="text" class="form-control" name="phone" id="recipient-Phone-Number" placeholder="Votre numéro de téléphone">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span>(300) </span>:</label>
                            <textarea class="form-control" name="message" placeholder="Votre message ici.."></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary pull-right">Envoyer!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="shareAd" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Partager sur les reseaux sociaux </h4>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <a class="col-sm-4" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('/annonce-detail/'.$ad->slug) }}"><i style="font-size: 100px;" class="fa fa-facebook-official"></i></a>
                        <a class="col-sm-4" target="_blank" href="https://twitter.com/home?status={{ url('/annonce-detail/'.$ad->slug) }}"><i style="font-size: 100px; color: #3498db;" class="fa fa-twitter" ></i></a>
                        <a class="col-sm-4" target="_blank" href="https://plus.google.com/share?url={{ url('/annonce-detail/'.$ad->slug) }}"><i style="font-size: 100px; color: #e74c3c;" class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--start footer--}}
    @include('front.partials._footer')
    {{-- end footer--}}

@endsection

@section('script')

    <script src="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.min.js')  }}" type="text/javascript"></script>
    <script>
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
    </script>

@endsection
