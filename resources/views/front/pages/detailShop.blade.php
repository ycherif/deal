@extends('front.master')

@section('style')

    <link href="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet">

@endsection

@section('dealApp')
    ng-app='dealApp'
@stop

@section('content')

    {{--start header--}}
    @include('front.partials._header')
    {{--end header --}}

<div ng-controller="ShopDeailCtrl" class="deal-country" id="{{config('deal.country')}}" ng-cloak>

    <div class="main-container"  >
        <div class="container">
            @include('front.errors._message')
            @include('front.errors._list')
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 white-box">
                    <div class="col-sm-8 col-thin-right">
                        <div class="ads-details-wrapper">
                            <h2><strong>{{ ucfirst($shop->name) }}</strong></h2>
                            <span class="category">{{ ucfirst($shop->category_name) }} </span>
                            <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> {{ 'Le ' . date('d M Y à H:i:s',strtotime($shop->created_at)) }} </span> - <span class="item-location"><i class="fa fa-map-marker"></i> {{ $shop->city_name }} </span> </span>
                            <div class="Ads-Details">
                                <div class="row">
                                    <div class="ads-details-info col-md-10" style="font-size: 16px;">
                                        <p>{!! ucfirst($shop->description )!!} </p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <hr>
                                <h3>Adresse(s)</h3>
                                <div class="ads-details-info col-md-8">
                                      <ul>
                                          <li> * {{ $shop->address1 }}</li>
                                          {!! !empty($shop->address2) ? '<li>* '.$shop->address2.'</li>' : '' !!}
                                          {!! !empty($shop->address3) ? '<li>* '.$shop->address3.'</li>' : '' !!}
                                      </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4 col-thin-left container white-box">
                        <div class="row">
                            <div class="ads-image col-sm-12 col-xs-12">
                                <ul>
                                    <li><img class="img-responsive" ng-src="{{ $shop->logo ? config('deal.image').$shop->logo : asset('/back/img/shops/default-shop.png') }}" alt="img"/></li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <a class="btn btn-primary col-sm-12 col-xs-12" data-toggle="modal" href="#contactShop"><i class=" icon-mail-2"></i> Envoyer un mail </a><br><br>
                                <a class="btn btn-info col-sm-12 col-xs-12"><i class=" icon-phone-1"></i> {{ $shop->phone }} </a><br><br>
                                @if($shop->site)
                                    <a  class="btn btn-danger col-sm-12 col-xs-12" href="http://{{ $shop->site }}" target="_blank "><i class="fa fa-link"></i> {{ $shop->site}} </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row white-box">
                <div class="col-sm-12">
                    <h2 class="text-center"><strong>Toutes les annonces de la boutique</strong></h2>
                    <div class="form-group col-sm-2">
                        <label class="col-md-4 control-label">Recherche</label>
                        <input type="text" ng-model="search.term" search-patern ng-change="setDealUrlPath()" placeholder="iphone 4" class="form-control"/>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="col-md-4 control-label">Categorie</label>
                        <select ng-model="search.category"  ng-change="setDealUrlPath()" class="form-control">
                            <option value="">-- Toutes les categories --</option>
                            @foreach($categories as $category)
                                <option value="{{$category->slug_search}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2">
                        <label class="col-md-4 control-label">Ville</label>
                        <select ng-model="search.city"  ng-change="setDealUrlPath()"  class="form-control">
                            <option value="">-- Toutes les villes --</option>
                            @foreach($cities as $city)
                                <option value="{{$city->slug}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2">
                        <label class="col-md-4 control-label">Minimum</label>
                        <input type="text" ng-init="search.price_min = ''" ng-model=" search.price_min " numbers-only placeholder="Ex : 5000" class="form-control">
                    </div>
                    <div class="form-group col-sm-2">
                        <label class="col-md-4 control-label">Maximum</label>
                        <input type="text" ng-init="search.price_max = ''" ng-model=" search.price_max " numbers-only placeholder="Ex : 100000" class="form-control">
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" ng-click="searchPage()" class="btn btn-block btn-primary" style="margin-top: 28px !important;"> <i class="fa fa-search"></i> </button>
                    </div>
                    <hr>
                    <div class="adds-wrapper" >
                        <div ng-repeat="ad in ads" class="item-list">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image">
                                    <span class="photo-count"><i class="fa fa-camera"></i> @{{ ad.medias_count }}</span>
                                    <a ng-href="/annonce-detail/@{{ ad.slug }} ">
                                        <img class="thumbnail no-margin" ng-src="@{{ ad.url ? '/' + ad.url : '/front/images/default-ad.png' }}" alt="img">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title"> <a ng-href="/annonce-detail/@{{ ad.slug }} ">
                                            @{{ ad.title }} </a> </h5>
                                    <p>@{{ ad.description | limitTo: 100 }}</p>
                                    <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Le @{{ ad.published_at | amDateFormat:'dddd, Do MMMM YYYY, h:mm:ss' }} </span> - <span class="category">@{{ ad.cat_name }} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> @{{ ad.city_name }} </span> </span>
                                </div>
                            </div>

                            <div class="col-sm-3 text-right price-box">
                                <h2 class="item-price">  @{{ ad.price | number:0}} CFA</h2>
                                <a ng-if="ad.user_type=='B'" class="btn btn-primary  btn-sm make-favorite" ng-href=""> <i class="fa fa-briefcase"></i> <span>Pro</span> </a>
                                <a class="btn btn-danger btn-sm make-favorite" ng-href="@{{ '/utilisateur/favoris/ajouter/' + ad.id }}">
                                    <i class="fa fa-heart"></i>
                                    <span>Favori</span>
                                </a>
                            </div>

                        </div>


                    </div>

                    <div class="tab-box    text-center">
                        <img ng-show="loading" ng-src="/front/images/loaders/loading.gif" height="400px" width="400px">
                    </div>

                    <div ng-if="ads.length<1" class="pagination-bar text-center">
                        <div class="well text-warning">
                            <h1>Aucune Annonce publiée !</h1>
                        </div>
                    </div>


                </div>

                <div ng-if="ads.length>0" class="pagination-bar text-center">
                    <ads-pagination></ads-pagination>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="contactShop" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                    <h4 class="modal-title"><i class=" icon-mail-2"></i> Contacter la boutique par mail </h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/contacts/boutique/{{ $shop->email.'/'.$shop->slug }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="receiver" value="{{ $shop->email }}"/>
                        <input type="hidden" name="slug" value="{{ $shop->slug }}">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nom:</label>
                            <input class="form-control required" id="recipient-name" name="name" placeholder="Votre nom" type="text" {{ Auth::user() ? 'value='.Auth::user()->lastname : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="sender-email" class="control-label">E-mail:</label>
                            <input id="sender-email" type="text" name="sender" placeholder="Votre adresse eamil" class="form-control email" {{ Auth::user() ? "value=".Auth::user()->email : '' }}>
                        </div>
                        <div class="form-group">
                            <label for="recipient-Phone-Number" class="control-label">Téléphone (Facultatif):</label>
                            <input type="text" class="form-control" name="phone" id="recipient-Phone-Number" placeholder="Votre numéro de téléphone">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span>(300) </span>:</label>
                            <textarea class="form-control" name="message" placeholder="Votre message ici.."></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary pull-right">Envoyer!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" ng-init="categories={{json_encode($categories)}} ; cities={{json_encode($cities)}} ;">
</div>

@endsection

@section('script')

    <script src="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.min.js')  }}" type="text/javascript"></script>
    <script>
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });

    </script>


    <script src="{{ asset('/front/js/app/app.js') }}"></script>
    <script src="{{ asset('/front/assets/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/front/assets/plugins/moment/angular-moment.min.js') }}"></script>
    <script src="{{ asset('/front/js/app/controllers/ShopDetailController.js') }}"></script>

@endsection