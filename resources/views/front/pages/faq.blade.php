@extends('front.master')
@section('content')


    {{--start header--}}
    @include('front.partials._header')
    {{--end header--}}
    <div id="wrapper">
        <div class="intro-inner">
            <div class="about-intro" style="
                background:url(front/images/bg2.jpg) no-repeat center;
                background-size:cover;">
                <div class="dtable hw100">
                    <div class="dtable-cell hw100">
                        <div class="container text-center">
                            <h1 class="intro-title animated fadeInDown"> Les Questions Les Plus Fréquentes </h1>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="main-container inner-page">
            <div class="container">
                {{--start message--}}
                @include('front.errors._message')
                @include('front.errors._list')
                {{--end message--}}
                <div class="section-content">
                    <div class="row ">
                        <h1 class="text-center title-1"> PinsDeal {{config('deal.country')}} <strong> FAQ</strong> </h1>
                        <hr class="center-block small text-hr">
                    </div>
                    <div class="faq-content">
                        <div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group faq-panel">
                            <div class="panel">
                                <div id="heading_06" role="tab" class="panel-heading">
                                    <a aria-controls="collapse_06" aria-expanded="false" href="#collapse_06" data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                        <h4 class="panel-title">
                                                Posez une Question ...
                                        </h4>
                                    </a>
                                </div>
                                <div aria-labelledby="heading_06" role="tabpanel" class="panel-collapse collapse in" id="collapse_06">
                                    <div class="panel-body">
                                        <form class="form-horizontal" method="post" action="{{ route('faq.store') }}">
                                            <fieldset>
                                                {!! csrf_field() !!}
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="textinput-name">Email</label>
                                                    <div class="col-md-8">
                                                        <input id="textinput-name" name="email" placeholder="Votre email" class="form-control input-md" type="text" {{ Auth::user() ? 'value='.Auth::user()->email : ''}}>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="textinput-name">Question</label>
                                                    <div class="col-md-8">
                                                        <input id="textinput-name" name="question" placeholder="Votre Question" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="textinput-name"></label>
                                                    <div class="col-md-8">
                                                        <input class="btn btn-primary col-md-12" type="submit" value="Envoyer" >
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @foreach($faqs as $faq)
                                <div class="panel">
                                    <div id="headingOne" role="tab" class="panel-heading">
                                        <a aria-controls="{{ $faq->id }}" aria-expanded="true" href="#{{ $faq->id }}" data-parent="#accordion" data-toggle="collapse">
                                            <h4 class="panel-title">
                                                {!! $faq->question !!}
                                            </h4>
                                        </a>
                                    </div>
                                    <div aria-labelledby="headingOne" role="tabpanel" class="panel-collapse collapse" id="{{ $faq->id }}">
                                        <div class="panel-body">
                                            {!! $faq->response !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="center">
                            {!! $faqs->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="parallaxbox about-parallax-bottom">
            <div class="container">
                <div class="row text-center featuredbox">
                    <div class="col-sm-4 xs-gap">
                        <div class="inner">
                            <div class="icon-box-wrap"> <i class="icon-book-open ln-shadow-box shape-8"></i></div>
                        <h3 class="title-4">Il est 100% gratuit</h3>
                        <p>PINSDEAL {{config('deal.country')}} est complètement libre, à la fois pour le vendeur et l'acheteur.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 xs-gap">
                        <div class="inner">
                            <div class="inner">
                        <div class="icon-box-wrap"> <i class=" icon-lightbulb ln-shadow-box shape-7"></i></div>
                        <h3 class="title-4">Des offres uniques</h3>
                        <p>Avec PINSDEAL {{config('deal.country')}} vous économisez de l'argent parce que vous avez accès à des offres uniques.</p>
                    </div>
                        </div>
                    </div>
                    <div class="col-sm-4 xs-gap">
                        <div class="inner">
                            <div class="icon-box-wrap"> <i class="icon-search ln-shadow-box shape-3"></i></div>
                        <h3 class="title-4">Trouver tous </h3>
                        <p>Avec PINSDEAL {{config('deal.country')}}, vous trouverez tout: l'électronique, immobilier, véhicules, biens ou services. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @include('front.partials._footer')

@endsection