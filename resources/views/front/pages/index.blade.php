@extends('front.master')

@section('meta')
    <meta charset="UTF-8">
    <meta name="author" content="PINSSERVICE AFRICA">
    <meta name="description" lang="fr" content="{{ str_limit(config('deal.home_desc'), 250, '...') }}">
    <meta property="og:description" content="{{ str_limit(config('deal.home_desc'), 250, '...') }}"/>
    <meta name="keywords"  lang="fr" content="{{str_limit(config('deal.home_keywords'),1000,'...')}}">
    <meta name="category" content="{{config('deal.category')}}">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="{{ str_limit(config('deal.home_title'), 50, '...') }}" />
    <meta property="og:site_name" content="{{ str_slug(config('deal.country')).'.pinsdeal.com' }}"/>
@endsection


@section('style')

    <link href="{{ asset('/front/assets/css/fileinput.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/assets/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/assets/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/assets/plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet">
    {{--<style>.themeControll{background:#2d3e50;height:auto;width:170px;position:fixed;left:0;padding:20px 0;top:100px;z-index:999999;-webkit-transform:translateX(0);-moz-transform:translateX(0);-o-transform:translateX(0);-ms-transform:translateX(0);transform:translateX(0);opacity:1;-ms-filter:none;filter:none;-webkit-transition:opacity .5s linear,-webkit-transform .7s cubic-bezier(.56,.48,0,.99);-moz-transition:opacity .5s linear,-moz-transform .7s cubic-bezier(.56,.48,0,.99);-o-transition:opacity .5s linear,-o-transform .7s cubic-bezier(.56,.48,0,.99);-ms-transition:opacity .5s linear,-ms-transform .7s cubic-bezier(.56,.48,0,.99);transition:opacity .5s linear,transform .7s cubic-bezier(.56,.48,0,.99);}.themeControll.active{display:block;-webkit-transform:translateX(-170px);-moz-transform:translateX(-170px);-o-transform:translateX(-170px);-ms-transform:translateX(-170px);transform:translateX(-170px);-webkit-transition:opacity .5s linear,-webkit-transform .7s cubic-bezier(.56,.48,0,.99);-moz-transition:opacity .5s linear,-moz-transform .7s cubic-bezier(.56,.48,0,.99);-o-transition:opacity .5s linear,-o-transform .7s cubic-bezier(.56,.48,0,.99);-ms-transition:opacity .5s linear,-ms-transform .7s cubic-bezier(.56,.48,0,.99);transition:opacity .5s linear,transform .7s cubic-bezier(.56,.48,0,.99);}.themeControll a{border-bottom:1px solid rgba(255,255,255,0.1);border-radius:0;clear:both;color:#fff;display:block;height:auto;line-height:16px;margin-bottom:5px;padding-bottom:8px;text-transform:capitalize;width:auto;}.tbtn{background:#2D3E50;color:#FFFFFF!important;font-size:30px;height:auto;padding:10px;position:absolute;right:-40px;top:0;width:40px;cursor:pointer;}.linkinner{display:block;height:400px;}.linkScroll .scroller-bar{width:17px;}.linkScroll .scroller-bar,.linkScroll .scroller-track{background:#1d2e40!important;border-color:#1d2e40!important;}@media (max-width: 780px) {.themeControll{display:none;}}</style>--}}
    <style>
        #page_info {
            position: relative;
        }
        #page_info:before {
            position: absolute;
            content: ' ';
            top: 0;
            width: 100%;
            left: 0;
            background: rgba(0,0,0, .6);
            height: 100%;
        }
    </style>
    @stop

    @section('content')
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.4&appId=1607465852867123";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <!-- Start header -->
    @include('front.partials._header')
            <!-- End header -->

    <!-- Start intro -->
    <div class="intro" style="background-image: url('/front/images/deal_bg.jpeg');">
        <div class="dtable hw100" style="background: rgba(0,0,0, .5);">
            <div class="dtable-cell hw100">
                <div class="container text-center">
                    <h1 class="intro-title animated fadeInDown">TROUVEZ LE BON DEAL</h1>
                    <p class="sub animateme fittext3 animated fadeIn">Trouvez le bon deal près de vous en quelques clics</p>
                    <div class="row search-row animated fadeInUp">
                        <form id="trouverForm">
                            <div class="col-lg-3 col-sm-3 search-col relative locationicon">
                                <select id="ville" name="ville" class="form-control locinput input-rel searchtag-input has-icon" style="-webkit-appearance: none !important;" >
                                    <option value="{{ str_slug(config('deal.country')) }}"> {{ ucfirst(config('deal.country')) }} </option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->slug }}"> {{ $city->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4 col-sm-4 search-col relative locationicon">
                                <select id="category" name="category" class="form-control locinput input-rel searchtag-input has-icon" style="-webkit-appearance: none !important;" >
                                    <option value="tous"> Toutes les catégories </option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->slug }}"> {{ $category->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3 col-sm-3 search-col relative"> <i class="icon-docs icon-append"></i>
                                <input id="query" type="text" name="q" class="form-control has-icon" placeholder="Je cherche ..." value="">
                            </div>
                            <div class="col-lg-2 col-sm-2 search-col">
                                <button class="btn btn-primary btn-search btn-block"><i class="icon-search"></i>TROUVER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End intro -->

    <!-- Start main container -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('front.errors._message')
                <div class="col-sm-12 page-content col-thin-right">
                    <div class="inner-box relative">
                        <div class="row">
                            <div class="col-md-12 ">
                                <h3 class="title-2"> <i class="icon-search-1"></i> Trouver une annonce par catégorie </h3>
                                <div class="row">
                                    <?php $cpt=0; ?>
                                    <?php $nbrCategory=0; ?>
                                    @foreach($categories as $parent)
                                        @if($cpt % 4 == 0)
                                            <div id="test" class="col-sm-6 row">
                                                @endif
                                                <div class="cat-list text-center">
                                                    <img class="center-block" src="{{ asset('front/images/categories').'/'.$parent->icon }}" alt="{{ $parent->icon }}">
                                                    <h3 class="text-center"><a href="{{ route('ads.search',[str_slug(config('deal.country')),$parent->slug]) }}"> {{ $parent->name }}</a>
                                                        <span data-target=".cat-id-{{$cpt+1}}" data-toggle="collapse"> <span style="font-size: 25px;" class="fa fa-chevron-circle-down"></span> </span>
                                                    </h3>
                                                    <ul class="cat-collapse cat-id-{{$cpt+1}} collapse">
                                                        @foreach ($parent->children as $children)
                                                            <?php $nbrCategory++ ?>
                                                            <li> <a href="{{ route('ads.search',[str_slug(config('deal.country')),$children->slug]) }}">{{ $children->name }}</a> </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <?php $cpt++; ?>
                                                @if($cpt % 4 == 0 && $cpt != 0)
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(sizeof($ads) >= 4)
                        <div class="inner-box relative">
                            <h2 class="title-2"><i class="icon-clock-2"></i> Nos dernières recommandations
                                <a id="nextItem" class="link  pull-right carousel-nav"> <i class="icon-right-open-big"></i></a>
                                <a id="prevItem" class="link pull-right carousel-nav"> <i class="icon-left-open-big"></i> </a>
                            </h2>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="no-margin item-carousel owl-carousel owl-theme">
                                        @foreach($ads as $ad)
                                            <div class="item">
                                                <a href="{{ route('ads.detail', $ad->slug) }}">
                                                <span class="item-carousel-thumb">
                                                <img class="img-responsive" src="{{ $ad->medias[0] ? config('deal.image').$ad->medias[0]->url : '/front/images/default-ad.png'}}" alt="img">
                                                </span>
                                                    <span class="item-name"> {{ $ad->title }} </span>
                                                    <span class="price"> {{ number_format($ad->price, 0, '', '.') }} CFA </span>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="inner-box relative">
                        <div class="row">
                            <div class="col-md-12 ">
                                <h3 class="title-2"> <i class="icon-location-2"></i>Emplacements Populaires </h3>
                                <div class="row">
                                    <ul class="cat-list">
                                        @foreach($cities as $city)
                                            <li class="col-md-3 col-sm-3"> <a href="{{ route('ads.search',[$city->slug]) }}"><i class="icon-map"></i> {{ $city->name }} </a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End main container -->

    <!-- Start page info -->
    <div class="page-info" id="page_info">
        <div class="container text-center section-promo">
            <div class="row">
               <div class="col-sm-2 col-xs-6 col-xxs-12">
                    <div class="iconbox-wrap">
                        <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                                <i class="icon  icon-book"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                                <h5><span>{{$nbrAds[0]->number}}</span> </h5>
                                <div class="iconbox-wrap-text">Annonces</div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 col-xxs-12">
                    <div class="iconbox-wrap">
                        <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                                <i class="icon  icon-th-large-1"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                                <h5><span>{{ $nbrCategory }}</span> </h5>
                                <div class="iconbox-wrap-text">Categories</div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3 col-xs-6  col-xxs-12">
                    <div class="iconbox-wrap">
                        <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                                <i class="icon icon-map"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                                <h5><span>{{ count($cities) }}</span> </h5>
                                <div class="iconbox-wrap-text">Villes</div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Facebook Page Integration-->
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <!--<div class="fb-like" id="fb-like"></div>-->
                    <div class="fb-page" data-href="https://www.facebook.com/pinsdeal.ivoire/"
                         data-width="380" data-hide-cover="false" data-show-facepile="true"
                         data-show-posts="false"></div>
                </div>
            </div>
        </div>
    </div><!-- End page info -->

    <!-- Start page bottom info -->
    <div class="page-bottom-info">
        <div class="page-bottom-info-inner">
            <div class="page-bottom-info-content text-center">
                <h1>Si vous avez une question, n'hésitez pas à consulter notre <span><a href="{{ route('faq.create') }}">FAQ</a></span> avant de nous contacter.</h1>
            </div>
        </div>
    </div><!-- End page bottom info -->

    <!-- Start footer -->
    @include('front.partials._footer')
            <!-- End footer -->

@endsection

@section('script')

    <script src="{{ asset('/front/assets/js/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            var owl = $(".owl-carousel").owlCarousel({

                autoPlay: 3000, //Set AutoPlay to 3 seconds

                items : 4,
                itemsDesktop : [1199,3],
                itemsDesktopSmall : [979,3]

            });


            owl.owlCarousel();

            // Custom Navigation Events
            $("#nextItem").click(function(){
                owl.trigger('owl.next');
            });
            $("#prevItem").click(function(){
                owl.trigger('owl.prev');
            });



            $('#query').on('input',function(e){
                $('#query').val($('#query').val().replace(/[^A-Za-z0-9 ]/g, ''));
            });

            $('#trouverForm').submit(function(e){
                e.preventDefault();

                var newpath="{{ route('ads.search',["##ville","##category","##q"]) }}";
                newpath=newpath.replace("##category",$('#category').val()+"");
                newpath=newpath.replace("##ville",$('#ville').val()+"");
                newpath=newpath.replace("##q",$('#query').val().split(" ").join("-")+"");
                window.location.href =newpath ;

            });

        });

    </script>

@endsection