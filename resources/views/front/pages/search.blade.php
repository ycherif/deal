@extends('front.master')

@section('dealApp')
    ng-app='dealApp'
@stop

    @section('content')
        <!-- Start header -->
        @include('front.partials._header')
        <!-- End header -->

        <div ng-controller="SearchCtrl" class="deal-country" id="{{config('deal.country')}}" ng-cloak >
            @include('front.partials._searchBar')
            <div class="main-container">
                <div class="container-fluid">
                    <div class="row">
                        @include('front.errors._message')
                        @include('front.errors._list')
                        @include('front.partials._leftSidebar')
                        @include('front.partials._resultTemplate')
                    </div>
                </div>
            </div>
            <input type="hidden" ng-init="categories={{json_encode($categories)}} ; cities={{json_encode($cities)}} ;">
        </div>

        <!-- Start footer -->
        @include('front.partials._footer')
        <!-- End footer -->
    @endsection

@section('script')
    <script src="{{ asset('/front/js/app/app.js') }}"></script>
    <script src="{{ asset('/front/assets/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/front/assets/plugins/moment/angular-moment.min.js') }}"></script>
    <script src="{{ asset('/front/js/app/controllers/SearchController.js') }}"></script>
@endsection