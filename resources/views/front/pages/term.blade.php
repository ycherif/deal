@extends('front.master')
@section('content')


    {{--start header--}}
    @include('front.partials._header')
    {{--end header--}}

    <div class="main-container inner-page">
        <div class="container">
            <div class="section-content">
                <div class="row ">
                    <h1 class="text-center title-1"> Conditions d'utilisation </h1>
                    <hr class="center-block small text-hr">
                    <div class="col-sm-12">
                        <div class="text-content has-lead-para text-center">

                            <h1>Droits et devoir du déposant </h1>

                            <p class="lead">Avant le dépôt de chacune de vos annonces, vous devez vous assurer que les informations qui y sont contenues respectent les dispositions légales et réglementaires en vigueur, le droit des tiers (notamment le droit de propriété intellectuelle, le droit à la vie privée etc…) et la ligne éditoriale du site Pinsdeal. A défaut, Pinsdeal se réserve le droit de ne pas publier l’annonce déposée, sans avoir besoin de justifier ce refus de publication.</p>
                            <p class="lead">Vous êtes responsable du contenu des annonces que vous déposez et qui seront le cas échéant publiées, et garantissez Pinsdeal contre toute revendication de tiers à ce titre. Dès lors, Pinsdeal se réserve le droit de supprimer, à tout moment et à la demande expresse des tiers concernés, les annonces ne respectant pas les conditions du présent article. Pinsdeal n’intervient pas dans les relations entre les utilisateurs du service des petites annonces et ne saurait en aucun cas voir sa responsabilité engagée dans ce cadre.</p>
                            <p class="lead">Vous pouvez déposer autant d'annonces que vous voulez et pouvez intervenir à tout moment pour supprimer ou modifier une ou plusieurs de vos annonces.</p>
                            <h1>Modération </h1>

                            <p class="lead">Le service des petites annonces est modéré a priori, ce qui signifie que les annonces sont lues par le modérateur de Pinsdeal avant leur mise en ligne et à la suite de toute modification éventuelle. </p>

                            <p class="lead">Un délai de 24h (hors week-ends et jours fériés) pourra donc s’écouler entre le dépôt de votre annonce et sa publication sur le site de Pinsdeal. </p>

                            <p class="lead">Le modérateur se réserve unilatéralement le droit de ne pas publier les annonces qui ne respectent pas les dispositions visées dans les présentes conditions d’utilisation. Toutefois, le modérateur de Pinsdeal ne contrôle pas l’authenticité des informations diffusées dans les petites annonces, ce contrôle relevant de la seule responsabilité des déposants. Par conséquent Pinsdeal ne saurait être responsable du contenu des petites annonces. </p>

                            <h1>Destination des données personnelles et des informations publiées </h1>

                            <p class="lead">Les données personnelles et les informations contenues dans les annonces sont utilisées dans le but exclusif de permettre la diffusion desdites annonces sur le site. Ces données ne sont ni transmises, ni cédées, ni louées à des tiers.</p>

                            <p class="lead">Les annonces publiées ne seront plus accessibles sur le site 3 mois aprés sa publication. Ce delai est renouvelable sous une demande expresse adressée à nos services.</p>

                            <h1>Droit d’accès, de rectification et de retrait </h1>

                            <p class="lead">Conformément à la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, vous disposez d’un droit d’accès, d’opposition, de rectification et de retrait de toute donnée personnelle. </p>

                            <p class="lead">Vous pouvez exercer ces droits en accédant directement à votre compte, créé lors de votre enregistrement. Vous avez ainsi accès à ou aux annonces que vous avez déposées et pouvez à tout moment les modifier ou les retirer.</p>

                            <h1> Droit d'auteur</h1>

                            <p class="lead">Tous les droits de propriété intellectuelle y compris le droit d'auteur, les marques déposées, les marques de service, les noms commerciaux et les autres marques distinctives sur le Site sont la propriété de Pinsdeal ou sont utilisés par celle-ci avec l'accord des titulaires des droits.</p>

                            <p class="lead">Il est interdit d'utiliser et/ou reproduire et/ou représenter et/ou modifier et/ou adapter et/ou traduire et/ou copier et/ou distribuer, l'un quelconque des éléments du site ou le site lui-même, de façon intégrale ou partielle, sur quelque support que ce soit (électronique, papier ou tout autre support) sans l'autorisation expresse et préalable de nos services.</p>

                            <p class="lead">La Société autorise cependant la reproduction sans altération et en un exemplaire, aux fins de représentation strictement privée sur écran monoposte, ou pour copie de sauvegarde ou copie privée en tirage papier.</p>

                            <p class="lead">Toute utilisation non expressément autorisée entraînera une violation des droits d'auteur, des droits à l'image, droits des personnes ou de tous autres droits et réglementations en vigueur. Cette utilisation pourra donc engager la responsabilité civile et/ou pénale de son auteur et à ce titre l’éditeur se réserve le droit d'engager des poursuites judiciaires à l'encontre de toute personne qui n'aurait pas respecté cette interdiction.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('front.partials._footer')

@endsection