<div class="footer" id="footer">
    <div class="container">
        <div class="col-sm-12 footer-nav-border">
            <ul class=" navbar-link footer-nav" style="text-align-all: center">
                <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Accueil </a> <a href="{{route('about')}}"><i class="fa fa-question"></i> Qui sommes-nous ? </a> <a href="{{route('condition')}}"><i class="fa fa-bars"></i> Conditions d'utilisation </a> <a href="{{ route('contact') }}"><i class="fa fa-envelope-o"></i> Contactez-nous </a> <a href="{{route('faq.create')}}"><i class="fa fa-question-circle"></i> FAQ </a> <br> <span>&copy; Pinsdeal {{date('Y')}}. Tous les droits réservés.</span><br>
            </ul>
        </div><br>
        {{-- <div class="col-sm-12 footer-nav-sapce footer-borter"> --}}
            {{-- <h4 class="">PinsDeal {{config('deal.country')}}, fait partie d'un réseau international de sites d'annonces. Visitez nos sites dans d'autres pays :</h4> --}}
            {{-- <ul class="navbar-link footer-nav"> --}}
                {{-- <li> --}}
                    {{-- <a target="_blank" href="http://niger.pinsdeal.com"> PinsDeal Niger </a> --}}
                    {{-- <a target="_blank" href="http://ivoire.pinsdeal.com"> PinsDeal Côte d'Ivoire</a>
                    <a target="_blank" href="http://senegal.pinsdeal.com"> PinsDeal Sénégal</a>
                    <a target="_blank" href="http://benin.pinsdeal.com"> PinsDeal Bénin</a>
                    <a target="_blank" href="http://togo.pinsdeal.com"> PinsDeal Togo </a>
                </li>
            </ul>
        </div> --}}
    </div>
</div>