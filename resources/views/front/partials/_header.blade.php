<div class="header">
    <nav class="navbar   navbar-site navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="{{ route('home') }}" class="navbar-brand logo logo-title">

                    <img class="logonav" src="{{ asset('front/images/site/logos/'.str_slug(config('deal.country')).'-logo.png') }}" alt=""/> </a> </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('ads.search',str_slug(config('deal.country'))) }}">Annonces <i class="fa fa-th-list"></i> </a></li>
                    <!--<li><a href=" route('shops.search',str_slug(config('deal.country'))) }}">Boutiques <i class="fa fa-shopping-cart"></i> </a></li>-->
                    @if(Auth::user())
                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{ Auth::user()->firstname ? ucfirst(Auth::user()->firstname) : ''}}</span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                            <ul class="dropdown-menu user-menu">
                                <li><a href="{{ route('profile.index') }}"><i class="fa fa-home"></i> Mon profil </a></li>
                                <li class="text-center">Annonce</li>
                                <li><a href="{{ route('ads.index') }}"><i class="fa fa-bullhorn"></i>Mes Annonces</a></li>
                                <!--<li class="text-center">Boutiques</li>
                                <li><a href=" route('shop.index') }}"><i class="fa fa-th-list"></i>Mes Boutiques</a></li>
                                <li><a href=" route('invoice.index') }}"><i class="fa fa-history"></i>Paiement</a></li>
                                <li><a href=" route('shop.ads') }}"><i class="fa fa-bullhorn"></i> Annonces</a></li>-->
                                @if(Auth::user() && !Auth::user()->hasRole('member'))
                                    <li class="divider"></li>
                                    <li class="text-center">Moderation</li>
                                    <li><a href="{{ route('admin.ads.pending') }}"><i class="fa fa-spinner"></i> Annonces en attente</a></li>
                                    @if(Auth::user()->hasRole('admin'))
                                        <li class="divider"></li>
                                        <li class="text-center">Administration</li>
                                        <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-th-list"></i> Utilisateurs</a></li>
                                        <li class="text-center">Statistiques</li>
                                        <li><a href="{{ route('admin.stats.user') }}"><i class="fa fa-users"></i> Utilisateurs</a></li>
                                        <li><a href="{{ route('admin.stats.ad') }}"><i class="fa fa-bullhorn"></i> Annonces</a></li>
                                        <!--<li class="text-center">Boutiques</li>
                                        <li><a href=" route('admin.shops') }}"><i class="fa fa-th-list"></i>Liste</a></li>
                                        <li><a href=" route('admin.invoice') }}"><i class="fa fa-money"></i>Paiement</a></li>-->
                                    @endif
                                @endif
                                <li class="divider"></li>
                                <li><a href="{{ route('auth.logout') }}">Déconnexion <i class="glyphicon glyphicon-off"></i> </a></li>
                            </ul>
                        </li>
                        <li class="postadd"><a class="btn btn-block btn-border btn-post btn-danger" href="{{ route('ads.create') }}"> Poster une annonce</a></li>
                    @else
                        <li><a href="{{ route('auth.login') }}"> Connexion <i class="fa fa-sign-in"></i></a></li>
                        <li><a href="{{ route('auth.register') }}"> Créer un compte <i class="fa fa-plus"></i></a></li>
                        <li class="postadd"><a class="btn btn-block btn-border btn-post btn-danger" href="{{ route('auth.login') }}"> Poster une annonce</a></li>
                    @endif
                </ul>
            </div>

        </div>

    </nav>
</div>

<div class="alert alert-dismissable alert-danger" id="ad-block-prevent" style="display: none;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Chargement des annonces impossible. Si vous utilisez Ad Block, assurez vous d'ajouter une exception pour Pinsdeal.
    </div>
