<div class="col-sm-3 page-sidebar col-sm-offset-1">
    <aside>
        <div class="inner-box">
            <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Type</a></strong></h5>
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12">
                            <div class="input-group" ng-init="search.type=''">
                                <div id="radioBtn" class="btn-group">
                                    <a class="btn btn-primary btn-sm active" ng-click="search.type='';searchAds(1)" data-toggle="fun" data-title="T">Tous</a>
                                    <a class="btn btn-primary btn-sm notActive" ng-click="search.type='O';searchAds(1)" data-toggle="fun" data-title="O">Offre</a>
                                    <a class="btn btn-primary btn-sm notActive" ng-click="search.type='D';searchAds(1)" data-toggle="fun" data-title="D">Demande</a>
                                </div>
                                <input type="hidden" name="fun" id="fun">
                            </div>
                        </div>
                    </div>
                <div style="clear:both"></div>
            </div>
            <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Prix</a></strong></h5>
                <form role="form" class="form-inline ">
                    <div class="form-group col-sm-5 no-padding">
                        <label class="col-md-4 control-label">Minimum</label>
                        <input type="text" ng-init="search.price_min=''" ng-model=" search.price_min "  numbers-only placeholder="Ex : 5000" class="form-control">
                    </div>
                    <div class="form-group col-sm-2 no-padding text-center"></div>
                    <div class="form-group col-sm-5 no-padding">
                        <label class="col-md-4 control-label">Maximum</label>
                        <input type="text" ng-init="search.price_max = ''" ng-model=" search.price_max " numbers-only placeholder="Ex : 100000" class="form-control">
                    </div>
                    <div class="form-group col-sm-5 no-padding" style="margin-top: 10px !important;">
                        <button ng-click="search.price_min = ''; search.price_max = ''; searchPage(1);" class="btn btn-danger btn-block">Annuler <i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group col-sm-2 no-padding text-center"></div>
                    <div class="form-group col-sm-5 no-padding" style="margin-top: 10px !important;">
                        <button ng-click="searchPage(1);" class="btn btn-primary btn-block">Rechercher <i class="fa fa-search"></i></button>
                    </div>
                </form>
                <div style="clear:both"></div>
            </div>
            <div class="categories-list ">
                <h5 class="list-title"><strong><a >Categories</a></strong></h5>
                <ul class="list-unstyled">
                    @foreach($categories as $cat)
                        @if($cat->parent_id==0)
                            <li ng-click="setCategory('{{$cat->slug_search}}')" ><a  style="text-decoration: none; display: block;"><span class="title">{{  $cat->name }}</span><span class="count">&nbsp;{{ $cat->count }}</span></a></li>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="locations-list">
                <h5 class="list-title"><strong><a >Emplacement</a></strong></h5>
                <ul class=" list-unstyled">
                    @foreach($cities as $ville)
                        <li ng-click="setCity('{{$ville->slug}}')" ><a  style="text-decoration: none; display: block;"><span class="title">{{  $ville->name }}</span><span class="count">&nbsp;{{ $ville->count }}</span></a></li>
                    @endforeach
                </ul>
            </div>

            <div style="clear:both"></div>
        </div>

    </aside>
</div>

@section('script')
    @parent
    <script>
        $('#radioBtn a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#'+tog).prop('value', sel);

            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        });
    </script>

@endsection