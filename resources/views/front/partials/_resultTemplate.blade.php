<div class="col-sm-7 page-content col-thin-left">
    <div class="category-list">
        <div class="tab-box ">
            <ul class="nav nav-tabs add-tabs" ng-init="userType=''">
                <li class="active"><a ng-href="#allAds" ng-click="search.user_type=''; searchPage(1)" role="tab" data-toggle="tab">Tout <span class="badge">@{{ stats['T'] }}</span></a></li>
                <li><a ng-href="#businessAds" ng-click="search.user_type='B'; searchPage(1)" role="tab" data-toggle="tab">Professionnels <span class="badge">@{{ stats['B'] }}</span></a></li>
                <li><a ng-href="#personalAds" ng-click="search.user_type='P'; searchPage(1)" role="tab" data-toggle="tab">Particuliers <span class="badge">@{{ stats['P'] }}</span></a></li>
                <div class="tab-filter">
                    <select ng-model="search.order_by" style="margin-top: 3px" ng-change="searchPage(1)" class="selectpicker form-control" data-style="btn-select" data-width="auto">
                        <option value="">Date: Décroissant</option>
                        <option value="published_at,asc" >Date: Croissant</option>
                        <option value="price,asc" >Prix: Croissant</option>
                        <option value="price,desc">Prix: Décroissant</option>
                    </select>
                </div>
            </ul>

        </div>

        <div class="adds-wrapper">
            <div class="tab-content">
                <div class="tab-pane active" id="allAds" >

                    <div ng-repeat="ad in ads" class="item-list" >

                        <div class="col-sm-2 no-padding photobox">
                            <div class="add-image"> <span class="photo-count"><i class="fa fa-camera"></i> @{{ ad.medias_count }} </span> <a ng-href="/annonce-detail/@{{ ad.slug }}"><img class="thumbnail no-margin" ng-src="@{{ ad.url ? ad.url : '/front/images/default-ad.png' }}" alt="img"></a> </div>
                        </div>

                        <div class="col-sm-7 add-desc-box">
                            <div class="add-details">
                                <h5 class="add-title"> <a ng-href="/annonce-detail/@{{ ad.slug }}">
                                        @{{ ad.title.charAt(0).toUpperCase()+ ad.title.slice(1) }} </a> </h5>
                                <p>@{{ ad.description.charAt(0).toUpperCase() + ad.description.slice(1) | limitTo: 100 }}</p>
                                <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Le @{{ ad.published_at | amDateFormat:'dddd, Do MMMM YYYY, h:mm:ss' }} </span> - <span class="category"> @{{ ad.cat_name }} </span>- <span class="item-location"><i class="fa fa-map-marker"></i> @{{ ad.city_name }} </span> </span> </div>
                            <a ng-if="ad.shop_slug" class="btn btn-primary  btn-sm " ng-href="@{{ '/boutique-detail/'+ad.shop_slug }}"> <i class="fa fa-cart-arrow-down"></i> <span>@{{ ad.shop_slug.split("-").join(" ") }}</span> </a>

                        </div>

                        <div class="col-sm-3 text-right  price-box">
                            <h2 class="item-price"> @{{ ad.price | number:0}} CFA</h2>
                            <a ng-if="ad.user_type=='B'" class="btn btn-primary  btn-sm make-favorite" ng-href=""> <i class="fa fa-briefcase"></i> <span>Pro</span> </a>
                            <a class="btn btn-danger  btn-sm make-favorite" ng-href="@{{ '/utilisateur/favoris/ajouter/' + ad.id }}"> <i class="fa fa-heart"></i> <span>Favori</span> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-box   save-search-bar text-center">
            <img ng-show="loading" ng-src="/front/images/loaders/waiting.gif" style="height: 100px;margin-top: 30px;margin-bottom: 30px">
        </div>
    </div>
    <div ng-if="ads.length <= 0" class="pagination-bar text-center">
        <div class="well text-warning">
            <h1>Aucune annonce trouvée !</h1>
        </div>
    </div>

    <div ng-if="ads.length>0" class="pagination-bar text-center">
        <ads-pagination></ads-pagination>
    </div>

    <div class="post-promo text-center">
        <h2> Voulez-vous vendre quelque chose ? </h2>
        <h5>Vendez votre produit en ligne gratuitement, c'est plus simple!</h5>

        <a href="{{ route('ads.create') }}" class="btn btn-lg btn-border btn-post btn-danger">Poster une annonce </a></div>

</div>
