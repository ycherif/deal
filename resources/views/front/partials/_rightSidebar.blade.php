<div class="col-md-3 reg-sidebar">
    <div class="reg-sidebar-inner text-center">
        <div class="promo-text-box"> <i class=" icon-picture fa fa-4x icon-color-1"></i>
            <h3><strong>Poster une annonce</strong></h3>
            <p>Créer et gérer vos propres annonces sur {{config('deal.country')}}.PinsDeal, gratuitement, rapidement et facilement.</p>
        </div>
        <div class="panel sidebar-panel">
            <div class="panel-heading uppercase"><small><strong>Comment vendre rapidement?</strong></small></div>
            <div class="panel-content">
                <div class="panel-body text-left">
                    <ul class="list-check">
                        <li> Utilisez un titre simple et descriptif </li>
                        <li> Assurez-vous que vous publiez dans la bonne catégorie</li>
                        <li> Ajouter de belles photos de votre annonce</li>
                        <li> Mettre un prix raisonnable</li>
                        <li> Vérifiez l'annonce avant de publier</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>