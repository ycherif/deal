<div class="search-row-wrapper">
    <div class="container ">
        <form id="searchForm">
            <div class="col-sm-3">
                <input ng-model="search.term" class="form-control keyword" ng-change="setDealUrlPath()"  search-patern    type="text" name="term" placeholder="Exemple: Samsung S4">
            </div>
            <div class="col-sm-4">
                <select  ng-model="search.category" ng-change="setDealUrlPath()"  ng-init="search.category = '' " class="form-control selecter" >
                    <option value="">-- Toutes les catégories --</option>
                    @foreach($categories as $category)
                        <option value="{{$category->slug_search}}">{{ $category->parent_id == 0 ? '-- '.$category->name.' --' : $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-3">
                <select ng-model="search.city" ng-change="setDealUrlPath()"  ng-init="search.city = '' " class="form-control selecter">
                    <option value="ivoire">-- Toutes les villes --</option>
                    @foreach($cities as $city)
                        <option value="{{$city->slug}}">{{$city->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <button type="submit" ng-click="searchPage(1)" class="btn btn-block btn-primary  "> <i class="fa fa-search"></i> </button>
            </div>
        </form>
    </div>
</div>