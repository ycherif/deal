<div class="container">
    <div class="col-sm-12 page-content col-thin-left">
        <h1 class="text-center"><strong>Les Boutiques</strong></h1>
        <div class="category-list">
            <div class="adds-wrapper">
                    <div ng-repeat="shop in shops" class="item-list" >
                        <div class="col-sm-2 no-padding photobox">
                            <div class="add-image">
                                <span class="photo-count">
                                    <i class="fa fa-camera"></i>
                                </span>
                                <a ng-href="/boutique-detail/@{{ shop.slug }}">
                                    <img class="thumbnail no-margin" ng-src="@{{ shop.logo ? '/' + shop.logo : '/back/img/shops/default-shop.png' }}" alt="img">
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-10 add-desc-box">
                            <div class="add-details">
                                <h5 class="add-title">
                                    <a style="font-size: 15px !important;" ng-href="/boutique-detail/@{{ shop.slug }}">
                                        @{{ shop.name.toUpperCase() }}
                                    </a>
                                </h5>
                                <p>@{{ shop.description.charAt(0).toUpperCase() + shop.description.slice(1) }}</p>
                                <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Le @{{ shop.created_at | amDateFormat:'dddd, Do MMMM YYYY, h:mm:ss' }}</span> - <span class="item-location"><i class="fa fa-map-marker"></i> @{{ shop.city_name }} </span> </span>
                            </div>
                        </div>
                    </div>
                <div class="tab-box   save-search-bar text-center">
                    <img ng-show="loading" ng-src="/front/images/loaders/loading.gif" height="400px" width="400px">
                </div>
            </div>
            <div ng-if="shops.length <= 0" class="pagination-bar text-center">
                <div class="well text-warning">
                    <h1>Aucune boutique trouvée !</h1>
                </div>
            </div>

            <div ng-if="shops.length > 0" class="pagination-bar text-center">
                <ads-pagination></ads-pagination>
            </div>

        </div>
    </div>
</div>
<input type="hidden" ng-init="categories={{json_encode($categories)}} ; cities={{json_encode($cities)}} ;">

