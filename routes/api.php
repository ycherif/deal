<?php


/* Front api routes */
Route::group(['prefix' => 'v1'], function() {
    //Route::get('/allAds', 'Front\PagesController@apiAllAds');
    Route::get('/products/{data}', 'Front\PagesController@apiSearchAds');
    // Route::get('/ads/{data}', 'Front\PagesController@apiSearchAds');
    // Route::get('/shops/{data}', 'Front\PagesController@apiSearchShops');
    //Route::get('/shop/{slug}', 'Front\PagesController@apiShopDetail');
    //Route::get('/categories_cities', 'Front\PagesController@apiCategoriesAndCities');
    //Route::get('/allShops', 'Front\PagesController@apiAllShops');
    //Route::get('/detailShop/{slug}', 'Front\PagesController@apiDetailShop');
});