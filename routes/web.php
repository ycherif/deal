<?php

/* Pages routes */
Route::get('/', ['as' =>'home', 'uses' => 'Front\PagesController@getHome']);
Route::get('/a-propos', ['as' => 'about','uses' => 'Front\PagesController@getAbout']);
Route::get('/conditions-d-utilisation', ['as' => 'condition','uses' => 'Front\PagesController@getCondition']);

// Faq routes
Route::get('/faq', ['as' => 'faq.create','uses' => 'Front\FaqController@index']);
Route::post('/faq', ['as' => 'faq.store','uses' => 'Front\FaqController@store']);

// Contact routes
Route::get('/contact', ['as' => 'contact', 'uses' => 'Front\PagesController@getContact']);
Route::post('/contact/Envoyer', ['as' => 'contact.sendContactMsg', 'uses' => 'Front\PagesController@sendContactMsg']);

//shops
// Route::get('/boutiques/{ville}/{cat?}/{q?}',['as' => 'shops.search', 'uses' => 'Front\PagesController@shopSearch']);
// Route::get('/boutique-detail/{slug}/{ville?}/{cat?}/{q?}',['as' => 'shop.detail', 'uses' => 'Front\PagesController@shopDetailSearch']);

// Ads
Route::get('/annonces/{ville}/{cat?}/{q?}',['as' => 'ads.search', 'uses' => 'Front\PagesController@adsSearch']);
Route::get('/annonce-detail/{slug}', ['as' => 'ads.detail', 'uses' => 'Front\PagesController@adDetail']);

// File
Route::get('/images/{item}/{tp}/{imagename}',['as' => 'get.image', 'uses' => 'ImagesController@getImage']);

//Route::get('/favoris/{slug}', ['as' => 'favorites.show', 'uses' => 'FavoritesController@show']);
Route::post('/contacts/membre/{email}/{slug}',['as' => 'contacts.sendMember','uses' => 'Front\ContactsController@sendMember']);
Route::post('/contacts/boutique/{email}/{slug}',['as' => 'contacts.sendShop','uses' => 'Front\ContactsController@sendShop']);
Route::post('/contacts/moderateur/{id}/{slug}',['as' => 'contacts.sendModerator','uses' => 'Front\ContactsController@sendModerator']);
Route::post('/contacts/administrateur',['as' => 'contacts.sendAdmin','uses' => 'Front\ContactsController@sendAdmin']);

/* Authentication routes */
Route::get('/connexion',['as' => 'auth.login',          'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/connexion',['as' =>  'auth.login-post',     'uses' => 'Auth\LoginController@postLogin']);
Route::get('/inscription',['as' =>  'auth.register',       'uses' => 'Auth\RegisterController@getRegister']);
Route::post('/inscription',['as' =>  'auth.register-post',  'uses' => 'Auth\RegisterController@postRegister']);

Route::get('/confirmation/{token}', ['as' =>  'auth.active-link','uses' => 'Auth\AuthController@getConfirm']);

/* Social authentication */
Route::get('/social/redirect/{provider}', ['as' => 'social.redirect',   'uses' => 'Auth\AuthController@getSocialRedirect']);
Route::get('/social/handle/{provider}', ['as' => 'social.handle',     'uses' => 'Auth\AuthController@getSocialHandle']);

/* All users routes */
Route::group(['prefix' => 'utilisateur', 'middleware' => 'auth:member|moderator|admin'], function()
{
    /* Profile routes */
    Route::get('/', ['as' => 'profile.index', 'uses' => 'Back\ProfilesController@getHome']);
    Route::post('/information', ['as' => 'profile.info', 'uses' => 'Back\ProfilesController@postProfile']);
    Route::post('/mot-de-passe', ['as' => 'profile.password', 'uses' => 'Back\ProfilesController@postPassword']);


    /* Ads favorites */
    Route::get('/favoris', ['as' => 'favorites.index', 'uses' => 'Back\FavoritesController@index']);
    Route::get('/favoris/ajouter/{id}', ['as' => 'favorites.create', 'uses' => 'Back\FavoritesController@create']);
    Route::post('/favoris/supprimer/{id}', ['as' => 'favorites.destroy', 'uses' => 'Back\FavoritesController@destroy']);

    /* Shops route */

    // Route::get('/boutiques', ['as' => 'shop.index', 'uses' => 'Back\ShopsController@index']);
    // Route::get('/boutiques/ajouter', ['as' => 'shop.create', 'uses' => 'Back\ShopsController@create']);
    // Route::post('/boutiques/{id}/supprimer', ['as' => 'shop.destroy', 'uses' => 'Back\ShopsController@destroy']);
    // Route::post('/boutiques', ['as' => 'shop.store', 'uses' => 'Back\ShopsController@store']);
    // Route::get('/boutiques/{id}/edition', ['as' => 'shop.edit', 'uses' => 'Back\ShopsController@edit']);
    // Route::post('/boutiques/{id}', ['as' => 'shop.update', 'uses' => 'Back\ShopsController@update']);
    // Route::get('/boutique/apperçu/{slug}', ['as' => 'shop.preview', 'uses' => 'Back\ShopsController@preview']);

    /* Shops ads route */
    // Route::get('/boutiques/annonces', ['as' => 'shop.ads', 'uses' => 'Back\ShopsController@getAds']);
    // Route::post('/boutiques/annonces/ajouter', ['as' => 'shop.create.ad', 'uses' => 'Back\ShopsController@storeAd']);
    // Route::get('/boutique/annonces/ajouter/{hasShops?}', ['as' => 'shop.ads.create', 'uses' => 'Back\AdsController@create']);

    /* Ads routes */
    Route::get('/annonces', ['as' => 'ads.index', 'uses' => 'Back\AdsController@index']);
    Route::get('/annonces/publiees',['as' => 'ads.published','uses' => 'Back\AdsController@published']);
    Route::get('/annonces/en-attente',['as' => 'ads.pending','uses' => 'Back\AdsController@pending']);
    Route::get('/annonces/ajouter', ['as' => 'ads.create', 'uses' => 'Back\AdsController@create']);
    Route::post('/annonces', ['as' => 'ads.store', 'uses' => 'Back\AdsController@store']);
    Route::get('/annonces/{id}/edition', ['as' => 'ads.edit', 'uses' => 'Back\AdsController@edit']);
    Route::post('/annonces/{id}', ['as' => 'ads.update', 'uses' => 'Back\AdsController@update']);
    Route::post('/annonces/supprimer/{id}', ['as' => 'ads.destroy', 'uses' => 'Back\AdsController@destroy']);
    Route::get('/annonces/apperçu/{slug}', ['as' => 'ads.preview', 'uses' => 'Back\AdsController@preview']);

    /* Ivoices route */
    Route::get('/paiements', ['as' => 'invoice.index', 'uses' => 'Back\InvoicesController@index']);
    Route::get('/paiements/ajouter', ['as' => 'invoice.create', 'uses' => 'Back\InvoicesController@create']);
    Route::post('/paiements/ajouter', ['as' => 'invoice.store', 'uses' => 'Back\InvoicesController@store']);
    Route::post('/argent/verification', ['as'=>'invoice.approve','uses'=>'Back\InvoicesController@update']);

    /* Logout route */
    Route::get('/deconnexion',['as' =>   'auth.logout','uses' => 'Auth\AuthController@getLogout']);
});

/* Users routes */
Route::group(['prefix' => 'moderateur', 'middleware' => 'auth:member'], function(){
    Route::get('/suppression', ['as'=>'profile.delete','uses'=>'Back\ProfilesController@delete']);
    Route::post('/suppression', ['as'=>'profile.destroy','uses'=>'Back\ProfilesController@destroy']);
});

/* Moderator and Administrator routes */
Route::group(['prefix' => 'moderateur', 'middleware' => 'auth:moderator|admin'], function(){
    Route::get('/annonces', ['as' => 'admin.ads.index', 'uses' => 'Back\AdminsAdsController@index']);
    Route::get('/annonces/publiees',['as' => 'admin.ads.published','uses' => 'Back\AdminsAdsController@published']);
    Route::get('/annonces/en-attente',['as' => 'admin.ads.pending','uses' => 'Back\AdminsAdsController@pending']);
    Route::get('/annonces/boutique/en-attente',['as' => 'admin.ads.shopAd','uses' => 'Back\AdminsAdsController@shopAd']);
    Route::get('/annonces/approuve/{id}', ['as' => 'admin.ads.approve', 'uses' => 'Back\AdminsAdsController@approve']);
    Route::get('/annonces/disapprove/{id}', ['as' => 'admin.ads.disapprove', 'uses' => 'Back\AdminsAdsController@disapprove']);

    // Shop
    // Route::get('/boutiques/approuve/{id}', ['as' => 'shop.approve', 'uses' => 'Back\AdminsShopsController@approve']);
    // Route::get('/boutiques/disapprove/{id}', ['as' => 'shop.disapprove', 'uses' => 'Back\AdminsShopsController@disapprove']);

    // Faq
    Route::get('/faq', ['as' => 'admin.faq', 'uses' => 'Back\AdminsFaqsController@index']);
    Route::get('/faq/creer', ['as' => 'admin.faq.create', 'uses' => 'Back\AdminsFaqsController@create']);
    Route::post('/faq/creer', ['as' => 'admin.faq.create', 'uses' => 'Back\AdminsFaqsController@store']);
    Route::get('/faq/publier/{id}', ['as' => 'admin.faq.publish', 'uses' => 'Back\AdminsFaqsController@publish']);
    Route::get('/faq/retirer/{id}', ['as' => 'admin.faq.unpublish', 'uses' => 'Back\AdminsFaqsController@unpublish']);
    Route::get('/faq/editer/{id}', ['as' => 'admin.faq.edit', 'uses' => 'Back\AdminsFaqsController@edit']);
    Route::post('/faq/editer/{id}', ['as' => 'admin.faq.edit', 'uses' => 'Back\AdminsFaqsController@update']);
    Route::post('/faq/supprimer/{id}', ['as' => 'admin.faq.destroy', 'uses' => 'Back\AdminsFaqsController@destroy']);
    Route::post('/faq/sendMail/{id}', ['as' => 'admin.faq.sendMail', 'uses' => 'Back\AdminsFaqsController@sendMail']);
});

/* Administrator routes */
Route::group(['prefix' => 'administrateur', 'middleware' => 'auth:admin'], function(){

    /* Ads */
    Route::post('/annonces/supprimer/{id}', ['as' => 'admin.ads.destroy', 'uses' => 'Back\AdminsAdsController@destroy']);

    /* Users */
    Route::get('/utilisateurs', ['as' => 'admin.users.index', 'uses' => 'Back\AdminsUsersController@index']);
    Route::get('/moderateurs',['as'=>'admin.moderators.index', 'uses'=>'Back\AdminsUsersController@moderators']);
    Route::get('/moderateurs/revoke/{id}',['as'=>'admin.moderators.revoke', 'uses'=>'Back\AdminsUsersController@revokeModerator']);
    Route::post('/moderateur/contact',['as'=>'admin.moderator.contact', 'uses'=>'Back\AdminsUsersController@contactModerator']);
    Route::get('/utilisateurs/ajouter', ['as' => 'admin.users.create', 'uses' => 'Back\AdminsUsersController@create']);
    Route::post('/utilisateurs', ['as' => 'admin.users.store', 'uses' => 'Back\AdminsUsersController@store']);
    Route::get('/utilisateurs/{id}/edition', ['as' => 'admin.users.edit', 'uses' => 'Back\AdminsUsersController@edit']);
    Route::get('/utilisateurs/{id}/restoration', ['as' => 'admin.users.restore', 'uses' => 'Back\AdminsUsersController@restore']);
    Route::post('/utilisateurs/{id}', ['as' => 'admin.users.update', 'uses' => 'Back\AdminsUsersController@update']);
    Route::post('/utilisateurs/supprimer/{id}', ['as' => 'admin.users.destroy', 'uses' => 'Back\AdminsUsersController@destroy']);

    /**
     * Stats routes
     */
    Route::get('/sites/statistiques/utilisateurs', ['as' => 'admin.stats.user', 'uses' => 'Back\AdminsStatsController@user']);
    Route::get('/sites/statistiques/utilisateurs/api', ['as' => 'admin.stats.userApi', 'uses' => 'Back\AdminsStatsController@userApi']);
    Route::get('/sites/statistiques/annonces', ['as' => 'admin.stats.ad', 'uses' => 'Back\AdminsStatsController@ad']);


    /**
     * Shop routes
     */

    // Route::get('/boutiques', ['as' => 'admin.shops', 'uses' => 'Back\AdminsShopsController@index']);
    // Route::post('/boutiques/{id}/edition', ['as' => 'admin.shop.update', 'uses' => 'Back\AdminsShopsController@update']);
    // Route::post('/boutiques/{id}/supprimer', ['as' => 'admin.shop.destroy', 'uses' => 'Back\AdminsShopsController@destroy']);

    /**
     * Invoice routes
     */
    Route::get('/paiements', ['as' => 'admin.invoice', 'uses' => 'Back\AdminsInvoicesController@index']);
    Route::post('/paiements/approuve', ['as' => 'admin.invoice.approve', 'uses' => 'Back\AdminsInvoicesController@approve']);
    Route::get('/paiements/disapprove/{id}', ['as' => 'admin.invoice.disapprove', 'uses' => 'Back\AdminsInvoicesController@disapprove']);

    /**
     * Logs routes
     */
    Route::get('/logs', ['as' => 'admin.logs.index', 'uses' => 'Back\AdminsLogsController@index']);
    Route::get('/logs/resolue/{id}', ['as' => 'admin.logs.resolved', 'uses' => 'Back\AdminsLogsController@resolved']);
    Route::get('/logs/nonresolue/{id}', ['as' => 'admin.logs.unresolved', 'uses' => 'Back\AdminsLogsController@unresolved']);
    Route::post('/logs/{id}/supprimer', ['as' => 'admin.logs.destroy', 'uses' => 'Back\AdminsLogsController@destroy']);

});

/* Password routes */
Route::get('/mot-de-passe',         ['as' => 'auth.password',       'uses' => 'Auth\PasswordController@getForgot']);
Route::post('/mot-de-passe',        ['as' => 'auth.password-post',  'uses' => 'Auth\PasswordController@postForgot']);
Route::get('/mot-de-passe/{token}', ['as' => 'auth.reset',          'uses' => 'Auth\PasswordController@getReset']);
Route::post('/mot-de-passe/{token}',['as' => 'auth.reset-post',     'uses' => 'Auth\PasswordController@postReset']);
